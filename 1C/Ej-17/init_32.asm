
EXTERN		__sys_tables_start
EXTERN		__sys_tables_end
EXTERN      __sys_tables_LMA
EXTERN		__nucleo_start
EXTERN		__nucleo_end
EXTERN		__nucleo_LMA
EXTERN		__ISR_LMA
EXTERN		__ISR_end
EXTERN		__ISR_start
EXTERN		__tabla_dig_start
EXTERN		__tabla_dig_end
EXTERN		__tabla_pag_start
EXTERN		__tabla_pag_end
EXTERN		__pila_start
EXTERN		__pila_end
EXTERN		__datos_start
EXTERN		__datos_end
EXTERN 		__datos_LMA
EXTERN		__section_tss_LMA
EXTERN		__section_tss_end
EXTERN		__section_tss_start
EXTERN		__contexto_idle_end
EXTERN		__contexto_idle_start
EXTERN		__contexto1_end
EXTERN		__contexto1_start
EXTERN		__contexto2_end
EXTERN		__contexto2_start
EXTERN		__contexto3_end
EXTERN		__contexto3_start
EXTERN		__contexto4_end
EXTERN		__contexto4_start


EXTERN 		__tarea0_LMA
EXTERN 		__data_tarea0_LMA
EXTERN 		__tarea0_start
EXTERN		__tarea0_end
EXTERN		__bss_tarea0_start
EXTERN 		__bss_tarea0_end
EXTERN 		__data_tarea0_start
EXTERN 		__data_tarea0_end
EXTERN 		__pila_nucleo_tarea0_start
EXTERN 		__pila_nucleo_tarea0_end
EXTERN 		__pila_usuario_tarea0_start
EXTERN 		__pila_usuario_tarea0_end

EXTERN 		__tarea1_LMA
EXTERN 		__data_tarea1_LMA
EXTERN 		__tarea1_start
EXTERN		__tarea1_end
EXTERN		__bss_tarea1_start
EXTERN 		__bss_tarea1_end
EXTERN 		__data_tarea1_start
EXTERN 		__data_tarea1_end
EXTERN 		__pila_nucleo_tarea1_start
EXTERN 		__pila_nucleo_tarea1_end
EXTERN 		__pila_usuario_tarea1_start
EXTERN 		__pila_usuario_tarea1_end

EXTERN 		__tarea2_LMA
EXTERN 		__data_tarea2_LMA
EXTERN 		__tarea2_start
EXTERN		__tarea2_end
EXTERN		__bss_tarea2_start
EXTERN 		__bss_tarea2_end
EXTERN 		__data_tarea2_start
EXTERN 		__data_tarea2_end
EXTERN 		__pila_nucleo_tarea2_start
EXTERN 		__pila_nucleo_tarea2_end
EXTERN 		__pila_usuario_tarea2_start
EXTERN 		__pila_usuario_tarea2_end

EXTERN 		__tarea3_LMA
EXTERN 		__data_tarea3_LMA
EXTERN 		__tarea3_start
EXTERN		__tarea3_end
EXTERN		__bss_tarea3_start
EXTERN 		__bss_tarea3_end
EXTERN 		__data_tarea3_start
EXTERN 		__data_tarea3_end
EXTERN 		__pila_nucleo_tarea3_start
EXTERN 		__pila_nucleo_tarea3_end
EXTERN 		__pila_usuario_tarea3_start
EXTERN 		__pila_usuario_tarea3_end


EXTERN 		__tarea4_LMA
EXTERN 		__data_tarea4_LMA
EXTERN 		__tarea4_start
EXTERN		__tarea4_end
EXTERN		__bss_tarea4_start
EXTERN 		__bss_tarea4_end
EXTERN 		__data_tarea4_start
EXTERN 		__data_tarea4_end
EXTERN 		__pila_nucleo_tarea4_start
EXTERN 		__pila_nucleo_tarea4_end
EXTERN 		__pila_usuario_tarea4_start
EXTERN 		__pila_usuario_tarea4_end

	
EXTERN 		INICIO_TABLA
EXTERN 		FIN_TABLA
EXTERN 		BUFFER_TABLA
EXTERN 		CONTADOR_TABLA


EXTERN 		INICIO_TABLAS_PAGINACION
EXTERN		INICIO_DTP_TAREA1
EXTERN		INICIO_TP0_TAREA1
EXTERN		INICIO_TP1_TAREA1
EXTERN		INICIO_TP2_TAREA1
EXTERN		INICIO_TP3_TAREA1

EXTERN 		INICIO_DTP_TAREA2
EXTERN		INICIO_TP0_TAREA2
EXTERN		INICIO_TP1_TAREA2
EXTERN 		INICIO_TP2_TAREA2
EXTERN		INICIO_TP3_TAREA2

EXTERN 		INICIO_DTP_TAREA3
EXTERN		INICIO_TP0_TAREA3
EXTERN		INICIO_TP1_TAREA3
EXTERN 		INICIO_TP2_TAREA3
EXTERN		INICIO_TP3_TAREA3

EXTERN 		INICIO_DTP_TAREA4
EXTERN		INICIO_TP0_TAREA4
EXTERN		INICIO_TP1_TAREA4
EXTERN 		INICIO_TP2_TAREA4
EXTERN		INICIO_TP3_TAREA4

EXTERN 		INICIO_DTP_TAREA0
EXTERN		INICIO_TP0_TAREA0
EXTERN		INICIO_TP1_TAREA0
EXTERN 		INICIO_TP2_TAREA0
EXTERN		INICIO_TP3_TAREA0

long_pila	equ		0x1000

; dd reserva 4 bytes
; Cada entrada de la tabla son 8 bytes
; La tabla va a tener una longitud de 64K
SECTION .tabla_dig		nobits
;times	4096	db 		0
;resb 4096
resb 0x10000

; Reservo 4Mb por las dudas
SECTION .tabla_pag		nobits
resb	0x100000


SECTION .pila 			nobits
resb	long_pila

;********************************************************************************
; Seccion para el contexto 0
;********************************************************************************
SECTION 	.contexto0			progbits

tss_tarea0		  times 104 db 0					;TSS de la tarea 0 (contexto)

;********************************************************************************
; Seccion para el contexto 1
;********************************************************************************
SECTION 	.contexto1			progbits

tss_tarea1		  times 104 db 0					;TSS de la tarea 1 (contexto)

;********************************************************************************
; Seccion para el contexto 2
;********************************************************************************
SECTION 	.contexto2			progbits

tss_tarea2		  times 104 db 0					;TSS de la tarea 2 (contexto)

;********************************************************************************

;********************************************************************************
; Seccion para el contexto 3
;********************************************************************************
SECTION 	.contexto3			progbits

tss_tarea3		  times 104 db 0					;TSS de la tarea 3 (contexto)

;********************************************************************************

;********************************************************************************
; Seccion para el contexto 4
;********************************************************************************
SECTION 	.contexto4			progbits

tss_tarea4		  times 104 db 0					;TSS de la tarea 4 (contexto)

;********************************************************************************


SECTION .reset_vect

; Incrusta binario compilado del reset
incbin "reset_vect.bin"


SECTION .rom_inicio 

; binario compilado de pasaje a modo protegido, ya que estoy en 16 bits
incbin "init_16.bin"


SECTION .init_32

EXTERN 		TSS_principal
EXTERN 		LONG_TSS
EXTERN      OFFSET_BACKLINK
EXTERN      OFFSET_ESP0
EXTERN      OFFSET_SS0
EXTERN      OFFSET_ESP1
EXTERN      OFFSET_SS1
EXTERN      OFFSET_ESP2
EXTERN      OFFSET_SS2
EXTERN      OFFSET_CR3
EXTERN      OFFSET_EIP
EXTERN      OFFSET_EFLAGS
EXTERN      OFFSET_EAX
EXTERN      OFFSET_ECX
EXTERN      OFFSET_EDX
EXTERN      OFFSET_EBX
EXTERN      OFFSET_ESP
EXTERN      OFFSET_EBP
EXTERN      OFFSET_ESI
EXTERN      OFFSET_EDI
EXTERN     	OFFSET_ES
EXTERN      OFFSET_CS
EXTERN      OFFSET_SS
EXTERN      OFFSET_DS
EXTERN      OFFSET_FS
EXTERN      OFFSET_GS
EXTERN      OFFSET_LDT
EXTERN      OFFSET_T
EXTERN      OFFSET_BITMAP
EXTERN      OFFSET_PRIM_EJEC
EXTERN      OFFSET_SIG_CR3


EXTERN 		system_call
EXTERN 		SEL_TSS
EXTERN 		SEL_SYSTEM_CALL
EXTERN 		offset_system_call
EXTERN 		CS_SEL_PL3
EXTERN 		DS_SEL_PL3
EXTERN 		CS_SEL_32
EXTERN 		DS_SEL_32
EXTERN 		GDT_tam_32
EXTERN 		GDT_32
EXTERN 		im_gdtr_32

EXTERN 		IDT_32
EXTERN 		idtr_img
EXTERN 		IDT

EXTERN 		main
EXTERN		tarea_1
EXTERN		tarea_2
EXTERN 		tarea_3
EXTERN 		tarea_4

EXTERN 		Excepcion_0
EXTERN 		Excepcion_1
EXTERN 		Excepcion_2
EXTERN 		Excepcion_3
EXTERN 		Excepcion_4
EXTERN 		Excepcion_5
EXTERN 		Excepcion_6
EXTERN 		Excepcion_7
EXTERN 		Excepcion_8
EXTERN 		Excepcion_9
EXTERN 		Excepcion_10
EXTERN 		Excepcion_11
EXTERN 		Excepcion_12
EXTERN 		Excepcion_13
EXTERN 		Excepcion_14
EXTERN 		Excepcion_15
EXTERN 		Excepcion_16
EXTERN 		Excepcion_17
EXTERN 		Excepcion_18
EXTERN 		Excepcion_19

EXTERN 		Interrupcion_0
EXTERN 		Interrupcion_1
EXTERN 		Interrupcion_2
EXTERN 		Interrupcion_3
EXTERN 		Interrupcion_4
EXTERN 		Interrupcion_5
EXTERN 		Interrupcion_6
EXTERN 		Interrupcion_7
EXTERN 		Interrupcion_8
EXTERN 		Interrupcion_9
EXTERN 		Interrupcion_10
EXTERN 		Interrupcion_11
EXTERN 		Interrupcion_12
EXTERN 		Interrupcion_13
EXTERN 		Interrupcion_14
EXTERN 		Interrupcion_15

EXTERN 		EXC_handler_0
EXTERN 		EXC_handler_1
EXTERN 		EXC_handler_2
EXTERN 		EXC_handler_3
EXTERN 		EXC_handler_4
EXTERN 		EXC_handler_5
EXTERN 		EXC_handler_6
EXTERN 		EXC_handler_7
EXTERN 		EXC_handler_8
EXTERN 		EXC_handler_9
EXTERN 		EXC_handler_10
EXTERN 		EXC_handler_11
EXTERN 		EXC_handler_12
EXTERN 		EXC_handler_13
EXTERN 		EXC_handler_14
EXTERN 		EXC_handler_15
EXTERN 		EXC_handler_16
EXTERN 		EXC_handler_17
EXTERN 		EXC_handler_18
EXTERN 		EXC_handler_19

EXTERN 		IRQ_handler_0
EXTERN 		IRQ_handler_1
EXTERN 		IRQ_handler_2
EXTERN 		IRQ_handler_3
EXTERN 		IRQ_handler_4
EXTERN 		IRQ_handler_5
EXTERN 		IRQ_handler_6
EXTERN 		IRQ_handler_7
EXTERN 		IRQ_handler_8
EXTERN 		IRQ_handler_9
EXTERN 		IRQ_handler_10
EXTERN 		IRQ_handler_11
EXTERN 		IRQ_handler_12
EXTERN 		IRQ_handler_13
EXTERN 		IRQ_handler_14
EXTERN 		IRQ_handler_15


EXTERN 		dir_fisica_systables
EXTERN 		dir_fisica_nucleo
EXTERN 		dir_fisica_ISR
EXTERN 		dir_fisica_pagtables
EXTERN 		dir_fisica_digtable
EXTERN 		dir_fisica_pila
EXTERN 		dir_fisica_datos
EXTERN 		dir_fisica_tarea0_text
EXTERN 		dir_fisica_tarea0_bss
EXTERN 		dir_fisica_tarea0_data
EXTERN 		dir_fisica_pila_usuario_tarea0
EXTERN 		dir_fisica_pila_nucleo_tarea0
EXTERN 		dir_fisica_tarea1_text
EXTERN 		dir_fisica_tarea1_bss
EXTERN 		dir_fisica_tarea1_data
EXTERN 		dir_fisica_pila_usuario_tarea1
EXTERN 		dir_fisica_pila_nucleo_tarea1
EXTERN 		dir_fisica_tarea2_text
EXTERN 		dir_fisica_tarea2_bss
EXTERN 		dir_fisica_tarea2_data
EXTERN 		dir_fisica_pila_usuario_tarea2
EXTERN 		dir_fisica_pila_nucleo_tarea2
EXTERN 		dir_fisica_tarea3_text
EXTERN 		dir_fisica_tarea3_bss
EXTERN 		dir_fisica_tarea3_data
EXTERN 		dir_fisica_pila_usuario_tarea3
EXTERN 		dir_fisica_pila_nucleo_tarea3
EXTERN 		dir_fisica_tarea4_text
EXTERN 		dir_fisica_tarea4_bss
EXTERN 		dir_fisica_tarea4_data
EXTERN 		dir_fisica_pila_usuario_tarea4
EXTERN 		dir_fisica_pila_nucleo_tarea4

EXTERN 		dir_fisica_tss
EXTERN 		dir_fisica_contexto0
EXTERN 		dir_fisica_contexto1
EXTERN 		dir_fisica_contexto2
EXTERN 		dir_fisica_contexto3
EXTERN 		dir_fisica_contexto4
EXTERN		dir_contexto_SIMD

EXTERN		dir_lineal_pila_nucleo_tarea0
EXTERN		dir_lineal_pila_nucleo_tarea1
EXTERN		dir_lineal_pila_nucleo_tarea2
EXTERN		dir_lineal_pila_nucleo_tarea3
EXTERN		dir_lineal_pila_nucleo_tarea4
EXTERN		dir_lineal_pila_usuario_tarea0
EXTERN      dir_lineal_pila_usuario_tarea1
EXTERN 		dir_lineal_pila_usuario_tarea2
EXTERN 		dir_lineal_pila_usuario_tarea3
EXTERN 		dir_lineal_pila_usuario_tarea4


;********************************************************************************
; Inicializacion 32bit
;********************************************************************************
cargar_GDT_32:

    mov ax, 0x10
    mov ds, ax
    mov es, ax
    mov ax, 0x00
    mov fs, ax
    mov gs, ax

    lldt ax				; LDT no se usa -> La pongo a cero

	cli
    xor esi,esi
    xor edi,edi

    ; Copia de secciones

    ;/***Copio la seccion de tablas de ROM a RAM***/
    cld 									;Segun el DF incrementa o decrementa SI y DI
	mov esi, __sys_tables_LMA				;Fuente	
	mov edi, dir_fisica_systables			;Destino
	mov ecx, __sys_tables_end
	sub ecx, __sys_tables_start
	rep movsb 
	;/***********************************/

	;/***Copio el código principal de ROM a RAM****/
	cld 									;Segun el DF incrementa o decrementa SI y DI
	mov esi, __nucleo_LMA					;Fuente
	mov edi, dir_fisica_nucleo				;Destino
	mov ecx, __nucleo_end
	sub ecx, __nucleo_start
	rep movsb
	;/***********************************/

	;/***Copio el código ISR de ROM a RAM****/
	cld 									;Segun el DF incrementa o decrementa SI y DI
	mov esi, __ISR_LMA						;Fuente
	mov edi, dir_fisica_ISR 				;Destino
	mov ecx, __ISR_end
	sub ecx, __ISR_start
	rep movsb
	;/***********************************/

	;/***Copio seccion de tabla de paginacion
	cld
	mov ax, 0x00
	mov edi, dir_fisica_pagtables
	mov ecx, __tabla_pag_end
	sub ecx, __tabla_pag_start
	rep stosb
	;/***********************************/

	;/***Copio seccion de tabla de digitos
	cld
	mov ax, 0x00
	mov edi, dir_fisica_digtable
	mov ecx, __tabla_dig_end
	sub ecx, __tabla_dig_start
	rep stosb
	;/***********************************/


	;/***Copio seccion datos de ROM a RAM****/
	cld 
	mov esi, __datos_LMA
	mov edi, dir_fisica_datos
	mov ecx, __datos_end
	sub ecx, __datos_start
	rep movsb
	;/***********************************/


	;/***Copio seccion de pila
	cld
	mov ax, 0x00
	mov edi, dir_fisica_pila
	mov ecx, __pila_end
	sub ecx, __pila_start
	rep stosb
	;/***********************************/

	;/***Copio el código de la TAREA0 de ROM a RAM****/
	cld 									;Segun el DF incrementa o decrementa SI y DI
	mov esi, __tarea0_LMA					;Fuente
	mov edi, dir_fisica_tarea0_text 		;Destino
	mov ecx, __tarea0_end
	sub ecx, __tarea0_start
	rep movsb
	;/***********************************/


	;/***Copio el código de la TAREA1 de ROM a RAM****/
	cld 									;Segun el DF incrementa o decrementa SI y DI
	mov esi, __tarea1_LMA					;Fuente
	mov edi, dir_fisica_tarea1_text 		;Destino
	mov ecx, __tarea1_end
	sub ecx, __tarea1_start
	rep movsb
	;/***********************************/

	;/***Copio el código de la TAREA2 de ROM a RAM****/
	cld 									;Segun el DF incrementa o decrementa SI y DI
	mov esi, __tarea2_LMA					;Fuente
	mov edi, dir_fisica_tarea2_text 		;Destino
	mov ecx, __tarea2_end
	sub ecx, __tarea2_start
	rep movsb
	;/***********************************/

	;/***Copio el código de la TAREA3 de ROM a RAM****/
	cld 									;Segun el DF incrementa o decrementa SI y DI
	mov esi, __tarea3_LMA					;Fuente
	mov edi, dir_fisica_tarea3_text 		;Destino
	mov ecx, __tarea3_end
	sub ecx, __tarea3_start
	rep movsb
	;/***********************************/

	;/***Copio el código de la TAREA4 de ROM a RAM****/
	cld 									;Segun el DF incrementa o decrementa SI y DI
	mov esi, __tarea4_LMA					;Fuente
	mov edi, dir_fisica_tarea4_text 		;Destino
	mov ecx, __tarea4_end
	sub ecx, __tarea4_start
	rep movsb
	;/***********************************/

	;/***Copio seccion bss de la tarea 1
	cld
	mov ax, 0x00
	mov edi, dir_fisica_tarea0_bss
	mov ecx, __bss_tarea0_end
	sub ecx, __bss_tarea0_start
	rep stosb
	;/***********************************/

	;/***Copio seccion bss de la tarea 1
	cld
	mov ax, 0x00
	mov edi, dir_fisica_tarea1_bss
	mov ecx, __bss_tarea1_end
	sub ecx, __bss_tarea1_start
	rep stosb
	;/***********************************/

	;/***Copio seccion bss de la tarea 2
	cld
	mov ax, 0x00
	mov edi, dir_fisica_tarea2_bss
	mov ecx, __bss_tarea2_end
	sub ecx, __bss_tarea2_start
	rep stosb
	;/***********************************/

	;/***Copio seccion bss de la tarea 3
	cld
	mov ax, 0x00
	mov edi, dir_fisica_tarea3_bss
	mov ecx, __bss_tarea3_end
	sub ecx, __bss_tarea3_start
	rep stosb
	;/***********************************/

	;/***Copio seccion bss de la tarea 4
	cld
	mov ax, 0x00
	mov edi, dir_fisica_tarea4_bss
	mov ecx, __bss_tarea4_end
	sub ecx, __bss_tarea4_start
	rep stosb
	;/***********************************/

	;/***Copio seccion .data de la TAREA0 de ROM a RAM****/
	cld 									;Segun el DF incrementa o decrementa SI y DI
	mov esi, __data_tarea0_LMA				;Fuente
	mov edi, dir_fisica_tarea0_data 		;Destino
	mov ecx, __data_tarea0_end
	sub ecx, __data_tarea0_start
	rep movsb
	;/***********************************/

	;/***Copio seccion .data de la TAREA1 de ROM a RAM****/
	cld 									;Segun el DF incrementa o decrementa SI y DI
	mov esi, __data_tarea1_LMA				;Fuente
	mov edi, dir_fisica_tarea1_data 		;Destino
	mov ecx, __data_tarea1_end
	sub ecx, __data_tarea1_start
	rep movsb
	;/***********************************/

	;/***Copio seccion .data de la TAREA2 de ROM a RAM****/
	cld 									;Segun el DF incrementa o decrementa SI y DI
	mov esi, __data_tarea2_LMA				;Fuente
	mov edi, dir_fisica_tarea2_data 		;Destino
	mov ecx, __data_tarea2_end
	sub ecx, __data_tarea2_start
	rep movsb
	;/***********************************/

	;/***Copio seccion .data de la TAREA3 de ROM a RAM****/
	cld 									;Segun el DF incrementa o decrementa SI y DI
	mov esi, __data_tarea3_LMA				;Fuente
	mov edi, dir_fisica_tarea3_data 		;Destino
	mov ecx, __data_tarea3_end
	sub ecx, __data_tarea3_start
	rep movsb
	;/***********************************/

	;/***Copio seccion .data de la TAREA4 de ROM a RAM****/
	cld 									;Segun el DF incrementa o decrementa SI y DI
	mov esi, __data_tarea4_LMA				;Fuente
	mov edi, dir_fisica_tarea4_data 		;Destino
	mov ecx, __data_tarea4_end
	sub ecx, __data_tarea4_start
	rep movsb
	;/***********************************/

	;/***Copio seccion de pila usuario de tarea 0
	cld
	mov ax, 0x00
	mov edi, dir_fisica_pila_usuario_tarea0
	mov ecx, __pila_usuario_tarea0_end
	sub ecx, __pila_usuario_tarea0_start
	rep stosb
	;/***********************************/

	;/***Copio seccion de pila usuario de tarea 1
	cld
	mov ax, 0x00
	mov edi, dir_fisica_pila_usuario_tarea1
	mov ecx, __pila_usuario_tarea1_end
	sub ecx, __pila_usuario_tarea1_start
	rep stosb
	;/***********************************/

	;/***Copio seccion de pila usuario de tarea 2
	cld
	mov ax, 0x00
	mov edi, dir_fisica_pila_usuario_tarea2
	mov ecx, __pila_usuario_tarea2_end
	sub ecx, __pila_usuario_tarea2_start
	rep stosb
	;/***********************************/

	;/***Copio seccion de pila usuario de tarea 3
	cld
	mov ax, 0x00
	mov edi, dir_fisica_pila_usuario_tarea3
	mov ecx, __pila_usuario_tarea3_end
	sub ecx, __pila_usuario_tarea3_start
	rep stosb
	;/***********************************/

	;/***Copio seccion de pila usuario de tarea 4
	cld
	mov ax, 0x00
	mov edi, dir_fisica_pila_usuario_tarea4
	mov ecx, __pila_usuario_tarea4_end
	sub ecx, __pila_usuario_tarea4_start
	rep stosb
	;/***********************************/

	;/***Copio seccion de pila nucleo de tarea 0
	cld
	mov ax, 0x00
	mov edi, dir_fisica_pila_nucleo_tarea0
	mov ecx, __pila_nucleo_tarea0_end
	sub ecx, __pila_nucleo_tarea0_start
	rep stosb
	;/***********************************/

	;/***Copio seccion de pila nucleo de tarea 1
	cld
	mov ax, 0x00
	mov edi, dir_fisica_pila_nucleo_tarea1
	mov ecx, __pila_nucleo_tarea1_end
	sub ecx, __pila_nucleo_tarea1_start
	rep stosb
	;/***********************************/

	;/***Copio seccion de pila nucleo de tarea 2
	cld
	mov ax, 0x00
	mov edi, dir_fisica_pila_nucleo_tarea2
	mov ecx, __pila_nucleo_tarea2_end
	sub ecx, __pila_nucleo_tarea2_start
	rep stosb
	;/***********************************/

	;/***Copio seccion de pila nucleo de tarea 3
	cld
	mov ax, 0x00
	mov edi, dir_fisica_pila_nucleo_tarea3
	mov ecx, __pila_nucleo_tarea3_end
	sub ecx, __pila_nucleo_tarea3_start
	rep stosb
	;/***********************************/

	;/***Copio seccion de pila nucleo de tarea 4
	cld
	mov ax, 0x00
	mov edi, dir_fisica_pila_nucleo_tarea4
	mov ecx, __pila_nucleo_tarea4_end
	sub ecx, __pila_nucleo_tarea4_start
	rep stosb
	;/***********************************/

	;/***Copio la seccion de la TSS de ROM a RAM***/
    cld 									;Segun el DF incrementa o decrementa SI y DI
	mov esi, __section_tss_LMA				;Fuente	
	mov edi, dir_fisica_tss  				;Destino
	mov ecx, __section_tss_end
	sub ecx, __section_tss_start
	rep movsb 
	;/***********************************/
	; dir_fisica_tss = dir_lineal_tss = 0x0000D000
	; Idem para todas las tareas 

	; Todas las tareas tienen su contexto con la misma dir lineal = 0x00008000

	mov ax, 0x00							
	mov edi, dir_fisica_contexto0
	mov ecx, __contexto_idle_end
	sub ecx, __contexto_idle_start
	rep stosb

	mov ax, 0x00							
	mov edi, dir_fisica_contexto1
	mov ecx, __contexto1_end
	sub ecx, __contexto1_start
	rep stosb

	mov ax, 0x00							
	mov edi, dir_fisica_contexto2
	mov ecx, __contexto2_end
	sub ecx, __contexto2_start
	rep stosb

	mov ax, 0x00							
	mov edi, dir_fisica_contexto3
	mov ecx, __contexto3_end
	sub ecx, __contexto3_start
	rep stosb

	mov ax, 0x00							
	mov edi, dir_fisica_contexto4
	mov ecx, __contexto4_end
	sub ecx, __contexto4_start
	rep stosb
	;/***********************************/


	; Inicializacion de los contextos para las tareas y paginacion
	call inicializacion_contextos
	call paginacion_tarea1
	call paginacion_tarea2
	call paginacion_tarea3
	call paginacion_tarea4
	call paginacion_idle

	mov eax, INICIO_DTP_TAREA0
	mov cr3, eax							; Apunto al directorio de páginas inicial

	mov eax, cr0							; Activo paginación tocando bit 31 del cr0
	or eax, 0x80000000
	mov cr0, eax

	xchg bx,bx								; Debug para ver si la paginacion es correcta. En bochs = info tab
    lgdt [im_gdtr_32]
    lidt [idtr_img]

    mov ax, SEL_TSS
    ltr ax 									; Cargo el Task register con el selector de TSS

    mov ax, DS_SEL_32
    mov ds, ax
    mov es, ax

    mov ss, ax 								; Inicializo la PILA con la dir lineal de la pila 0 - tarea idle
    mov esp, dir_lineal_pila_nucleo_tarea0
    add esp, 0xFFF      					; long de la pila (sumo 4K y resto 1 = 0x1000-1 = 0xFFF)


    ;/*************CARGA DE HANDLERS***************/
    mov eax, EXC_handler_0
	mov word[Excepcion_0], ax				;Cargo la dirección del IRQ_handler_1 en el Offset 15-0 de su entrada en la IDT
	shr eax, 16								;Shifteo 16 veces a la derecha para quedarme con la parte alta
	mov word[Excepcion_0+6], ax				;Cargo la parte alta en el offset 31-16 de la entrada de la IDT

	mov eax, EXC_handler_1
	mov word[Excepcion_1], ax				;Cargo la dirección del IRQ_handler_1 en el Offset 15-0 de su entrada en la IDT
	shr eax, 16								;Shifteo 16 veces a la derecha para quedarme con la parte alta
	mov word[Excepcion_1+6], ax				;Cargo la parte alta en el offset 31-16 de la entrada de la IDT

	mov eax, EXC_handler_2
	mov word[Excepcion_2], ax				;Cargo la dirección del IRQ_handler_1 en el Offset 15-0 de su entrada en la IDT
	shr eax, 16								;Shifteo 16 veces a la derecha para quedarme con la parte alta
	mov word[Excepcion_2+6], ax				;Cargo la parte alta en el offset 31-16 de la entrada de la IDT

	mov eax, EXC_handler_3
	mov word[Excepcion_3], ax				;Cargo la dirección del IRQ_handler_1 en el Offset 15-0 de su entrada en la IDT
	shr eax, 16								;Shifteo 16 veces a la derecha para quedarme con la parte alta
	mov word[Excepcion_3+6], ax				;Cargo la parte alta en el offset 31-16 de la entrada de la IDT

	mov eax, EXC_handler_4
	mov word[Excepcion_4], ax				;Cargo la dirección del IRQ_handler_1 en el Offset 15-0 de su entrada en la IDT
	shr eax, 16								;Shifteo 16 veces a la derecha para quedarme con la parte alta
	mov word[Excepcion_4+6], ax				;Cargo la parte alta en el offset 31-16 de la entrada de la IDT

	mov eax, EXC_handler_5
	mov word[Excepcion_5], ax				;Cargo la dirección del IRQ_handler_1 en el Offset 15-0 de su entrada en la IDT
	shr eax, 16								;Shifteo 16 veces a la derecha para quedarme con la parte alta
	mov word[Excepcion_5+6], ax				;Cargo la parte alta en el offset 31-16 de la entrada de la IDT

	mov eax, EXC_handler_6
	mov word[Excepcion_6], ax				;Cargo la dirección del IRQ_handler_1 en el Offset 15-0 de su entrada en la IDT
	shr eax, 16								;Shifteo 16 veces a la derecha para quedarme con la parte alta
	mov word[Excepcion_6+6], ax				;Cargo la parte alta en el offset 31-16 de la entrada de la IDT

	mov eax, EXC_handler_7
	mov word[Excepcion_7], ax				;Cargo la dirección del IRQ_handler_1 en el Offset 15-0 de su entrada en la IDT
	shr eax, 16								;Shifteo 16 veces a la derecha para quedarme con la parte alta
	mov word[Excepcion_7+6], ax				;Cargo la parte alta en el offset 31-16 de la entrada de la IDT

	mov eax, EXC_handler_8
	mov word[Excepcion_8], ax				;Cargo la dirección del IRQ_handler_1 en el Offset 15-0 de su entrada en la IDT
	shr eax, 16								;Shifteo 16 veces a la derecha para quedarme con la parte alta
	mov word[Excepcion_8+6], ax				;Cargo la parte alta en el offset 31-16 de la entrada de la IDT

	mov eax, EXC_handler_9
	mov word[Excepcion_9], ax				;Cargo la dirección del IRQ_handler_1 en el Offset 15-0 de su entrada en la IDT
	shr eax, 16								;Shifteo 16 veces a la derecha para quedarme con la parte alta
	mov word[Excepcion_9+6], ax				;Cargo la parte alta en el offset 31-16 de la entrada de la IDT

	mov eax, EXC_handler_10
	mov word[Excepcion_10], ax				;Cargo la dirección del IRQ_handler_1 en el Offset 15-0 de su entrada en la IDT
	shr eax, 16								;Shifteo 16 veces a la derecha para quedarme con la parte alta
	mov word[Excepcion_10+6], ax				;Cargo la parte alta en el offset 31-16 de la entrada de la IDT

	mov eax, EXC_handler_11
	mov word[Excepcion_11], ax				;Cargo la dirección del IRQ_handler_1 en el Offset 15-0 de su entrada en la IDT
	shr eax, 16								;Shifteo 16 veces a la derecha para quedarme con la parte alta
	mov word[Excepcion_11+6], ax				;Cargo la parte alta en el offset 31-16 de la entrada de la IDT

	mov eax, EXC_handler_12
	mov word[Excepcion_12], ax				;Cargo la dirección del IRQ_handler_1 en el Offset 15-0 de su entrada en la IDT
	shr eax, 16								;Shifteo 16 veces a la derecha para quedarme con la parte alta
	mov word[Excepcion_12+6], ax				;Cargo la parte alta en el offset 31-16 de la entrada de la IDT

	mov eax, EXC_handler_13
	mov word[Excepcion_13], ax				;Cargo la dirección del IRQ_handler_1 en el Offset 15-0 de su entrada en la IDT
	shr eax, 16								;Shifteo 16 veces a la derecha para quedarme con la parte alta
	mov word[Excepcion_13+6], ax				;Cargo la parte alta en el offset 31-16 de la entrada de la IDT

	mov eax, EXC_handler_14
	mov word[Excepcion_14], ax				;Cargo la dirección del IRQ_handler_1 en el Offset 15-0 de su entrada en la IDT
	shr eax, 16								;Shifteo 16 veces a la derecha para quedarme con la parte alta
	mov word[Excepcion_14+6], ax				;Cargo la parte alta en el offset 31-16 de la entrada de la IDT

	mov eax, EXC_handler_15
	mov word[Excepcion_15], ax				;Cargo la dirección del IRQ_handler_1 en el Offset 15-0 de su entrada en la IDT
	shr eax, 16								;Shifteo 16 veces a la derecha para quedarme con la parte alta
	mov word[Excepcion_15+6], ax				;Cargo la parte alta en el offset 31-16 de la entrada de la IDT

	mov eax, EXC_handler_16
	mov word[Excepcion_16], ax				;Cargo la dirección del IRQ_handler_1 en el Offset 15-0 de su entrada en la IDT
	shr eax, 16								;Shifteo 16 veces a la derecha para quedarme con la parte alta
	mov word[Excepcion_16+6], ax				;Cargo la parte alta en el offset 31-16 de la entrada de la IDT

	mov eax, EXC_handler_17
	mov word[Excepcion_17], ax				;Cargo la dirección del IRQ_handler_1 en el Offset 15-0 de su entrada en la IDT
	shr eax, 16								;Shifteo 16 veces a la derecha para quedarme con la parte alta
	mov word[Excepcion_17+6], ax				;Cargo la parte alta en el offset 31-16 de la entrada de la IDT

	mov eax, EXC_handler_18
	mov word[Excepcion_18], ax				;Cargo la dirección del IRQ_handler_1 en el Offset 15-0 de su entrada en la IDT
	shr eax, 16								;Shifteo 16 veces a la derecha para quedarme con la parte alta
	mov word[Excepcion_18+6], ax				;Cargo la parte alta en el offset 31-16 de la entrada de la IDT

	mov eax, EXC_handler_19
	mov word[Excepcion_19], ax				;Cargo la dirección del IRQ_handler_1 en el Offset 15-0 de su entrada en la IDT
	shr eax, 16								;Shifteo 16 veces a la derecha para quedarme con la parte alta
	mov word[Excepcion_19+6], ax				;Cargo la parte alta en el offset 31-16 de la entrada de la IDT



	mov eax, IRQ_handler_0
	mov word[Interrupcion_0], ax			;Cargo la dirección del IRQ_handler_1 en el Offset 15-0 de su entrada en la IDT
	shr eax, 16								;Shifteo 16 veces a la derecha para quedarme con la parte alta
	mov word[Interrupcion_0+6], ax			;Cargo la parte alta en el offset 31-16 de la entrada de la IDT

	mov eax, IRQ_handler_1
	mov word[Interrupcion_1], ax			;Cargo la dirección del IRQ_handler_1 en el Offset 15-0 de su entrada en la IDT
	shr eax, 16								;Shifteo 16 veces a la derecha para quedarme con la parte alta
	mov word[Interrupcion_1+6], ax			;Cargo la parte alta en el offset 31-16 de la entrada de la IDT

	mov eax, IRQ_handler_2
	mov word[Interrupcion_2], ax			;Cargo la dirección del IRQ_handler_1 en el Offset 15-0 de su entrada en la IDT
	shr eax, 16								;Shifteo 16 veces a la derecha para quedarme con la parte alta
	mov word[Interrupcion_2+6], ax			;Cargo la parte alta en el offset 31-16 de la entrada de la IDT

	mov eax, IRQ_handler_3
	mov word[Interrupcion_3], ax			;Cargo la dirección del IRQ_handler_1 en el Offset 15-0 de su entrada en la IDT
	shr eax, 16								;Shifteo 16 veces a la derecha para quedarme con la parte alta
	mov word[Interrupcion_3+6], ax			;Cargo la parte alta en el offset 31-16 de la entrada de la IDT

	mov eax, IRQ_handler_4
	mov word[Interrupcion_4], ax			;Cargo la dirección del IRQ_handler_1 en el Offset 15-0 de su entrada en la IDT
	shr eax, 16								;Shifteo 16 veces a la derecha para quedarme con la parte alta
	mov word[Interrupcion_4+6], ax			;Cargo la parte alta en el offset 31-16 de la entrada de la IDT

	mov eax, IRQ_handler_5
	mov word[Interrupcion_5], ax			;Cargo la dirección del IRQ_handler_1 en el Offset 15-0 de su entrada en la IDT
	shr eax, 16								;Shifteo 16 veces a la derecha para quedarme con la parte alta
	mov word[Interrupcion_5+6], ax			;Cargo la parte alta en el offset 31-16 de la entrada de la IDT

	mov eax, IRQ_handler_6
	mov word[Interrupcion_6], ax			;Cargo la dirección del IRQ_handler_1 en el Offset 15-0 de su entrada en la IDT
	shr eax, 16								;Shifteo 16 veces a la derecha para quedarme con la parte alta
	mov word[Interrupcion_6+6], ax			;Cargo la parte alta en el offset 31-16 de la entrada de la IDT

	mov eax, IRQ_handler_7
	mov word[Interrupcion_7], ax			;Cargo la dirección del IRQ_handler_1 en el Offset 15-0 de su entrada en la IDT
	shr eax, 16								;Shifteo 16 veces a la derecha para quedarme con la parte alta
	mov word[Interrupcion_7+6], ax			;Cargo la parte alta en el offset 31-16 de la entrada de la IDT

	mov eax, IRQ_handler_8
	mov word[Interrupcion_8], ax			;Cargo la dirección del IRQ_handler_1 en el Offset 15-0 de su entrada en la IDT
	shr eax, 16								;Shifteo 16 veces a la derecha para quedarme con la parte alta
	mov word[Interrupcion_8+6], ax			;Cargo la parte alta en el offset 31-16 de la entrada de la IDT

	mov eax, IRQ_handler_9
	mov word[Interrupcion_9], ax			;Cargo la dirección del IRQ_handler_1 en el Offset 15-0 de su entrada en la IDT
	shr eax, 16								;Shifteo 16 veces a la derecha para quedarme con la parte alta
	mov word[Interrupcion_9+6], ax			;Cargo la parte alta en el offset 31-16 de la entrada de la IDT

	mov eax, IRQ_handler_10
	mov word[Interrupcion_10], ax			;Cargo la dirección del IRQ_handler_1 en el Offset 15-0 de su entrada en la IDT
	shr eax, 16								;Shifteo 16 veces a la derecha para quedarme con la parte alta
	mov word[Interrupcion_10+6], ax			;Cargo la parte alta en el offset 31-16 de la entrada de la IDT

	mov eax, IRQ_handler_11
	mov word[Interrupcion_11], ax			;Cargo la dirección del IRQ_handler_1 en el Offset 15-0 de su entrada en la IDT
	shr eax, 16								;Shifteo 16 veces a la derecha para quedarme con la parte alta
	mov word[Interrupcion_11+6], ax			;Cargo la parte alta en el offset 31-16 de la entrada de la IDT

	mov eax, IRQ_handler_12
	mov word[Interrupcion_12], ax			;Cargo la dirección del IRQ_handler_1 en el Offset 15-0 de su entrada en la IDT
	shr eax, 16								;Shifteo 16 veces a la derecha para quedarme con la parte alta
	mov word[Interrupcion_12+6], ax			;Cargo la parte alta en el offset 31-16 de la entrada de la IDT

	mov eax, IRQ_handler_13
	mov word[Interrupcion_13], ax			;Cargo la dirección del IRQ_handler_1 en el Offset 15-0 de su entrada en la IDT
	shr eax, 16								;Shifteo 16 veces a la derecha para quedarme con la parte alta
	mov word[Interrupcion_13+6], ax			;Cargo la parte alta en el offset 31-16 de la entrada de la IDT

	mov eax, IRQ_handler_14
	mov word[Interrupcion_14], ax			;Cargo la dirección del IRQ_handler_1 en el Offset 15-0 de su entrada en la IDT
	shr eax, 16								;Shifteo 16 veces a la derecha para quedarme con la parte alta
	mov word[Interrupcion_14+6], ax			;Cargo la parte alta en el offset 31-16 de la entrada de la IDT

	mov eax, IRQ_handler_15
	mov word[Interrupcion_15], ax			;Cargo la dirección del IRQ_handler_1 en el Offset 15-0 de su entrada en la IDT
	shr eax, 16								;Shifteo 16 veces a la derecha para quedarme con la parte alta
	mov word[Interrupcion_15+6], ax			;Cargo la parte alta en el offset 31-16 de la entrada de la IDT

	; Para nivel de privilegio
	mov eax, system_call
	mov word[offset_system_call], ax
	shr eax, 16
	mov word[offset_system_call+6], ax

	;/*+*******************************************/


	; Habilitacion SIMD
	;/*+*******************************************/
	mov eax, cr4							;Activo los bits 9 y 10 de CR4
	or eax, 0x600
	mov cr4, eax
	;/*+*******************************************/

	;Para Interrupciones y Excepciones
	push ebp
	mov ebp, esp

	mov bx, 0x2028							; ¿Qué hace ésto?
	call InitPIC							; Inicializamos los PICs con todas las interrrupciones deshabilitadas
	xchg bx,bx
	mov esp, ebp
	pop ebp

	xor eax, eax
		
	;mov al, 0xFD							; Habilito solo IRQ1 => Teclado
	mov al, 0xFC							; Habilito IRQ1	=> Teclado	IRQ0	=> Timer
	out 0x21, al 							; 11111100
	sti

    jmp CS_SEL_32:main 						; Salto a tarea IDLE


;--------------------------------------------------------------------------------
; Inicializacion del controlador de interrupciones
; Corre la base de los tipos de interrupción de ambos PICs 8259A de la PC a los 8 tipos consecutivos a 
; partir de los valores base que recibe en BH para el PIC Nº1 y BL para el PIC Nº2.
; A su retorno las Interrupciones de ambos PICs están deshabilitadas.
;--------------------------------------------------------------------------------
InitPIC:
										; Inicialización PIC Nº1
										; ICW1
	mov		al, 11h         			; IRQs activas x flanco, cascada, y ICW4
	out     20h, al  
										; ICW2
	mov     al, bh          			; El PIC Nº1 arranca en INT tipo (BH)
	out     21h, al
										; ICW3
	mov     al, 04h         			; PIC1 Master, Slave ingresa Int.x IRQ2
	out     21h, al
										; ICW4
	mov     al, 01h         			; Modo 8086
	out     21h, al
										; Antes de inicializar el PIC Nº2, deshabilitamos 
										; las Interrupciones del PIC1
	mov     al, 0FFh
	out     21h, al
										; Ahora inicializamos el PIC Nº2
										; ICW1
	mov     al, 11h        			  	; IRQs activas x flanco,cascada, y ICW4
	out     0A0h, al  
										; ICW2
	mov    	al, bl          			; El PIC Nº2 arranca en INT tipo (BL)
	out     0A1h, al
										; ICW3
	mov     al, 02h         			; PIC2 Slave, ingresa Int x IRQ2
	out     0A1h, al
										; ICW4
	mov     al, 01h         			; Modo 8086
	out     0A1h, al
										; Enmascaramos el resto de las Interrupciones 
										; (las del PIC Nº2)
	mov     al, 0FFh
	out     0A1h, al

	;Inicialización del timer del sistema
    mov ebx,100
    call Init_PIT
    
	ret

;/*********************Inicialización del TIMER = 100ms ***********************/
;-------------------------------------------------------------------------------
;7 6 5 4 3 2 1 0   <-- Número de bit de la palabra de control
;| | | | | | | |
;| | | | | | | +-- Modo BCD:
;| | | | | | |     0 - El contador trabajará en formato binario de 16 bits
;| | | | | | |     1 - El contador trabajará en formato BCD con cuatro dígitos 
;| | | | | | |          decimales
;| | | | +-+-+---- Modo de operación para el contador:
;| | | |           000 - Modo 0. Interrupt on Terminal Count (Interrumpe al terminar el conteo)
;| | | |           001 - Modo 1. Hardware Retriggerable One-Shot (Disparo programable)
;| | | |           X10 - Modo 2. Rate Generator (Generador de impulsos). El valor del bit más significativo no importa
;| | | |           X11 - Modo 3. Square Wave(Generador de onda cuadrada). El valor del bit más significativo no importa
;| | | |           100 - Modo 4. Software Triggered Strobe (Strobe disparado por software)
;| | | |           101 - Modo 5. Hardware Triggered Strobe (Retriggerable) (Strobe disparado por hardware)
;| | | |
;| | +-+---------- Modo de acceso (lectura/escritura) para el valor del contador:
;| |               00 - Counter Latch. El valor puede ser leído de la manera en que fue ajustado previamente.
;| |                                   El valor es mantenido hasta que es leído o sobreescrito.
;| |               01 - Lee (o escribe) solo el byte menos significativo del contador (bits 0-7)
;| |               10 - Lee (o escribe) solo el byte más significativo del contador (bits 8-15)
;| |               11 - Primero se lee (o escribe) el byte menos significativo del contador, y luego el byte más significativo
;| |
;+-+-------------- Selección del contador:
;                  00 - Se selecciona el contador 0
;                  01 - Se selecciona el contador 1
;                  10 - Se selecciona el contador 2
;                  11 - No usado. (solo hay 3 contadores)
;                  (Los demás bits de la palabra de control indican cómo será programado el contador seleccionado)

Init_PIT:
    pushad
    pushfd
    
    cli
    
    mov al, 00110110b
    out 0x43, al 			; 43h = Registro de control
    mov ax, 11931			; Los 3 contadores del PIT reciben una señal de clock de 1.19318 MHz 
                            ; 11931 * (1/1.19318 MHz) = 10 interrupciones por segundo
                            ; El intervalo entre interrupciones es de 1 ms.
    out 0x40, al
    mov al, ah
    out 0x40, al 			; 40h = contador 0
 
    popfd
 
    popad
    ret	


;/*********************************************************************************************************************/
;Rutina de inicializacion de esquema de paginacion para tarea 1 (PL=3)
;/*********************************************************************************************************************/
paginacion_tarea1:

	mov edi, INICIO_TABLAS_PAGINACION
	mov ecx, 5*0x400 						; Cantidad de entradas del DTP + Entradas de las TP
	xor eax, eax							; Poner a cero esas entradas
	rep stosd								; Copio lo que tengo en eax a ES:EDI

	; Rutina inicializacion de DTP
	; Atributos: 0x03 => R/W y presente

	; Entrada de la DTP correspondiente a la traduccion de la ROM.
	; Ésta traduccion es igual para todas las tareas
	mov DWORD [INICIO_TABLAS_PAGINACION + 0x3FF*4], INICIO_TP0_TAREA1 + 0x03

	; Primeros 4Mb:
	; ISR, VIDEO, Tablas de sistema, Tablas de paginación
	; Ésta traduccion es igual para todas las tareas
	mov DWORD [INICIO_TABLAS_PAGINACION + 0x0*4], INICIO_TP1_TAREA1 + 0x03

	; Segundos 4Mb. Para paginar el núcleo, tabla de digitos, pila tarea 1, text tarea 1, bss tarea 1, data tarea 1, datos
	mov DWORD [INICIO_TABLAS_PAGINACION + 0x1*4], INICIO_TP2_TAREA1 + 0x07

	; Tabla de pagina para la pila (del sistema)
	mov DWORD [INICIO_TABLAS_PAGINACION + 0x7F*4], INICIO_TP3_TAREA1 + 0x03


	; Rutina de inicializacion de las TP

	; La TP0 voy a usarla para paginar en identity mapping la ROM
	; Como la ROM es de 64KB y cada pagina es de 4KB, necesito inicializar 16 entradas de la TP0

	mov edi, INICIO_TP0_TAREA1 + 0x3F0*4
	mov eax, 0xFFFF0000 + 0x03 		; 0x03: Atributos => presente y R/W
	mov ecx, 16						; Cantidad de entradas a inicializar = 16

	ciclo_tarea1:
		mov [edi], eax		
		add eax, 0x1000
		add edi, 4
		dec ecx
		jnz ciclo_tarea1

	; ------- TP1 --------
	mov DWORD [INICIO_TP1_TAREA1 + 0x0*4],   0x00000000 + 0x03 	; Entrada 0 <= Dir. Física ISR.
	mov DWORD [INICIO_TP1_TAREA1 + 0x010*4], 0x000B8000 + 0x03 	; Entrada 0x010 <= Dir. Fisica VIDEO
	mov DWORD [INICIO_TP1_TAREA1 + 0x100*4], 0x00100000 + 0x03 	; Entrada 0x100 <= Dir. Fisica Tablas del sistema
	;mov DWORD [INICIO_TP1_TAREA1 + 0x110*4], 0x00110000 + 0x03 	; Entrada 0x110 <= Dir. Fisica Tablas Paginación

	mov edi, INICIO_TP1_TAREA1 + 0x110*4
	mov eax, 0x00110000 + 0x07		; 0x03: Atributos => presente y R/W
	mov ecx, 40						; Cantidad de entradas a inicializar = 16

	ciclo_tablas1:
		mov [edi], eax		
		add eax, 0x1000
		add edi, 4
		dec ecx
		jnz ciclo_tablas1

	; Tablas dinamicas (solo 4 tablas a modo de prueba)
	; Para probar codigo de PF
	; Pagino en id mapping de 0x00300000 a 0x00300000 + 4K
	; mov DWORD [INICIO_TP1_TAREA1 + 0x300*4], 0x00300000 + 0x03 
	; mov DWORD [INICIO_TP1_TAREA1 + 0x301*4], 0x00301000 + 0x03 
	; mov DWORD [INICIO_TP1_TAREA1 + 0x302*4], 0x00302000 + 0x03 
	; mov DWORD [INICIO_TP1_TAREA1 + 0x303*4], 0x00303000 + 0x03 

	; TSS y contexto
	mov DWORD [INICIO_TP1_TAREA1 + 0x00D*4], 0x0000D000 + 0x03  ; Entrada 0x00D <= Dir. Fisica TSS
	mov DWORD [INICIO_TP1_TAREA1 + 0x008*4], 0x0000A000 + 0x03  ; Entrada 0x008 <= Dir. Fisica Contexto 1

	; ------- TP2 --------
	mov DWORD [INICIO_TP2_TAREA1 + 0x100*4], 0x00500000 + 0x03     ; Entrada 0x100 <= Dir. Física Nucleo

	mov DWORD [INICIO_TP2_TAREA1 + 0x110*4], 0x00510000 + 0x07 		; Entrada 0x110 <= Dir. Física Tabla digitos
																	; Con esto solo pagino 4K


	mov DWORD [INICIO_TP2_TAREA1 + 0x210*4], 0x00521000 + 0x07  	; Entrada 0x210 <= Dir. Fisica Text Tarea 1
	mov DWORD [INICIO_TP2_TAREA1 + 0x211*4], 0x00522000 + 0x07 		; Entrada 0x211 <= Dir. Fisica Bss Tarea 1
	mov DWORD [INICIO_TP2_TAREA1 + 0x212*4], 0x00523000 + 0x07 		; Entrada 0x212 <= Dir. Fisica Data Atrea 1
	mov DWORD [INICIO_TP2_TAREA1 + 0x1E0*4], 0x005E0000 + 0x07   	; Entrada 0x1E0 <= Dir. Fisica Datos

	mov DWORD [INICIO_TP2_TAREA1 + 0x213*4], 0x1FFFE000 + 0x07 		; Entrada 0x213 <= Dir. Fisica Pila Usuario Tarea 1

	mov DWORD [INICIO_TP2_TAREA1 + 0x214*4], 0x1FFFA000 + 0x03 		; Entrada 0x214 <= Dir. Fisica Pila Nucleo Tarea 1	


	; ------- TP3 --------
	mov DWORD [INICIO_TP3_TAREA1 + 0x3FB*4], 0x1FFFB000 + 0x03       ; Entrada 0 <= Dir. Física pila nucleo

	ret
;/*********************************************************************************************************************/



;/*********************************************************************************************************************/
;Rutina de inicializacion de esquema de paginacion para tarea 2
;/*********************************************************************************************************************/
paginacion_tarea2:

	mov edi, INICIO_DTP_TAREA2
	mov ecx, 5*0x400 						; Cantidad de entradas del DTP + Entradas de las TP
	xor eax, eax							; Poner a cero esas entradas
	rep stosd								; Copio lo que tengo en eax a ES:EDI

	; Rutina inicializacion de DTP
	; Atributos: 0x03 => R/W y presente

	; Entrada de la DTP correspondiente a la traduccion de la ROM.
	; Ésta traduccion es igual para todas las tareas
	mov DWORD [INICIO_DTP_TAREA2 + 0x3FF*4], INICIO_TP0_TAREA2+ 0x03

	; Primeros 4Mb:
	; ISR, VIDEO, Tablas de sistema, Tablas de paginación
	; Ésta traduccion es igual para todas las tareas
	mov DWORD [INICIO_DTP_TAREA2 + 0x0*4], INICIO_TP1_TAREA2 + 0x03

	; Segundos 4Mb. Para paginar el núcleo, tabla de digitos, pila tarea 2, text tarea 2, bss tarea 2, data tarea 2, datos
	mov DWORD [INICIO_DTP_TAREA2 + 0x1*4], INICIO_TP2_TAREA2 + 0x07

	; Tabla de pagina para la pila (del sistema)
	mov DWORD [INICIO_DTP_TAREA2 + 0x7F*4], INICIO_TP3_TAREA2 + 0x03


	; Rutina de inicializacion de las TP

	; La TP0 voy a usarla para paginar en identity mapping la ROM
	; Como la ROM es de 64KB y cada pagina es de 4KB, necesito inicializar 16 entradas de la TP0

	mov edi, INICIO_TP0_TAREA2 + 0x3F0*4
	mov eax, 0xFFFF0000 + 0x03 		; 0x03: Atributos => presente y R/W
	mov ecx, 16						; Cantidad de entradas a inicializar = 16

	ciclo_tarea2:
		mov [edi], eax		
		add eax, 0x1000
		add edi, 4
		dec ecx
		jnz ciclo_tarea2

	; ------- TP1 --------
	mov DWORD [INICIO_TP1_TAREA2 + 0x0*4],   0x00000000 + 0x03 	; Entrada 0 <= Dir. Física ISR.
	mov DWORD [INICIO_TP1_TAREA2 + 0x010*4], 0x000B8000 + 0x03 	; Entrada 0x010 <= Dir. Fisica VIDEO
	mov DWORD [INICIO_TP1_TAREA2 + 0x100*4], 0x00100000 + 0x03 	; Entrada 0x100 <= Dir. Fisica Tablas del sistema
	;mov DWORD [INICIO_TP1_TAREA2 + 0x110*4], 0x00110000 + 0x03 	; Entrada 0x110 <= Dir. Fisica Tablas Paginación

	mov edi, INICIO_TP1_TAREA2 + 0x110*4
	mov eax, 0x00110000 + 0x07 		; 0x03: Atributos => presente y R/W
	mov ecx, 40						; Cantidad de entradas a inicializar = 16

	ciclo_tablas2:
		mov [edi], eax		
		add eax, 0x1000
		add edi, 4
		dec ecx
		jnz ciclo_tablas2

	; Tablas dinamicas (solo 4 tablas a modo de prueba)
	; Para probar codigo de PF
	; Pagino en id mapping de 0x00300000 a 0x00300000 + 4K
	; mov DWORD [INICIO_TP1_TAREA2 + 0x300*4], 0x00300000 + 0x03 
	; mov DWORD [INICIO_TP1_TAREA2 + 0x301*4], 0x00301000 + 0x03 
	; mov DWORD [INICIO_TP1_TAREA2 + 0x302*4], 0x00302000 + 0x03 
	; mov DWORD [INICIO_TP1_TAREA2 + 0x303*4], 0x00303000 + 0x03 

	; TSS y contexto
	mov DWORD [INICIO_TP1_TAREA2 + 0x00D*4], 0x0000D000 + 0x03  ; Entrada 0x00D <= Dir. Fisica TSS
	mov DWORD [INICIO_TP1_TAREA2 + 0x008*4], 0x0000B000 + 0x03  ; Entrada 0x008 <= Dir. Fisica Contexto 2

	; ------- TP2 --------
	mov DWORD [INICIO_TP2_TAREA2 + 0x100*4], 0x00500000 + 0x03     ; Entrada 0x100 <= Dir. Física Nucleo

	mov DWORD [INICIO_TP2_TAREA2 + 0x110*4], 0x00510000 + 0x07 		; Entrada 0x110 <= Dir. Física Tabla digitos
																	; Con esto solo pagino 4K


	mov DWORD [INICIO_TP2_TAREA2 + 0x210*4], 0x00531000 + 0x07  	; Entrada 0x210 <= Dir. Fisica Text Tarea 2
	mov DWORD [INICIO_TP2_TAREA2 + 0x211*4], 0x00532000 + 0x07 		; Entrada 0x211 <= Dir. Fisica Bss Tarea 2
	mov DWORD [INICIO_TP2_TAREA2 + 0x212*4], 0x00533000 + 0x07 		; Entrada 0x212 <= Dir. Fisica Data Atrea 2
	mov DWORD [INICIO_TP2_TAREA2 + 0x1E0*4], 0x005E0000 + 0x03   	; Entrada 0x1E0 <= Dir. Fisica Datos

	mov DWORD [INICIO_TP2_TAREA2 + 0x213*4], 0x1FFFD000 + 0x07 		; Entrada 0x213 <= Dir. Fisica Pila Usuario Tarea 2

	mov DWORD [INICIO_TP2_TAREA2 + 0x214*4], 0x1FFF9000 + 0x03 		; Entrada 0x214 <= Dir. Fisica Pila Nucleo Tarea 2	


	; ------- TP3 --------
	mov DWORD [INICIO_TP3_TAREA2 + 0x3FB*4], 0x1FFFB000 + 0x03       ; Entrada 0 <= Dir. Física pila nucleo

	ret
;/*********************************************************************************************************************/


;/*********************************************************************************************************************/
;Rutina de inicializacion de esquema de paginacion para tarea 0 / idle
;/*********************************************************************************************************************/
paginacion_idle:

	mov edi, INICIO_DTP_TAREA0
	mov ecx, 5*0x400 						; Cantidad de entradas del DTP + Entradas de las TP
	xor eax, eax							; Poner a cero esas entradas
	rep stosd								; Copio lo que tengo en eax a ES:EDI

	; Rutina inicializacion de DTP
	; Atributos: 0x03 => R/W y presente

	; Entrada de la DTP correspondiente a la traduccion de la ROM.
	; Ésta traduccion es igual para todas las tareas
	mov DWORD [INICIO_DTP_TAREA0 + 0x3FF*4], INICIO_TP0_TAREA0+ 0x03

	; Primeros 4Mb:
	; ISR, VIDEO, Tablas de sistema, Tablas de paginación
	; Ésta traduccion es igual para todas las tareas
	mov DWORD [INICIO_DTP_TAREA0 + 0x0*4], INICIO_TP1_TAREA0 + 0x03

	; Segundos 4Mb. Para paginar el núcleo, tabla de digitos, pila tarea 2, text tarea 2, bss tarea 2, data tarea 2, datos
	mov DWORD [INICIO_DTP_TAREA0 + 0x1*4], INICIO_TP2_TAREA0 + 0x03

	; Tabla de pagina para la pila (del sistema)
	mov DWORD [INICIO_DTP_TAREA0 + 0x7F*4], INICIO_TP3_TAREA0 + 0x03


	; Rutina de inicializacion de las TP

	; La TP0 voy a usarla para paginar en identity mapping la ROM
	; Como la ROM es de 64KB y cada pagina es de 4KB, necesito inicializar 16 entradas de la TP0

	mov edi, INICIO_TP0_TAREA0 + 0x3F0*4
	mov eax, 0xFFFF0000 + 0x03 		; 0x03: Atributos => presente y R/W
	mov ecx, 16						; Cantidad de entradas a inicializar = 16

	ciclo_tarea0:
		mov [edi], eax		
		add eax, 0x1000
		add edi, 4
		dec ecx
		jnz ciclo_tarea0

	; ------- TP1 --------
	mov DWORD [INICIO_TP1_TAREA0 + 0x0*4],   0x00000000 + 0x03 	; Entrada 0 <= Dir. Física ISR.
	mov DWORD [INICIO_TP1_TAREA0 + 0x010*4], 0x000B8000 + 0x03 	; Entrada 0x010 <= Dir. Fisica VIDEO
	mov DWORD [INICIO_TP1_TAREA0 + 0x100*4], 0x00100000 + 0x03 	; Entrada 0x100 <= Dir. Fisica Tablas del sistema
	;mov DWORD [INICIO_TP1_TAREA0 + 0x110*4], 0x00110000 + 0x03 	; Entrada 0x110 <= Dir. Fisica Tablas Paginación

	mov edi, INICIO_TP1_TAREA0 + 0x110*4
	mov eax, 0x00110000 + 0x03
	mov ecx, 40

	ciclo_tablas0:
		mov [edi], eax		
		add eax, 0x1000
		add edi, 4
		dec ecx
		jnz ciclo_tablas0


	; Tablas dinamicas (solo 4 tablas a modo de prueba)
	; Para probar codigo de PF
	; Pagino en id mapping de 0x00300000 a 0x00300000 + 4K
	; mov DWORD [INICIO_TP1_TAREA0 + 0x300*4], 0x00300000 + 0x03 
	; mov DWORD [INICIO_TP1_TAREA0 + 0x301*4], 0x00301000 + 0x03 
	; mov DWORD [INICIO_TP1_TAREA0 + 0x302*4], 0x00302000 + 0x03 
	; mov DWORD [INICIO_TP1_TAREA0 + 0x303*4], 0x00303000 + 0x03 


	; TSS y contexto
	mov DWORD [INICIO_TP1_TAREA0 + 0x00D*4], 0x0000D000 + 0x03  ; Entrada 0x00D <= Dir. Fisica TSS
	mov DWORD [INICIO_TP1_TAREA0 + 0x008*4], 0x00009000 + 0x03  ; Entrada 0x008 <= Dir. Fisica Contexto 0

	; ------- TP2 --------
	mov DWORD [INICIO_TP2_TAREA0 + 0x100*4], 0x00500000 + 0x03     ; Entrada 0x100 <= Dir. Física Nucleo

	mov DWORD [INICIO_TP2_TAREA0 + 0x110*4], 0x00510000 + 0x03 		; Entrada 0x110 <= Dir. Física Tabla digitos
																	; Con esto solo pagino 4K


	mov DWORD [INICIO_TP2_TAREA0 + 0x210*4], 0x00501000 + 0x03  	; Entrada 0x210 <= Dir. Fisica Text Tarea 0
	mov DWORD [INICIO_TP2_TAREA0 + 0x211*4], 0x00502000 + 0x03 		; Entrada 0x211 <= Dir. Fisica Bss Tarea 0
	mov DWORD [INICIO_TP2_TAREA0 + 0x212*4], 0x00503000 + 0x03 		; Entrada 0x212 <= Dir. Fisica Data Atrea 0
	mov DWORD [INICIO_TP2_TAREA0 + 0x1E0*4], 0x005E0000 + 0x03   	; Entrada 0x1E0 <= Dir. Fisica Datos

	mov DWORD [INICIO_TP2_TAREA0 + 0x213*4], 0x1FFFC000 + 0x03 		; Entrada 0x213 <= Dir. Fisica Pila Usuario Tarea 0

	mov DWORD [INICIO_TP2_TAREA0 + 0x214*4], 0x1FFF8000 + 0x03 		; Entrada 0x214 <= Dir. Fisica Pila Nucleo Tarea 0	


	; ------- TP3 --------
	mov DWORD [INICIO_TP3_TAREA0 + 0x3FB*4], 0x1FFFB000 + 0x03       ; Entrada 0 <= Dir. Física pila nucleo

	ret
;/*********************************************************************************************************************/


;/*********************************************************************************************************************/
;Rutina de inicializacion de esquema de paginacion para tarea 3
;/*********************************************************************************************************************/
paginacion_tarea3:
	mov edi, INICIO_DTP_TAREA3
	mov ecx, 5*0x400 						; Cantidad de entradas del DTP + Entradas de las TP
	xor eax, eax							; Poner a cero esas entradas
	rep stosd								; Copio lo que tengo en eax a ES:EDI

	; Rutina inicializacion de DTP
	; Atributos: 0x03 => R/W y presente

	; Entrada de la DTP correspondiente a la traduccion de la ROM.
	; Ésta traduccion es igual para todas las tareas
	mov DWORD [INICIO_DTP_TAREA3 + 0x3FF*4], INICIO_TP0_TAREA3+ 0x03

	; Primeros 4Mb:
	; ISR, VIDEO, Tablas de sistema, Tablas de paginación
	; Ésta traduccion es igual para todas las tareas
	mov DWORD [INICIO_DTP_TAREA3 + 0x0*4], INICIO_TP1_TAREA3 + 0x03

	; Segundos 4Mb. Para paginar el núcleo, tabla de digitos, pila tarea 3, text tarea 3, bss tarea 3, data tarea 3, datos
	mov DWORD [INICIO_DTP_TAREA3 + 0x1*4], INICIO_TP2_TAREA3 + 0x07

	; Tabla de pagina para la pila (del sistema)
	mov DWORD [INICIO_DTP_TAREA3 + 0x7F*4], INICIO_TP3_TAREA3 + 0x03


	; Rutina de inicializacion de las TP

	; La TP0 voy a usarla para paginar en identity mapping la ROM
	; Como la ROM es de 64KB y cada pagina es de 4KB, necesito inicializar 16 entradas de la TP0

	mov edi, INICIO_TP0_TAREA3 + 0x3F0*4
	mov eax, 0xFFFF0000 + 0x03 		; 0x03: Atributos => presente y R/W
	mov ecx, 16						; Cantidad de entradas a inicializar = 16

	ciclo_tarea3:
		mov [edi], eax		
		add eax, 0x1000
		add edi, 4
		dec ecx
		jnz ciclo_tarea3

	; ------- TP1 --------
	mov DWORD [INICIO_TP1_TAREA3 + 0x0*4],   0x00000000 + 0x03 	; Entrada 0 <= Dir. Física ISR.
	mov DWORD [INICIO_TP1_TAREA3 + 0x010*4], 0x000B8000 + 0x03 	; Entrada 0x010 <= Dir. Fisica VIDEO
	mov DWORD [INICIO_TP1_TAREA3 + 0x100*4], 0x00100000 + 0x03 	; Entrada 0x100 <= Dir. Fisica Tablas del sistema
	;mov DWORD [INICIO_TP1_TAREA2 + 0x110*4], 0x00110000 + 0x03 	; Entrada 0x110 <= Dir. Fisica Tablas Paginación

	mov edi, INICIO_TP1_TAREA3 + 0x110*4
	mov eax, 0x00110000 + 0x07 		; 0x03: Atributos => presente y R/W
	mov ecx, 40						; Cantidad de entradas a inicializar = 16

	ciclo_tablas3:
		mov [edi], eax		
		add eax, 0x1000
		add edi, 4
		dec ecx
		jnz ciclo_tablas3

	; Tablas dinamicas (solo 4 tablas a modo de prueba)
	; Para probar codigo de PF
	; Pagino en id mapping de 0x00300000 a 0x00300000 + 4K
	; mov DWORD [INICIO_TP1_TAREA3 + 0x300*4], 0x00300000 + 0x03 
	; mov DWORD [INICIO_TP1_TAREA3 + 0x301*4], 0x00301000 + 0x03 
	; mov DWORD [INICIO_TP1_TAREA3 + 0x302*4], 0x00302000 + 0x03 
	; mov DWORD [INICIO_TP1_TAREA3 + 0x303*4], 0x00303000 + 0x03 

	; TSS y contexto
	mov DWORD [INICIO_TP1_TAREA3 + 0x00D*4], 0x0000D000 + 0x03  ; Entrada 0x00D <= Dir. Fisica TSS
	mov DWORD [INICIO_TP1_TAREA3 + 0x008*4], 0x0000C000 + 0x03  ; Entrada 0x008 <= Dir. Fisica Contexto 3

	; ------- TP2 --------
	mov DWORD [INICIO_TP2_TAREA3 + 0x100*4], 0x00500000 + 0x03     ; Entrada 0x100 <= Dir. Física Nucleo

	mov DWORD [INICIO_TP2_TAREA3 + 0x110*4], 0x00510000 + 0x07 		; Entrada 0x110 <= Dir. Física Tabla digitos
																	; Con esto solo pagino 4K


	mov DWORD [INICIO_TP2_TAREA3 + 0x210*4], 0x00541000 + 0x07  	; Entrada 0x210 <= Dir. Fisica Text Tarea 3
	mov DWORD [INICIO_TP2_TAREA3 + 0x211*4], 0x00542000 + 0x07 		; Entrada 0x211 <= Dir. Fisica Bss Tarea 3
	mov DWORD [INICIO_TP2_TAREA3 + 0x212*4], 0x00543000 + 0x07 		; Entrada 0x212 <= Dir. Fisica Data Atrea 3
	mov DWORD [INICIO_TP2_TAREA3 + 0x1E0*4], 0x005E0000 + 0x07   	; Entrada 0x1E0 <= Dir. Fisica Datos

	mov DWORD [INICIO_TP2_TAREA3 + 0x213*4], 0x1FF0E000 + 0x07 		; Entrada 0x213 <= Dir. Fisica Pila Usuario Tarea 3

	mov DWORD [INICIO_TP2_TAREA3 + 0x214*4], 0x1FF01000 + 0x03 		; Entrada 0x214 <= Dir. Fisica Pila Nucleo Tarea 3	


	; ------- TP3 --------
	mov DWORD [INICIO_TP3_TAREA3 + 0x3FB*4], 0x1FFFB000 + 0x03       ; Entrada 0 <= Dir. Física pila nucleo

	ret
;/*********************************************************************************************************************/



;/*********************************************************************************************************************/
;Rutina de inicializacion de esquema de paginacion para tarea 4
;/*********************************************************************************************************************/
paginacion_tarea4:

	mov edi, INICIO_DTP_TAREA4
	mov ecx, 5*0x400 						; Cantidad de entradas del DTP + Entradas de las TP
	xor eax, eax							; Poner a cero esas entradas
	rep stosd								; Copio lo que tengo en eax a ES:EDI

	; Rutina inicializacion de DTP
	; Atributos: 0x03 => R/W y presente

	; Entrada de la DTP correspondiente a la traduccion de la ROM.
	; Ésta traduccion es igual para todas las tareas
	mov DWORD [INICIO_DTP_TAREA4 + 0x3FF*4], INICIO_TP0_TAREA4+ 0x03

	; Primeros 4Mb:
	; ISR, VIDEO, Tablas de sistema, Tablas de paginación
	; Ésta traduccion es igual para todas las tareas
	mov DWORD [INICIO_DTP_TAREA4 + 0x0*4], INICIO_TP1_TAREA4 + 0x03

	; Segundos 4Mb. Para paginar el núcleo, tabla de digitos, pila tarea 4, text tarea 4, bss tarea 4, data tarea 4, datos
	mov DWORD [INICIO_DTP_TAREA4 + 0x1*4], INICIO_TP2_TAREA4 + 0x07

	; Tabla de pagina para la pila (del sistema)
	mov DWORD [INICIO_DTP_TAREA4 + 0x7F*4], INICIO_TP3_TAREA4 + 0x03


	; Rutina de inicializacion de las TP

	; La TP0 voy a usarla para paginar en identity mapping la ROM
	; Como la ROM es de 64KB y cada pagina es de 4KB, necesito inicializar 16 entradas de la TP0

	mov edi, INICIO_TP0_TAREA4 + 0x3F0*4
	mov eax, 0xFFFF0000 + 0x03 		; 0x03: Atributos => presente y R/W
	mov ecx, 16						; Cantidad de entradas a inicializar = 16

	ciclo_tarea4:
		mov [edi], eax		
		add eax, 0x1000
		add edi, 4
		dec ecx
		jnz ciclo_tarea4

	; ------- TP1 --------
	mov DWORD [INICIO_TP1_TAREA4 + 0x0*4],   0x00000000 + 0x03 	; Entrada 0 <= Dir. Física ISR.
	mov DWORD [INICIO_TP1_TAREA4 + 0x010*4], 0x000B8000 + 0x03 	; Entrada 0x010 <= Dir. Fisica VIDEO
	mov DWORD [INICIO_TP1_TAREA4 + 0x100*4], 0x00100000 + 0x03 	; Entrada 0x100 <= Dir. Fisica Tablas del sistema
	;mov DWORD [INICIO_TP1_TAREA2 + 0x110*4], 0x00110000 + 0x03 	; Entrada 0x110 <= Dir. Fisica Tablas Paginación

	mov edi, INICIO_TP1_TAREA4 + 0x110*4
	mov eax, 0x00110000 + 0x07 		; 0x03: Atributos => presente y R/W
	mov ecx, 40						; Cantidad de entradas a inicializar = 16

	ciclo_tablas4:
		mov [edi], eax		
		add eax, 0x1000
		add edi, 4
		dec ecx
		jnz ciclo_tablas4

	; Tablas dinamicas (solo 4 tablas a modo de prueba)
	; Para probar codigo de PF
	; Pagino en id mapping de 0x00300000 a 0x00300000 + 4K
	; mov DWORD [INICIO_TP1_TAREA4 + 0x300*4], 0x00300000 + 0x03 
	; mov DWORD [INICIO_TP1_TAREA4 + 0x301*4], 0x00301000 + 0x03 
	; mov DWORD [INICIO_TP1_TAREA4 + 0x302*4], 0x00302000 + 0x03 
	; mov DWORD [INICIO_TP1_TAREA4 + 0x303*4], 0x00303000 + 0x03 

	; TSS y contexto
	mov DWORD [INICIO_TP1_TAREA4 + 0x00D*4], 0x0000D000 + 0x03  ; Entrada 0x00D <= Dir. Fisica TSS
	mov DWORD [INICIO_TP1_TAREA4 + 0x008*4], 0x0000E000 + 0x03  ; Entrada 0x008 <= Dir. Fisica Contexto 4

	; ------- TP2 --------
	mov DWORD [INICIO_TP2_TAREA4 + 0x100*4], 0x00500000 + 0x03     ; Entrada 0x100 <= Dir. Física Nucleo

	mov DWORD [INICIO_TP2_TAREA4 + 0x110*4], 0x00510000 + 0x07 		; Entrada 0x110 <= Dir. Física Tabla digitos
																	; Con esto solo pagino 4K


	mov DWORD [INICIO_TP2_TAREA4 + 0x210*4], 0x00551000 + 0x07  	; Entrada 0x210 <= Dir. Fisica Text Tarea 4
	mov DWORD [INICIO_TP2_TAREA4 + 0x211*4], 0x00552000 + 0x07 		; Entrada 0x211 <= Dir. Fisica Bss Tarea 4
	mov DWORD [INICIO_TP2_TAREA4 + 0x212*4], 0x00553000 + 0x07 		; Entrada 0x212 <= Dir. Fisica Data Atrea 4
	mov DWORD [INICIO_TP2_TAREA4 + 0x1E0*4], 0x005E0000 + 0x07   	; Entrada 0x1E0 <= Dir. Fisica Datos

	mov DWORD [INICIO_TP2_TAREA4 + 0x213*4], 0x1FF0F000 + 0x07 		; Entrada 0x213 <= Dir. Fisica Pila Usuario Tarea 4

	mov DWORD [INICIO_TP2_TAREA4 + 0x214*4], 0x1FF02000 + 0x03 		; Entrada 0x214 <= Dir. Fisica Pila Nucleo Tarea 4	


	; ------- TP3 --------
	mov DWORD [INICIO_TP3_TAREA4 + 0x3FB*4], 0x1FFFB000 + 0x03       ; Entrada 0 <= Dir. Física pila nucleo

	ret
;/*********************************************************************************************************************/


;/*********************************************************************************************************************/
;Rutina de inicializacion de contextos para las tareas
;/*********************************************************************************************************************/
inicializacion_contextos:

	pushad													; Salvo registros de proposito general

	; pongo la direccion lineal de los stacks (todas iguales)
	; El Length es 4k (0x1000) - 1 = 0xFFF					

	;TAREA 0 - IDLE
	mov eax, dir_lineal_pila_nucleo_tarea0
	add eax, 0xFFF 											;Muevo al puntero del stack al final, es decir le sumo 4K(0x1000) y le resto 1
	mov ecx, dir_fisica_contexto0
	mov dword[ecx + OFFSET_ESP0], eax						;Direccion inicial de la pila de la tarea idle
	mov dword[ecx + OFFSET_SS0], DS_SEL_32					;Cargamos SS
	mov dword[ecx + OFFSET_CR3], INICIO_DTP_TAREA0			;Valor de CR3 para tarea idle (es el directorio)
	mov dword[ecx + OFFSET_EIP], main						;EIP apunta al inicio del codigo de la tarea idle
	mov dword[ecx + OFFSET_EFLAGS], 0x200					;Con unicamente IF activo (interrupciones habilitadas)
	mov dword[ecx + OFFSET_ES], DS_SEL_32					;ES
	mov dword[ecx + OFFSET_DS], DS_SEL_32					;DS
	mov dword[ecx + OFFSET_SS], DS_SEL_32					;SS
	mov dword[ecx + OFFSET_CS], CS_SEL_32					;CS
	
	mov dword[ecx + OFFSET_PRIM_EJEC], 0					;Es primer ejecucion
	mov dword[ecx + OFFSET_SIG_CR3], INICIO_DTP_TAREA1		;Siguiente => tarea 1

	
	; TAREA 1
	mov eax, dir_lineal_pila_nucleo_tarea1
	add eax, 0xFFF											;Pila de nivel 0 de la tarea 1
	mov ecx, dir_fisica_contexto1
	mov dword[ecx + OFFSET_ESP0], eax						;Direccion inicial de la pila de la tarea1
	mov dword[ecx + OFFSET_SS0], DS_SEL_32					;Cargamos SS en nivel 0

	mov eax, dir_lineal_pila_usuario_tarea1
	add eax, 0xFFF
	mov dword[ecx + OFFSET_ESP], eax  						;Direccion inicial de la pila de nivel 3
	mov dword[ecx + OFFSET_SS], DS_SEL_PL3


	mov dword[ecx + OFFSET_CR3], INICIO_DTP_TAREA1			;Valor de CR3 para tarea 1
	mov dword[ecx + OFFSET_EIP], tarea_1					;EIP apunta al inicio del codigo de la tarea 1
	mov dword[ecx + OFFSET_EFLAGS], 0x200					;Con unicamente IF activo (interrupciones habilitadas)
	mov dword[ecx + OFFSET_ES], DS_SEL_PL3					;ES
	mov dword[ecx + OFFSET_DS], DS_SEL_PL3					;DS
	mov dword[ecx + OFFSET_SS], DS_SEL_PL3					;SS
	mov dword[ecx + OFFSET_CS], CS_SEL_PL3					;CS
	
	mov dword[ecx + OFFSET_PRIM_EJEC], 0					;Es primer ejecucion
	mov dword[ecx + OFFSET_SIG_CR3], INICIO_DTP_TAREA2		;Siguiente => tarea 2
	
	
	; TAREA 2
	mov eax, dir_lineal_pila_nucleo_tarea2
	add eax, 0xFFF 											;Muevo al puntero abajo de todo (le sumo 4k que es 0x1000 y le resto 1)
	mov ecx, dir_fisica_contexto2
	mov dword[ecx + OFFSET_ESP0], eax						;Direccion inicial de la pila de la tarea2
	mov dword[ecx + OFFSET_SS0], DS_SEL_32					;Cargamos SS
	
	mov eax, dir_lineal_pila_usuario_tarea2
	add eax, 0xFFF
	mov dword[ecx + OFFSET_ESP], eax  						;Direccion inicial de la pila de nivel 3
	mov dword[ecx + OFFSET_SS], DS_SEL_PL3


	mov dword[ecx + OFFSET_CR3], INICIO_DTP_TAREA2			;Valor de CR3 para tarea 2
	mov dword[ecx + OFFSET_EIP], tarea_2					;EIP apunta al inicio del codigo de la tarea 2
	mov dword[ecx + OFFSET_EFLAGS], 0x200					;Con unicamente IF activo (interrupciones habilitadas)
	mov dword[ecx + OFFSET_ES], DS_SEL_PL3					;ES
	mov dword[ecx + OFFSET_DS], DS_SEL_PL3					;DS
	mov dword[ecx + OFFSET_SS], DS_SEL_PL3					;SS
	mov dword[ecx + OFFSET_CS], CS_SEL_PL3					;CS
	
	mov dword[ecx + OFFSET_PRIM_EJEC], 0					;Es primer ejecucion
	mov dword[ecx + OFFSET_SIG_CR3], INICIO_DTP_TAREA3		;Siguiente => tarea 3


	; TAREA 3
	mov eax, dir_lineal_pila_nucleo_tarea3
	add eax, 0xFFF 											;Muevo al puntero abajo de todo (le sumo 4k que es 0x1000 y le resto 1)
	mov ecx, dir_fisica_contexto3
	mov dword[ecx + OFFSET_ESP0], eax						;Direccion inicial de la pila de la tarea3
	mov dword[ecx + OFFSET_SS0], DS_SEL_32					;Cargamos SS
	
	mov eax, dir_lineal_pila_usuario_tarea3
	add eax, 0xFFF
	mov dword[ecx + OFFSET_ESP], eax  						;Direccion inicial de la pila de nivel 3
	mov dword[ecx + OFFSET_SS], DS_SEL_PL3


	mov dword[ecx + OFFSET_CR3], INICIO_DTP_TAREA3			;Valor de CR3 para tarea 3
	mov dword[ecx + OFFSET_EIP], tarea_3					;EIP apunta al inicio del codigo de la tarea 3
	mov dword[ecx + OFFSET_EFLAGS], 0x200					;Con unicamente IF activo (interrupciones habilitadas)
	mov dword[ecx + OFFSET_ES], DS_SEL_PL3					;ES
	mov dword[ecx + OFFSET_DS], DS_SEL_PL3					;DS
	mov dword[ecx + OFFSET_SS], DS_SEL_PL3					;SS
	mov dword[ecx + OFFSET_CS], CS_SEL_PL3					;CS
	
	mov dword[ecx + OFFSET_PRIM_EJEC], 0					;Es primer ejecucion
	mov dword[ecx + OFFSET_SIG_CR3], INICIO_DTP_TAREA4		;Siguiente => tarea 4


	; TAREA 4
	mov eax, dir_lineal_pila_nucleo_tarea4
	add eax, 0xFFF 											;Muevo al puntero abajo de todo (le sumo 4k que es 0x1000 y le resto 1)
	mov ecx, dir_fisica_contexto4
	mov dword[ecx + OFFSET_ESP0], eax						;Direccion inicial de la pila de la tarea4
	mov dword[ecx + OFFSET_SS0], DS_SEL_32					;Cargamos SS
	
	mov eax, dir_lineal_pila_usuario_tarea4
	add eax, 0xFFF
	mov dword[ecx + OFFSET_ESP], eax  						;Direccion inicial de la pila de nivel 3
	mov dword[ecx + OFFSET_SS], DS_SEL_PL3


	mov dword[ecx + OFFSET_CR3], INICIO_DTP_TAREA4			;Valor de CR3 para tarea 4
	mov dword[ecx + OFFSET_EIP], tarea_4					;EIP apunta al inicio del codigo de la tarea 4
	mov dword[ecx + OFFSET_EFLAGS], 0x200					;Con unicamente IF activo (interrupciones habilitadas)
	mov dword[ecx + OFFSET_ES], DS_SEL_PL3					;ES
	mov dword[ecx + OFFSET_DS], DS_SEL_PL3					;DS
	mov dword[ecx + OFFSET_SS], DS_SEL_PL3					;SS
	mov dword[ecx + OFFSET_CS], CS_SEL_PL3					;CS
	
	mov dword[ecx + OFFSET_PRIM_EJEC], 0					;Es primer ejecucion
	mov dword[ecx + OFFSET_SIG_CR3], INICIO_DTP_TAREA1		;Siguiente => tarea 1

	
	;xchg bx,bx
	;TSS inicial, de arranque a tarea idle
	mov eax, dir_lineal_pila_nucleo_tarea0
	add eax, 0xFFF
	mov ecx, TSS_principal

	mov dword[ecx + OFFSET_ESP], eax
	mov dword[ecx + OFFSET_ESP0], eax					;Direccion inicial de la pila de la tarea1
	mov dword[ecx + OFFSET_SS0], DS_SEL_32				;Cargamos SS
	mov dword[ecx + OFFSET_CR3], INICIO_DTP_TAREA0		;Valor de CR3 para tarea 1
	mov dword[ecx + OFFSET_EIP], main					;EIP apunta al inicio del codigo de la tarea 1
	mov dword[ecx + OFFSET_EFLAGS], 0x200				;Con unicamente IF activo (interrupciones habilitadas)
	mov dword[ecx + OFFSET_ES], DS_SEL_32				;ES
	mov dword[ecx + OFFSET_DS], DS_SEL_32				;DS
	mov dword[ecx + OFFSET_CS], CS_SEL_32				;CS
	mov dword[ecx + OFFSET_SS], DS_SEL_32

	popad
	ret

