SECTION .datos

INDICE_TABLA          dd      0
BUFFER                dd      0
CONTADOR_BUFFER       dd      0


Tarea1:                 db 'Tarea 1: ',0
length_msg_T1           equ $-Tarea1

Tarea2:                 db 'Tarea 2: ',0
length_msg_T2           equ $-Tarea2

Tarea3:                 db 'Tarea 3: ',0
length_msg_T3           equ $-Tarea3

Tarea4:                 db 'Tarea 4: ',0
length_msg_T4           equ $-Tarea4

; para probar codigo de PF
;num_tabla_dinamica:    db      0


SECTION .data_tarea0

GLOBAL main
GLOBAL tarea_1
GLOBAL tarea_2
GLOBAL tarea_3 
GLOBAL tarea_4

GLOBAL EXC_handler_0
GLOBAL EXC_handler_1
GLOBAL EXC_handler_2
GLOBAL EXC_handler_3
GLOBAL EXC_handler_4
GLOBAL EXC_handler_5
GLOBAL EXC_handler_6
GLOBAL EXC_handler_7
GLOBAL EXC_handler_8
GLOBAL EXC_handler_9
GLOBAL EXC_handler_10
GLOBAL EXC_handler_11
GLOBAL EXC_handler_12
GLOBAL EXC_handler_13
GLOBAL EXC_handler_14
GLOBAL EXC_handler_15
GLOBAL EXC_handler_16
GLOBAL EXC_handler_17
GLOBAL EXC_handler_18
GLOBAL EXC_handler_19

GLOBAL IRQ_handler_0
GLOBAL IRQ_handler_1
GLOBAL IRQ_handler_2
GLOBAL IRQ_handler_3
GLOBAL IRQ_handler_4
GLOBAL IRQ_handler_5
GLOBAL IRQ_handler_6
GLOBAL IRQ_handler_7
GLOBAL IRQ_handler_8
GLOBAL IRQ_handler_9
GLOBAL IRQ_handler_10
GLOBAL IRQ_handler_11
GLOBAL IRQ_handler_12
GLOBAL IRQ_handler_13
GLOBAL IRQ_handler_14
GLOBAL IRQ_handler_15
GLOBAL system_call


EXTERN      Excepcion_0
EXTERN		dir_lineal_VIDEO
EXTERN   	dir_lineal_datos
EXTERN      dir_contexto_SIMD
EXTERN      SEL_SYSTEM_CALL
EXTERN		dir_lineal_digtable
EXTERN      busqueda_maximo_c

INICIO_TABLA	equ		dir_lineal_digtable
BUFFER_TABLA	equ		dir_lineal_digtable + 0x1000
FIN_TABLA		equ		BUFFER_TABLA + 0x1000
CONTADOR_TABLA 	equ		FIN_TABLA + 0x1000
 

;/*********************************************************************************************************************/
;Tarea 0 - IDLE
;/*********************************************************************************************************************/
main:

    xchg bx,bx
    mov eax, 0x7A5C0000     ; Debug pongo en eax = TASK0000 = task 0 - idle


	idle:
		hlt
		jmp idle


;/*********************************************************************************************************************/
;Tarea 1 - PL=3 - Imprime en pantalla la sumatoria de la tabla de digitos (SIMD paddq)
;/*********************************************************************************************************************/
SECTION .data_tarea1
tarea_1:

    ;xchg bx,bx
    loop_tarea_1:
    mov eax, 0x7A5C0001

	xor eax, eax 				; Limpio eax. Aca voy a poner el puntero a la tabla y lo voy a ir moviendo para recorrerla
	xor ecx, ecx 				; Limpio ecx. Contador para manejo del loop
	xor ebx, ebx 				; Variable donde voy a guardar la sumatoria de los digitos de la tabla

	mov eax, INICIO_TABLA 	    ; Guardo en eax el puntero a la tabla
	mov ecx, 1023   			; Inicializo contador para el manejo del loop 
	 							; En una tabla de 4KB, entran 0x400 numeros de 4 bytes (es decir 1024)
	mov ebx, [INICIO_TABLA]     ; Pongo en la variable sumatoria el valor del primer elemento de la tabla 
	movd mm1, ebx
    sumatoria:
		add eax, 0x04           ; Incremento el puntero 
        movd mm2, [eax]
        paddq mm1, mm2          ; Guardo el valor de la entrada correspondiente en la sumatoria 

        dec ecx                 ; Decremento ecx 
        jnz sumatoria           ; Si ecx es cero -> Termine el loop 

    movd edx, mm1               ; Parametro: numero a imprimir
    mov ebx,0x00020700          ; Parametro: Atributos de pantalla ->Fila=2, Columna=0, Blanco sobre negro
    mov ecx,Tarea1              ; Parametro: texto de tarea a imprimir
    call SEL_SYSTEM_CALL:0
    jmp loop_tarea_1


;/*********************************************************************************************************************/
; Tarea 2 - PL=3 - Imprime en pantalla la sumatoria de la tabla de digitos (SIMD paddw)
;/*********************************************************************************************************************/
SECTION     .data_tarea2         progbits

tarea_2:

    ;xchg bx,bx
    loop_tarea_2:
    mov eax, 0x7A5C0002         ; debug, TASK 2
    xor eax, eax                ; Limpio eax. Aca voy a poner el puntero a la tabla y lo voy a ir moviendo para recorrerla
    xor ecx, ecx                ; Limpio ecx. Contador para manejo del loop
    xor ebx, ebx                ; Variable donde voy a guardar la sumatoria de los digitos de la tabla

    mov eax, INICIO_TABLA       ; Guardo en eax el puntero a la tabla
    mov ecx, 1023               ; Inicializo contador para el manejo del loop 
                                ; En una tabla de 4KB, entran 0x400 numeros de 4 bytes (es decir 1024)
    
    mov ebx, [INICIO_TABLA]     ; Pongo en la variable sumatoria el valor del primer elemento de la tabla
    movd mm1, ebx 
    sumatoria_2:
        add eax, 0x04           ; Incremento el puntero 
        movd mm2, [eax]
        paddw mm1, mm2          ; Guardo el valor de la entrada correspondiente en la sumatoria 

        dec ecx                 ; Decremento ecx 
        jnz sumatoria_2         ; Si ecx es cero -> Termine el loop   

    movd edx, mm1               ; Parametro: numero a imprimir
    mov ebx,0x00040700          ; Fila=4, Columna=0, Blanco sobre negro
    mov ecx,Tarea2              ; Parametro: texto de tarea a imprimir
    call SEL_SYSTEM_CALL:0
    jmp loop_tarea_2


;/*********************************************************************************************************************/
; Tarea 3 - PL=3
;/*********************************************************************************************************************/

SECTION     .data_tarea3         progbits

tarea_3:

    ;xchg bx,bx
    loop_tarea_3:
    
    ;-> Guardo del tiempo de ejecucion actual
    rdtsc
    mov [task3_exec_low_st], eax        ; Parte baja del time counter
    mov [task3_exec_high_st], edx       ; Parte alta del time counter

    calculo_simd:
        push ebp
        mov ebp,esp
        push 1024                       ; Parametro 2: Long del buffer
        push INICIO_TABLA               ; Parametro 1: Puntero a la tabla de digitos
        call busqueda_maximo
        mov esp, ebp
        pop ebp

    ; Para imprimir el maximo en pantalla descomentar este bloque de codigo
    ; Y comentar el que imprime tiempo de ejecucion (ver abajo)
    ; mov edx, eax 
    ; mov ebx,0x00060700          ; Fila=4, Columna=0, Blanco sobre negro
    ; mov ecx,Tarea3              ; Parametro: texto de tarea a imprimir
    ; call SEL_SYSTEM_CALL:0
    ;----------------------------------------------------------------------

    ;-> Guardo tiempo de ejecucion actual
    rdtsc
    mov [task3_exec_low_end], eax
    mov [task3_exec_high_end], edx

    ;-> Tiempo de ejecucion de la funcion, asumiendo partes altas iguales
    mov ebx, [task3_exec_low_st]
    sub eax, ebx

    ; Con esto imprimo tiempo de ejecucion
    mov edx, eax 
    mov ebx,0x00060700          ; Fila=4, Columna=0, Blanco sobre negro
    mov ecx,Tarea3              ; Parametro: texto de tarea a imprimir
    call SEL_SYSTEM_CALL:0
    ;-------------------------------------------------------------------
    jmp loop_tarea_3

busqueda_maximo:
    mov eax, [ebp-8]
    mov ecx, [ebp-4]

    push ebx
    xor ebx,ebx

    movd xmm1, [eax]
    busqueda_maximo_loop:
        ;xor edx,edx              ; Si agrego esta linea sumo 1024 instrucciones (0x400) al total
        movd xmm2, [eax+ebx*4]
        pmaxud xmm1, xmm2
        inc ebx
        loop busqueda_maximo_loop

    movd eax, xmm1
    pop ebx
    ret


task3_exec_low_st       dd      0
task3_exec_high_st      dd      0
task3_exec_low_end      dd      0
task3_exec_high_end     dd      0

;/*********************************************************************************************************************/
; Tarea 4 - PL=3
;/*********************************************************************************************************************/
SECTION     .data_tarea4         progbits

tarea_4:

    ;xchg bx,bx
    loop_tarea_4:
    
    ;-> Guardo del tiempo de ejecucion actual
    rdtsc
    mov [task4_exec_low_st], eax        ; Parte baja del time counter
    mov [task4_exec_high_st], edx       ; Parte alta del time counter

    calculo_c:
        push ebp
        mov ebp,esp
        push 1024                       ; Parametro 2: Long del buffer
        push INICIO_TABLA               ; Parametro 1: Puntero a la tabla de digitos
        call busqueda_maximo_c
        mov esp, ebp
        pop ebp

    ; Para imprimir el maximo en pantalla descomentar este bloque de codigo
    ; Y comentar el que imprime tiempo de ejecucion (ver abajo)
    ; mov edx, eax 
    ; mov ebx,0x00080700          ; Fila=4, Columna=0, Blanco sobre negro
    ; mov ecx,Tarea4              ; Parametro: texto de tarea a imprimir
    ; call SEL_SYSTEM_CALL:0
    ; ; ----------------------------------------------------------------------

    ;-> Guardo tiempo de ejecucion actual
    rdtsc
    mov [task4_exec_low_end], eax
    mov [task4_exec_high_end], edx

    ;-> Tiempo de ejecucion de la funcion, asumiendo partes altas iguales
    mov ebx, [task4_exec_low_st]
    sub eax, ebx

    ; ; Con este bloque de codigo imprimo en pantalla lo que tarda el algoritmo de busqueda
    mov edx, eax                ; pongo en edx el tiempo de ejecucion
    mov ebx,0x00080700          ; Fila=4, Columna=0, Blanco sobre negro
    mov ecx,Tarea4              ; Parametro: texto de tarea a imprimir
    call SEL_SYSTEM_CALL:0
    ; ------------------------------------------------------------------------------------
    jmp loop_tarea_4


task4_exec_low_st       dd      0
task4_exec_high_st      dd      0
task4_exec_low_end      dd      0
task4_exec_high_end     dd      0

;/*********************************************************************************************************************/
;Handlers de interrupcion y excepciones
;/*********************************************************************************************************************/

SECTION .ISR

EXTERN      TSS_principal
EXTERN      LONG_TSS
EXTERN      OFFSET_BACKLINK
EXTERN      OFFSET_ESP0
EXTERN      OFFSET_SS0
EXTERN      OFFSET_ESP1
EXTERN      OFFSET_SS1
EXTERN      OFFSET_ESP2
EXTERN      OFFSET_SS2
EXTERN      OFFSET_CR3
EXTERN      OFFSET_EIP
EXTERN      OFFSET_EFLAGS
EXTERN      OFFSET_EAX
EXTERN      OFFSET_ECX
EXTERN      OFFSET_EDX
EXTERN      OFFSET_EBX
EXTERN      OFFSET_ESP
EXTERN      OFFSET_EBP
EXTERN      OFFSET_ESI
EXTERN      OFFSET_EDI
EXTERN      OFFSET_ES
EXTERN      OFFSET_CS
EXTERN      OFFSET_SS
EXTERN      OFFSET_DS
EXTERN      OFFSET_FS
EXTERN      OFFSET_GS
EXTERN      OFFSET_LDT
EXTERN      OFFSET_T
EXTERN      OFFSET_BITMAP
EXTERN      OFFSET_PRIM_EJEC
EXTERN      OFFSET_SIG_CR3
EXTERN      DS_SEL_32
EXTERN      DS_SEL_PL3
EXTERN      CS_SEL_PL3

;/************** #DE = DIVIDE ERROR *************/
EXC_handler_0:
	xchg bx,bx
	xor bx,bx
    mov BL, "D" 			; Caracter
    mov CL, 0   			; Fila
    ;mov ESI, 0xB8000   		; Puntero
    mov esi, dir_lineal_VIDEO
    mov CH, 0   			; Columna
    mov [ESI], BL  			; Escribo en pantalla
    mov BYTE [ESI+1], 0x07  ; Atributo

    inc CH					; Incremento Columna
    add ESI,2				; Incremento puntero
    mov BL, "E"
    mov [ESI], BL
    mov BYTE [ESI+1], 0x07
    hlt


;/************** #DB = Reserved INTEL *************/
EXC_handler_1:
	xchg bx,bx
	xor bx,bx
    mov BL, "D" 			; Caracter
    mov CL, 0   			; Fila
    ;mov ESI, 0xB8000   		; Puntero
    mov esi, dir_lineal_VIDEO
    mov CH, 0   			; Columna
    mov [ESI], BL  			; Escribo en pantalla
    mov BYTE [ESI+1], 0x07  ; Atributo

    inc CH					; Incremento Columna
    add ESI,2				; Incremento puntero
    mov BL, "B"
    mov [ESI], BL
    mov BYTE [ESI+1], 0x07
    xchg bx,bx


;/************** NMI *************/
EXC_handler_2:
	xchg bx,bx
	xor bx,bx
    mov BL, "N" 			; Caracter
    mov CL, 0   			; Fila
    ;mov ESI, 0xB8000   		; Puntero
    mov esi, dir_lineal_VIDEO
    mov CH, 0   			; Columna
    mov [ESI], BL  			; Escribo en pantalla
    mov BYTE [ESI+1], 0x07  ; Atributo

    inc CH					; Incremento Columna
    add ESI,2				; Incremento puntero
    mov BL, "M"
    mov [ESI], BL
    mov BYTE [ESI+1], 0x07

    inc CH					; Incremento Columna
    add ESI,2				; Incremento puntero
    mov BL, "I"
    mov [ESI], BL
    mov BYTE [ESI+1], 0x07
    xchg bx,bx

;/************* #BP = Breakpoint *******************/
EXC_handler_3:
	xchg bx,bx
	xor bx,bx
    mov BL, "B" 			; Caracter
    mov CL, 0   			; Fila
    ;mov ESI, 0xB8000   		; Puntero
    mov esi, dir_lineal_VIDEO
    mov CH, 0   			; Columna
    mov [ESI], BL  			; Escribo en pantalla
    mov BYTE [ESI+1], 0x07  ; Atributo

    inc CH					; Incremento Columna
    add ESI,2				; Incremento puntero
    mov BL, "P"
    mov [ESI], BL
    mov BYTE [ESI+1], 0x07
    xchg bx,bx

;/************* #OF = overflow ********************/
EXC_handler_4:
	xchg bx,bx
	xor bx,bx
    mov BL, "O" 			; Caracter
    mov CL, 0   			; Fila
    ;mov ESI, 0xB8000   		; Puntero
    mov esi, dir_lineal_VIDEO
    mov CH, 0   			; Columna
    mov [ESI], BL  			; Escribo en pantalla
    mov BYTE [ESI+1], 0x07  ; Atributo

    inc CH					; Incremento Columna
    add ESI,2				; Incremento puntero
    mov BL, "F"
    mov [ESI], BL
    mov BYTE [ESI+1], 0x07
    xchg bx,bx

;/************** #BR = Bound range *****************/
EXC_handler_5:
	xchg bx,bx
	xor bx,bx
    mov BL, "B" 			; Caracter
    mov CL, 0   			; Fila
    ;mov ESI, 0xB8000   		; Puntero
    mov esi, dir_lineal_VIDEO
    mov CH, 0   			; Columna
    mov [ESI], BL  			; Escribo en pantalla
    mov BYTE [ESI+1], 0x07  ; Atributo

    inc CH					; Incremento Columna
    add ESI,2				; Incremento puntero
    mov BL, "R"
    mov [ESI], BL
    mov BYTE [ESI+1], 0x07
    xchg bx,bx

;/************* #UD = Invalid opcode **************/
EXC_handler_6:
	xchg bx,bx
	xor bx,bx
    mov BL, "U" 			; Caracter
    mov CL, 0   			; Fila
    ;mov ESI, 0xB8000   		; Puntero
    mov esi, dir_lineal_VIDEO
    mov CH, 0   			; Columna
    mov [ESI], BL  			; Escribo en pantalla
    mov BYTE [ESI+1], 0x07  ; Atributo

    inc CH					; Incremento Columna
    add ESI,2				; Incremento puntero
    mov BL, "D"
    mov [ESI], BL
    mov BYTE [ESI+1], 0x07
    hlt

;/************ #NM = device not available **************/
EXC_handler_7:

    ;/* Para SIMD */
    ;xchg bx,bx
    pushad
    clts                                        ;CR0.TS <= 0
    fxrstor [dir_contexto_SIMD]               ;Cargamos los valores de los registros SIMD a partir de su contexto
    popad
    iret
;/************* #DF = DOUBLE FAULT **************/
EXC_handler_8:
	xchg bx,bx
	xor bx,bx
    mov BL, "D" 			; Caracter
    mov CL, 0   			; Fila
    ;mov ESI, 0xB8000   		; Puntero
    mov esi, dir_lineal_VIDEO
    mov CH, 0   			; Columna
    mov [ESI], BL  			; Escribo en pantalla
    mov BYTE [ESI+1], 0x07  ; Atributo

    inc CH					; Incremento Columna
    add ESI,2				; Incremento puntero
    mov BL, "F"
    mov [ESI], BL
    mov BYTE [ESI+1], 0x07
    hlt

;/************ Coprocessor Segment Overrun ***********/
EXC_handler_9:
	xchg bx,bx
	xor bx,bx
    mov BL, "C" 			; Caracter
    mov CL, 0   			; Fila
    ;mov ESI, 0xB8000   		; Puntero
    mov esi, dir_lineal_VIDEO
    mov CH, 0   			; Columna
    mov [ESI], BL  			; Escribo en pantalla
    mov BYTE [ESI+1], 0x07  ; Atributo

    inc CH					; Incremento Columna
    add ESI,2				; Incremento puntero
    mov BL, "S"
    mov [ESI], BL
    mov BYTE [ESI+1], 0x07

    inc CH					; Incremento Columna
    add ESI,2				; Incremento puntero
    mov BL, "O"
    mov [ESI], BL
    mov BYTE [ESI+1], 0x07
    xchg bx,bx


;/************* #TS = INVALID TSS***************/
EXC_handler_10:
	xchg bx,bx
	xor bx,bx
    mov BL, "T" 			; Caracter
    mov CL, 0   			; Fila
    ;mov ESI, 0xB8000   		; Puntero
    mov esi, dir_lineal_VIDEO
    mov CH, 0   			; Columna
    mov [ESI], BL  			; Escribo en pantalla
    mov BYTE [ESI+1], 0x07  ; Atributo

    inc CH					; Incremento Columna
    add ESI,2				; Incremento puntero
    mov BL, "S"
    mov [ESI], BL
    mov BYTE [ESI+1], 0x07
    xchg bx,bx

;/********* #NP = SEGMENT NOT PRESENT***********/
EXC_handler_11:
	xchg bx,bx
	xor bx,bx
    mov BL, "N" 			; Caracter
    mov CL, 0   			; Fila
    ;mov ESI, 0xB8000   		; Puntero
    mov esi, dir_lineal_VIDEO
    mov CH, 0   			; Columna
    mov [ESI], BL  			; Escribo en pantalla
    mov BYTE [ESI+1], 0x07  ; Atributo

    inc CH					; Incremento Columna
    add ESI,2				; Incremento puntero
    mov BL, "P"
    mov [ESI], BL
    mov BYTE [ESI+1], 0x07
    xchg bx,bx

;/********* #SS = STACK SEGMENT FAULT***********/
EXC_handler_12:
	xchg bx,bx
	xor bx,bx
    mov BL, "S" 			; Caracter
    mov CL, 0   			; Fila
   ;mov ESI, 0xB8000   		; Puntero
    mov esi, dir_lineal_VIDEO
    mov CH, 0   			; Columna
    mov [ESI], BL  			; Escribo en pantalla
    mov BYTE [ESI+1], 0x07  ; Atributo

    inc CH					; Incremento Columna
    add ESI,2				; Incremento puntero
    mov BL, "S"
    mov [ESI], BL
    mov BYTE [ESI+1], 0x07
    xchg bx,bx

;/********* #GP = GENERAL PROTECTION* ***********/
EXC_handler_13:

	;xchg bx,bx
	xor bx,bx
    mov BL, "G" 			; Caracter
    mov CL, 0   			; Fila
    ;mov ESI, 0xB8000   		; Puntero
    mov esi, dir_lineal_VIDEO
    mov CH, 0   			; Columna
    mov [ESI], BL  			; Escribo en pantalla
    mov BYTE [ESI+1], 0x07  ; Atributo

    inc CH					; Incremento Columna
    add ESI,2				; Incremento puntero
    mov BL, "P"
    mov [ESI], BL
    mov BYTE [ESI+1], 0x07
    ;hlt
    xchg bx,bx

;/************* #PF = PAGE FAULT ****************/
EXC_handler_14:
	
    ;/* Escribo en pantalla PF*/
    xchg bx,bx
    mov eax, 0xAFEFA
	mov edi, dir_lineal_VIDEO 	;Indice destino, para rep stosw
	mov ax, 0x720				;Atributo y caracter espacio
	;mov ax, 0x741				;Atributo y caracter A
	mov cx, 80*25				;Contador al total de caracteres de la pantalla
	cld							;Flag de direcciones a cero, copia ascendente
	rep stosw					;Almacena ax en es:[di] (2 bytes) 

	xchg bx,bx
	xor bx,bx
    mov BL, "P" 			; Caracter
    mov CL, 0   			; Fila
    ;mov ESI, 0xB8000   		; Puntero
    mov esi, dir_lineal_VIDEO
    mov CH, 0   			; Columna
    mov [ESI], BL  			; Escribo en pantalla
    mov BYTE [ESI+1], 0x07  ; Atributo

    inc CH					; Incremento Columna
    add ESI,2				; Incremento puntero
    mov BL, "F"
    mov [ESI], BL
    mov BYTE [ESI+1], 0x07
    ;/***************************/

    ; Codigo de page fault. Para probar-> definir una esquema (dir.fis/lin) para las tablas dinamicas 

    ; mov eax, cr2                                        ;Direccion lineal que genero la excepcion
    ; mov ebx, cr2
    
    ; shr eax, 22                                         ;Indice dentro del directorio
    ; shl ebx, 10
    ; shr ebx, 22                                         ;Indice dentro de la tabla
    
    ; mov edi, dir_fisica_tablas_dinamicas                ; Por consigna de TP = 0x0800000 
    ; mov ebp, cr3
    
    ; tabla_lectura:
    ;     mov edx, dword[ebp+eax*4]                       ;Leo el descriptor de la tabla con el indice correspondiente, para saber si ya esta presente o no esta tabla
    ;     shl edx, 31                                     ;Me quedo solamente con el bit de presente
    ;     shr edx, 31
        
    ;     cmp edx, 0x00                                   ;Veo si esta presente
    ;     je  tabla_no_presente                           ;Si no lo esta, hace falta cargar tabla 
    
    ; tabla_presente:
    ;     mov esi, dword[ebp+eax*4]                       ;Busco la direccion de la tabla
    ;     shr esi, 12
    ;     shl esi, 12                                     ;Me quedo solo con la direccion de la tabla
    ;     add edi, 0x03                                   ;Atributo de la pagina
        
    ;     mov cl, [num_tabla_dinamica]                    ;Para saber a que direccion fisica correspondera la pagina
    ;     cmp ecx, 0x00
    ;     jmp carga_pagina                                ;No hay otras paginas cargadas de forma dinamica anteriormente
        
    ;     carga_direc_fisica:                             ;Si hay otras paginas, me muevo de a 4k la cant de paginas que hayan
    ;         add edi, 0x1000
    ;         loop carga_direc_fisica
    ;     jmp carga_pagina
                
    ; tabla_no_presente:
    ;     mov esi, dir_lineal_tablas_dinamicas            ;Inicio de las tablas en 0x00300000 
    ;     mov cl, [num_tabla_dinamica]                    ;Cantidad de tablas ya cargadas
    ;     inc byte[num_tabla_dinamica]                    ;Incremento el numero de tablas presentes
    ;     cmp ecx, 0x00                                   ;Si es la primer tabla, no hace falta correr la direccion
    ;     je  carga_tabla
    
    ; cant_tablas:                                        ;Si hay varias tablas, ubico mi nueva tabla al final de estas
    ;     add esi, 0x1000 
    ;     add edi, 0x1000
    ;     loop cant_tablas    

    ; carga_tabla:
    ;     add esi, 0x03                                   ;Atributo de la tabla
    ;     add edi, 0x03                                   ;Atributo de la pagina
    ;     mov dword[ebp+ eax*4], esi                      ;Cargo la tabla correspondiente a partir del indice obtenido de la dir. lineal
    
    ; carga_pagina:
    ;     shr esi, 12
    ;     shl esi, 12
    ;     mov dword[esi + ebx*4], edi                     ;Cargo la Pagina
    
    ; popad
    ; add esp, 4                                          ;Popeamos el codigo de error
    iret

;/*************** Intel reserved, do not use *************/
EXC_handler_15:
;/********************************************************/

;/*************** #MF = x87 FPU Floating point *****************/
EXC_handler_16:
	xchg bx,bx
	xor bx,bx
    mov BL, "M" 			; Caracter
    mov CL, 0   			; Fila
    mov esi, dir_lineal_VIDEO
    ;mov ESI, 0xB8000   		; Puntero
    mov CH, 0   			; Columna
    mov [ESI], BL  			; Escribo en pantalla
    mov BYTE [ESI+1], 0x07  ; Atributo

    inc CH					; Incremento Columna
    add ESI,2				; Incremento puntero
    mov BL, "F"
    mov [ESI], BL
    mov BYTE [ESI+1], 0x07

;/*************** #AC = Alignment Check *************************/
EXC_handler_17:
	xchg bx,bx
	xor bx,bx
    mov BL, "A" 			; Caracter
    mov CL, 0   			; Fila
    mov esi, dir_lineal_VIDEO
    ;mov ESI, 0xB8000   		; Puntero
    mov CH, 0   			; Columna
    mov [ESI], BL  			; Escribo en pantalla
    mov BYTE [ESI+1], 0x07  ; Atributo

    inc CH					; Incremento Columna
    add ESI,2				; Incremento puntero
    mov BL, "C"
    mov [ESI], BL
    mov BYTE [ESI+1], 0x07

;/**************** #MC = Machine check ***************************/
EXC_handler_18:
	xchg bx,bx
	xor bx,bx
    mov BL, "M" 			; Caracter
    mov CL, 0   			; Fila
    mov esi, dir_lineal_VIDEO
    ;mov ESI, 0xB8000   		; Puntero
    mov CH, 0   			; Columna
    mov [ESI], BL  			; Escribo en pantalla
    mov BYTE [ESI+1], 0x07  ; Atributo

    inc CH					; Incremento Columna
    add ESI,2				; Incremento puntero
    mov BL, "C"
    mov [ESI], BL
    mov BYTE [ESI+1], 0x07
    xchg bx,bx

;/******************* #XF = SIMD Floating point exc *******************/
EXC_handler_19:
	xchg bx,bx
	xor bx,bx
    mov BL, "X" 			; Caracter
    mov CL, 0   			; Fila
    mov esi, dir_lineal_VIDEO
    ;mov ESI, 0xB8000   		; Puntero
    mov CH, 0   			; Columna
    mov [ESI], BL  			; Escribo en pantalla
    mov BYTE [ESI+1], 0x07  ; Atributo

    inc CH					; Incremento Columna
    add ESI,2				; Incremento puntero
    mov BL, "F"
    mov [ESI], BL
    mov BYTE [ESI+1], 0x07
    xchg bx,bx

;/***************TIMER****************/
IRQ_handler_0:

    ; IRQ0 con Multitarea - SCHEDULER
    
    push eax                            ;Cargamos en la pila eax
    mov ax, ds
    movzx eax, ax 
    push eax                            ;Cargamos en la pila ds
    
    mov eax, cr0                        ;chequeo bit 3 del cr0 (cr0.3 = Task switched)
    test eax, 8                         ;hago una and
    jnz sin_SIMD                        ;si no es cero significa que no debo resguardar el contexto de simd
    fxsave [dir_contexto_SIMD]          ;Guardamos contexto de los registros SIMD solo si TS = 0
    
    sin_SIMD:
    mov eax, cr0                        ;Pongo el TS a 1
    or eax, 0x08
    mov cr0, eax
    
    mov ax, DS_SEL_32
    mov ds, ax                          ;DS a nivel de kernel (privilegio 0)
    
    mov eax, 0x00008000                 ;Apuntamos al contexto de la tarea (todas comparten la misma dir lineal 0x00008000)
        
    mov dword[eax + OFFSET_EBX], ebx    ;Guardamos los valores actuales de los registros en el contexto 
    mov dword[eax + OFFSET_ECX], ecx
    mov dword[eax + OFFSET_EDX], edx
    mov dword[eax + OFFSET_ESI], esi
    mov dword[eax + OFFSET_EDI], edi
    mov dword[eax + OFFSET_EBP], ebp
        
    pop ecx                     
    mov dword[eax + OFFSET_DS], ecx     ;Sacamos de la pila ds y lo guardamos en el contexto
    pop ecx
    mov dword[eax + OFFSET_EAX], ecx    ;Sacamos de la pila eax y lo guardamos en el contexto
    
    mov dword[eax + OFFSET_ESP0], esp   ;Guardamos el valor de esp en el contexto
    
    mov ecx, [eax + OFFSET_SIG_CR3]     ;Cual es la siguiente tarea a correr
            
    mov cr3, ecx                        ;Nueva cr3
        
    mov al, 0x20                        ;Borro flag para que pueda volver a interrumpir
    out 0x20, al
        
    mov eax, 0x00008000                 ;Apunto al nuevo contexto (dir lineal 0x00008000)
        
    mov esp, [eax + OFFSET_ESP0]        ;Cargo esp a partir del contexto
        
    mov ecx, [eax + OFFSET_PRIM_EJEC]   ;Si es la primera ejecucion, hay q cargar en la pila cs, eflags y eip
    cmp ecx, 1
    je no_es_primer_ejec
    
    mov ecx, [eax + OFFSET_DS]                      ;Veo si es nivel 0 o 3
    cmp ecx, 0x10
    je nivel_0
    
    mov ecx, DS_SEL_PL3                             ;SS de nivel 3
    push ecx
    mov ecx, 0x00613000                             ;dir lineal de pila de usuario. tarea 1 y 2 tienen la misma dir lineal 0x00613000
    add ecx, 0xFFF                                  ;Apunto a la pila de nivel 3
    push ecx                                        ;La pusheo a la pila
    
    mov ecx, [eax + OFFSET_EFLAGS]                  ;Desde el contexto, cargamos la pila
    push ecx
    mov ecx, CS_SEL_PL3                             ;Cargo CS de la pila con CS de nivel 3
    push ecx
    mov ecx, [eax + OFFSET_EIP]
    push ecx
    mov dword[eax + OFFSET_PRIM_EJEC], 1            ;Ya no es primera ejcucion
    jmp no_es_primer_ejec
    
    nivel_0:
        mov ecx, [eax + OFFSET_EFLAGS]              ;Desde el contexto, cargamos la pila
        push ecx
        mov ecx, [eax + OFFSET_CS]
        push ecx
        mov ecx, [eax + OFFSET_EIP]
        push ecx
        mov dword[eax + OFFSET_PRIM_EJEC], 1        ;Ya no es primera ejcucion
        
    no_es_primer_ejec:
            
        mov ecx, [eax + OFFSET_ECX]                 ;Cargo un registro general cualquiera
        push ecx                                    ;Lo pongo en la pila, para no perder su valor
        mov ecx, eax                                ;Puntero al contexto
            
        mov eax, [ecx + OFFSET_EAX]                 ;Cargo todos los registros con los valores del contexto
        mov ebx, [ecx + OFFSET_EBX]
        mov edx, [ecx + OFFSET_EDX]
        mov esi, [ecx + OFFSET_ESI]
        mov edi, [ecx + OFFSET_EDI]
        mov ebp, [ecx + OFFSET_EBP]
        mov es,  [ecx + OFFSET_ES]
        mov ds,  [ecx + OFFSET_DS]
        pop ecx                                     ;Recupero el valor de ecx
        jmp salida_IRQ0
            
    salida_IRQ0:
    ;xchg bx, bx
    iret

;/*****************KEYBOARD******************/
IRQ_handler_1:
	;xchg bx, bx
	pushad
	
	xor ebx, ebx
	;mov ecx, BUFFER_TABLA
	mov ecx, BUFFER

	in al, 0x60								;Leo puerto de entrada
	mov bl, al
	;xchg bx,bx
	
	cmp al, 0x1F							;¿Se presionó la S?
	je	make_S
	;jmp salida_IRQ1						; Era solo cuando probaba el codigo de la S

	cmp al, 0x15
	je make_Y

	cmp al, 0x16
	je make_U

	cmp al, 0x17 
	je make_I

	cmp al, 0x18 
	je make_O

	cmp al, 0x19
	je make_P

	cmp al, 0x1E    ;  Es la A?
	je es_A

	cmp al, 0x30    ;  Es la B?
	je es_B
	
	cmp al, 0x2E    ;  Es la C?
	je es_C


	cmp al, 0x20    ;  Es la D?
	je es_D 


	cmp al, 0x12    ;  Es la E?
	je es_E 


	cmp al, 0x21    ;  Es la F?
	je es_F 
	jmp no_es_letra

	es_A:
		mov bl, 0x0A
		je es_valida    ;  Es la A, la escribo en buffer

	es_B:
		mov bl, 0xB
		je es_valida    ;  Es la B, la escribo en buffer

	es_C:
		mov bl, 0xC
		je es_valida    ;  Es la C, la escribo en buffer

	es_D:
		mov bl, 0xD
		je es_valida    ;  Es la D, la escribo en buffer

	es_E:
		mov bl, 0xE 
		je es_valida    ;  Es la E, la escribo en buffer

	es_F:
		mov bl, 0xF 
		je es_valida    ;  Es la F, la escribo en buffer


	no_es_letra:
	; ¿Es ENTER? MakeCode = 0x1C
	cmp al, 0x1C
	je aprete_enter

	; Si llego hasta acá quiere decir que no es ninguna de las letras entre A-F
	; Me fijo si es un número
	; El make code más bajo es el del 1 (0x02) y el más alto el del 0 (0x0B). Son todos contiguos.

	cmp al, 0x02
	jae higherorequal_1 ; Igual o más grande que el make code del 1

	higherorequal_1:
    	cmp al, 0x0B
    	jbe es_numero   ; Más chico o igual que el make code del 0 -> es número
    	jmp salida_IRQ1 ; Si no cumple, salgo de la interrupcion

	es_numero:
    	cmp al, 0x0B
    	je es_cero

    	dec bl 			;El make code de los numeros es mas grande en una unidad que el numero correspondiente
    	jmp es_valida

    	es_cero:
    	mov bl, 0x00
    	jmp es_valida


    ; Valida la LETRA
	es_valida:

		mov edx, [CONTADOR_BUFFER]

		cmp edx, 0
		je primera

		cmp edx, 0x07
		je reiniciar_contador

    	rol ebx, 28
    	shld [ecx], ebx, 4

    	;Antes lo tenia mal!
    	;rol ebx, 12
    	;shld [ecx], bx, 4

    	inc edx
    	mov [CONTADOR_BUFFER], edx
    	jmp salida_IRQ1

    primera:
    	rol ebx,28

    	shld [ecx], ebx, 4

    	inc edx
    	mov [CONTADOR_BUFFER], edx
    	jmp salida_IRQ1

    reiniciar_contador:
    	rol ebx,28
    	shld [ecx], ebx, 4
    	
    	mov edx, 0
    	mov [CONTADOR_BUFFER], edx
    	jmp salida_IRQ1

	aprete_enter:
    	;Guardar en tabla
    	xchg bx,bx

		mov ecx, [BUFFER] 					; Cargo el contenido del buffer en ecx
		xor edx, edx  						; Pongo a cero edx
		xor eax, eax 						; Pongo a cero eax

		mov edx, [INDICE_TABLA] 			; Cargo el indice de la tabla en edx. Me va a servir para mover el puntero
											; desdes la direccion 0x00410000. Este indice toma valores multiplos de 4.

		mov eax, INICIO_TABLA 				; Cargo en eax el puntero al inicio de la tabla (0x00410000)
		add eax, edx 						; Incremento el puntero para guardar una nueva entrada en la tabla
		mov [eax], ecx  					; Copio el contenido del buffer en la entrada correspondiente 

		add edx, 0x04   					; Actualizo el puntero
		mov [INDICE_TABLA], edx  
 
    	mov ecx, 0 						
    	mov [CONTADOR_BUFFER],ecx   		; Borro el contador del buffer que me sirve para manejar la cola circular
    	 									; Es como el "indice" del buffer
    	mov [BUFFER], ecx  					; Borro el buffer

    	; NOTA:
    	; Lo correcto seria implementar un control para no pasarme de la tabla. Es decir, si por 
    	; alguna razón yo llenara TODA la tabla y llegase al final, deberia volver al principio y escribir desde ahi
    	; Seria algo parecido al buffer circular.
    	; Como no es el objetivo del ejercicio (yo quiero meter valores para generar el fallo de pagina) y
    	; no va a pasar ni en pedo que se me va a llenar la tabla, lo dejo asi. 

    	jmp salida_IRQ1
	
	make_S:
		xchg bx,bx
		xor bx,bx
    	mov BL, "I" 			; Caracter
    	mov CL, 0   			; Fila
    	mov esi, dir_lineal_VIDEO
    	;mov ESI, 0xB8000   		; Puntero
    	mov CH, 0   			; Columna
    	mov [ESI], BL  			; Escribo en pantalla
    	mov BYTE [ESI+1], 0x07  ; Atributo

    	inc CH					; Incremento Columna
    	add ESI,2				; Incremento puntero
    	mov BL, "1"
    	mov [ESI], BL
    	mov BYTE [ESI+1], 0x07

    	;/***PRUEBA EXC0****/
    	xor edx, edx
    	xor ecx, ecx
    	xor eax, eax
    	mov eax, 0x10
    	div ecx

		jmp salida_IRQ1

	; Genero la #DE (Divide error) = EXC0
	make_Y:
		xor edx, edx
    	xor ecx, ecx
    	xor eax, eax
    	mov eax, 0x10
    	div ecx
    	jmp salida_IRQ1

    ; Genero la #UD (Invalid opcode) = EXC6
    make_U:
    	; Manual INTEL: The UD2 instruction is guaranteed to generate an invalid opcode exception.
    	ud2
    	jmp salida_IRQ1

    ; Genero la #DF (Double Fault) = EXC8 
    make_I:
    	; Le saco el bit de presente a la Excepcion 0

    	xor eax, eax
    	mov ax, 0x0F 						; Con ese valor borro el bit de presente.
    	mov word [Excepcion_0+5], ax 

    	; Genero la EXC0
    	xor edx, edx
    	xor ecx, ecx
    	xor eax, eax
    	mov eax, 0x10
    	div ecx
    	jmp salida_IRQ1

    ; Genero la #GP (General protection) = EXC13
    make_O:
    	; Manual INTEL: Loading the SS register with the segment selector of an executable segment or a null segment selector.
    	; Otra forma: Attempting to write a 1 into a reserved bit of CR4.
    	mov ax, 0x00
		mov ss, ax
    	jmp salida_IRQ1

    ; Genero la #PF (Page fault) = EXC14
    make_P:
    	jmp salida_IRQ1

		
	salida_IRQ1:							;Le aviso al 1er PIC que terminó el Handler 
	mov al, 0x20							;Limpiamos flag de interrupcion, para que pueda volver a interrumpir
	out 0x20, al

	popad
	iretd

IRQ_handler_2:
	pushad
	mov al, 0x20							;Limpiamos flag de interrupcion, para que pueda volver a interrumpir
	out 0x20, al
	popad
	iretd

IRQ_handler_3:
	pushad
	mov al, 0x20							;Limpiamos flag de interrupcion, para que pueda volver a interrumpir
	out 0x20, al
	popad
	iretd

IRQ_handler_4:
	pushad
	mov al, 0x20							;Limpiamos flag de interrupcion, para que pueda volver a interrumpir
	out 0x20, al
	popad
	iretd

IRQ_handler_5:
	pushad
	mov al, 0x20							;Limpiamos flag de interrupcion, para que pueda volver a interrumpir
	out 0x20, al
	popad
	iretd

IRQ_handler_6:
	pushad
	mov al, 0x20							;Limpiamos flag de interrupcion, para que pueda volver a interrumpir
	out 0x20, al
	popad
	iretd

IRQ_handler_7:
	pushad
	mov al, 0x20							;Limpiamos flag de interrupcion, para que pueda volver a interrumpir
	out 0x20, al
	popad
	iretd

IRQ_handler_8:
	pushad
	mov al, 0x20							;Limpiamos flag de interrupcion, para que pueda volver a interrumpir
	out 0x20, al
	popad
	iretd

IRQ_handler_9:
	pushad
	mov al, 0x20							;Limpiamos flag de interrupcion, para que pueda volver a interrumpir
	out 0x20, al
	popad
	iretd

IRQ_handler_10:
	pushad
	mov al, 0x20							;Limpiamos flag de interrupcion, para que pueda volver a interrumpir
	out 0x20, al
	popad
	iretd

IRQ_handler_11:
	pushad
	mov al, 0x20							;Limpiamos flag de interrupcion, para que pueda volver a interrumpir
	out 0x20, al
	popad
	iretd

IRQ_handler_12:
	pushad
	mov al, 0x20							;Limpiamos flag de interrupcion, para que pueda volver a interrumpir
	out 0x20, al
	popad
	iretd

IRQ_handler_13:
	pushad
	mov al, 0x20							;Limpiamos flag de interrupcion, para que pueda volver a interrumpir
	out 0x20, al
	popad
	iretd

IRQ_handler_14:
	pushad
	mov al, 0x20							;Limpiamos flag de interrupcion, para que pueda volver a interrumpir
	out 0x20, al
	popad
	iretd

IRQ_handler_15:
	pushad
	mov al, 0x20							;Limpiamos flag de interrupcion, para que pueda volver a interrumpir
	out 0x20, al
	popad
	iretd


;***********************************************************************
;   System Call para imprimir en pantalla
;***********************************************************************
system_call:

    ;xchg bx, bx
    ;mov eax, 0x0515CA11           ; debug, pongo en EAX "syscall"

    push edx ;Param3 ->  ebp+16: Numero a imprimir al lado del msg
    push ebx ;Param2 ->  ebp+12: En ebx viene la columna, fila y atributo de lo que se quiere escribir
    push ecx ;Param1 ->  ebp+8:  En ecx viene el comienzo del string
    call print
    add esp, 12 ;tres parametros, compenso stack
    
    retf

;/*******************************************************************/

;***********************************************************************
;   Funcion que imprime en pantalla strings
;***********************************************************************
print:
    push ebp
    mov ebp,esp

    mov edi,dir_lineal_VIDEO    ;Base de la memoria de video. Indice destino que usa movsw

    mov eax,[ebp+12]            ;cargo en eax columna-fila-color-basura
    shr eax,16                  ;shifteo los 16 bits mas significativos para cargar columna-fila en ax
    movzx eax,ah                ;Me quedo con la columna y extiendo con ceros
    mov ebx,2   
    mul ebx                     ;Multiplico la columna por dos porque cada caracter de la pantalla tiene 2 bytes
    add edi,eax                 ;sumo al offset, la columna en la q empieza el string en pantalla

    mov eax,[ebp+12]            ;cargo en eax columna-fila-color-basura
    shr eax,16                  ;shifteo los 16 bits mas significativos para cargar columna-fila en ax
    movzx eax,al                ;Me quedo con la fila y extiendo con ceros
    mov ebx,160
    mul ebx                     ;Multiplico la fila por 160 porque cada fila de la pantalla ocupa 160 bytes
    add edi,eax                 ;sumo al offset, la fila en la q empieza el string en pantalla

    mov eax,[ebp+8]             ;Cargo en eax la direccion de comienzo del string_ptr
    mov ecx,25*80               ;Total de caracteres de pantalla
    mov esi,edi                 ;Necesito el offset que tengo en edi para saber cuantos caracteres puedo escribir
    sub esi,dir_lineal_VIDEO    ;Le resto el comienzo de la memoria de video y me queda solo el caracter inicial
    sub ecx,esi                 ;Calculo cuantos caracteres puedo escribir a partir del inicial y lo dejo en ecx para que 
                                ;limite el lazo
    mov ebx,[ebp+12]            ;Cargo ebx, y en bh me queda el color (atributo)
    lazo:
        cmp byte[eax],0         ;Comparo para buscar el NULL
        je fin_task_msg           ;Si llegue al NULL me voy del lazo y dejo de escribir caracteres
        mov edx,[eax]           ;Cargo edx, y me quedo con el caracter (ascii) en dl
        mov bl,dl               ;Junto atributo y caracter en bx
        mov [ds:edi],bx         ;Escribo en pantalla
        inc eax                 ;Apunto al siguiente caracter
        add edi,2               ;Incremento el offset de los caracteres de la pantalla
    loop lazo

    ; Aca ya imprimi "TareaX: 
    ; Ahora quiero imprimir al lado el numero en cuestion

    fin_task_msg:
    xor eax, eax
    xor ebx, ebx
    xor ecx, ecx
    mov ecx, 8                  ; Variable que controla el loop de impresion del numero (8 veces)

    mov eax, [ebp + 16]         ; Tercer parametro, agarro el numero que quiero imprimir
    mov ebx, eax
    
    print_number:
      mov eax, ebx              ; Hago una copia de la sumatoria en eax
      and eax, 0xF0000000       ; Mascara para quedarme con el byte mas sign. Voy a imprimir del mas sign. al menos
      shr eax, 4*7              ; Muevo el byte mas sign. a la posición menos sign -> 0x0000000X

      cmp al, 0x09              ; Convierto a ascii. Si el numero esta entre 0 y 9, le sumo 48 para la conversion.
      jae sumar_55              ; Si está entre A y F, hay que sumar 55
      add eax, 48
      jmp pantalla

    sumar_55:
      add eax, 55               ; Si estoy entre A y F

    pantalla:
        mov [edi], al           ; Escribo en pantalla
        mov byte [edi+1], 0x07  ; Atributo
        shl ebx,4               ; Shifteo a la izquierda ebx para agarrar el proximo numero que quiero imprimir 
        add edi,2               ; Incremento puntero 
        dec ecx
        jnz print_number  


    exit_print:
    mov esp,ebp
    pop ebp
ret