;********************************************************************************
; Archivo: systables.asm
; Descripcion: - contiene seccion de las tablas de sistema (GDY e IDT)
;              - contiene seccion de tss para cambio de tarea manual 
;********************************************************************************


;********************************************************************************
; Seccion para las tss
;********************************************************************************
GLOBAL      TSS_principal
GLOBAL      LONG_TSS
GLOBAL      OFFSET_BACKLINK
GLOBAL      OFFSET_ESP0
GLOBAL      OFFSET_SS0
GLOBAL      OFFSET_ESP1
GLOBAL      OFFSET_SS1
GLOBAL      OFFSET_ESP2
GLOBAL      OFFSET_SS2
GLOBAL      OFFSET_CR3
GLOBAL      OFFSET_EIP
GLOBAL      OFFSET_EFLAGS
GLOBAL      OFFSET_EAX
GLOBAL      OFFSET_ECX
GLOBAL      OFFSET_EDX
GLOBAL      OFFSET_EBX
GLOBAL      OFFSET_ESP
GLOBAL      OFFSET_EBP
GLOBAL      OFFSET_ESI
GLOBAL      OFFSET_EDI
GLOBAL      OFFSET_ES
GLOBAL      OFFSET_CS
GLOBAL      OFFSET_SS
GLOBAL      OFFSET_DS
GLOBAL      OFFSET_FS
GLOBAL      OFFSET_GS
GLOBAL      OFFSET_LDT
GLOBAL      OFFSET_T
GLOBAL      OFFSET_BITMAP
GLOBAL      OFFSET_PRIM_EJEC
GLOBAL      OFFSET_SIG_CR3


SECTION 	.section_tss		progbits

TSS_principal	  times 104 db 0					;TSS que va a usar el descriptor
	
OFFSET_BACKLINK   equ 	0
OFFSET_ESP0       equ 	4
OFFSET_SS0        equ 	8
OFFSET_ESP1       equ 	12
OFFSET_SS1        equ 	16
OFFSET_ESP2       equ 	20
OFFSET_SS2        equ 	24
OFFSET_CR3        equ 	28
OFFSET_EIP        equ 	32
OFFSET_EFLAGS     equ 	36
OFFSET_EAX        equ 	40
OFFSET_ECX        equ 	44
OFFSET_EDX        equ 	48
OFFSET_EBX        equ 	52
OFFSET_ESP        equ 	56
OFFSET_EBP        equ 	60
OFFSET_ESI        equ 	64
OFFSET_EDI        equ 	68
OFFSET_ES         equ 	72
OFFSET_CS         equ 	76
OFFSET_SS         equ 	80
OFFSET_DS         equ 	84
OFFSET_FS         equ 	88
OFFSET_GS         equ 	92
OFFSET_LDT        equ 	96
OFFSET_T          equ 	100
OFFSET_BITMAP     equ 	102
OFFSET_PRIM_EJEC  equ   104
OFFSET_SIG_CR3	  equ	108

LONG_TSS		  equ 	104
;********************************************************************************


;********************************************************************************
; Seccion las tablas del sistema (GDT - IDT)
;********************************************************************************

SECTION .sys_tables

GLOBAL SEL_TSS
GLOBAL SEL_SYSTEM_CALL
GLOBAL offset_system_call
GLOBAL CS_SEL_PL3
GLOBAL DS_SEL_PL3
GLOBAL CS_SEL_32
GLOBAL DS_SEL_32
GLOBAL GDT_tam_32
GLOBAL GDT_32
GLOBAL im_gdtr_32

GLOBAL IDT_32
GLOBAL idtr_img
GLOBAL IDT

GLOBAL Excepcion_0
GLOBAL Excepcion_1
GLOBAL Excepcion_2
GLOBAL Excepcion_3
GLOBAL Excepcion_4
GLOBAL Excepcion_5
GLOBAL Excepcion_6
GLOBAL Excepcion_7
GLOBAL Excepcion_8
GLOBAL Excepcion_9
GLOBAL Excepcion_10
GLOBAL Excepcion_11
GLOBAL Excepcion_12
GLOBAL Excepcion_13
GLOBAL Excepcion_14
GLOBAL Excepcion_15
GLOBAL Excepcion_16
GLOBAL Excepcion_17
GLOBAL Excepcion_18
GLOBAL Excepcion_19

GLOBAL Interrupcion_0
GLOBAL Interrupcion_1
GLOBAL Interrupcion_2
GLOBAL Interrupcion_3
GLOBAL Interrupcion_4
GLOBAL Interrupcion_5
GLOBAL Interrupcion_6
GLOBAL Interrupcion_7
GLOBAL Interrupcion_8
GLOBAL Interrupcion_9
GLOBAL Interrupcion_10
GLOBAL Interrupcion_11
GLOBAL Interrupcion_12
GLOBAL Interrupcion_13
GLOBAL Interrupcion_14
GLOBAL Interrupcion_15



GDT_32:

	dq 0 ;selector nulo

    ; Quiero el selector de código en: 0xFFFF0000
    ; Descriptor de NIVEL CERO
    CS_SEL_32 equ $-GDT_32
		
		dw 0xffff   ; Limite 15-0
        dw 0		; Base 15-0
        db 0x00     ; Base 23-16
        db 0x9a     ; Derechos de Acceso: 0b10011010. De izquierda a derecha:
         			; Bit 7: Segmento presente. Bit 6-5: DPL=00 (Maximo privilegio: Kernel)
         			; Bi4 4: '1' Para código o datos. Bit 3: '1' para código
         			; Bit 2: '0' (no) Segmento conforme. Bit 1: '1' permiso de lectura.
         			; Bit 0: Accedido
        db 0xcf	    ; 0b11001111. Bit 7: Granularidad=1. Bit 6: Default=1 (Segmento 32 bits)
         			; Bit 5-4 = 00 por definicion.
         			; Limite 19-16 = f
        db 0x00     ; Base 31-24 = 0xff 

        ; Me queda: BASE = 0xFFFF0000 y LIMITE = 0xFFFFF y se completa con tres F más por tener G=1
        ; Si fuera G=0 el limite es el que figura en el descriptor

    ; Quiero el selector de datos en: 0x00000000
    ; Descriptor de NIVEL CERO
    DS_SEL_32 equ $-GDT_32
      
        dw 0xffff   ; Limite 15-0
        dw 0		; Base 15-0
        db 0		; Base 23-16 
        db 0x92	    ; Derechos de Acceso: 0b10010010. De izquierda a derecha:
         			; Bit 7: Segmento presente. Bit 6-5: DPL=00 (Maximo privilegio: Kernel)
         			; Bi4 4: '1' Para código o datos. Bit 3: '0' para datos
         			; Bit 2: '0' (no) Segmento conforme. Bit 1: '1' permiso de escritura.
         			; Bit 0: Accedido
        db 0xcf     ; 0b11001111. Bit 7: Granularidad=1. Bit 6: Default=1 (Segmento 32 bits)
         			; Bit 5-4 = 00 por definicion.
         			; Limite 19-16 = f
        db 0		; Base 31-24 = 0x00 

        ; Me queda BASE = 0x00000000 y LIMITE = 0xFFFFF y tres F más por tener G=1 (0xFFFFFFFF)
      
    ; Descriptor de NIVEL TRES
    CS_SEL_PL3 equ $-GDT_32+3
        
        dw 0xffff   ; Limite 15-0
        dw 0        ; Base 15-0
        db 0x00     ; Base 23-16
        db 0xfa     ; Derechos de Acceso: 0b11111010. De izquierda a derecha:
                    ; Bit 7: Segmento presente. Bit 6-5: DPL=11 (Privilegio más chico)
                    ; Bi4 4: '1' Para código o datos. Bit 3: '1' para código
                    ; Bit 2: '0' (no) Segmento conforme. Bit 1: '1' permiso de lectura.
                    ; Bit 0: Accedido
        db 0xcf     ; 0b11001111. Bit 7: Granularidad=1. Bit 6: Default=1 (Segmento 32 bits)
                    ; Bit 5-4 = 00 por definicion.
                    ; Limite 19-16 = f
        db 0x00     ; Base 31-24 = 0xff 

    ; Descriptor de NIVEL TRES
    DS_SEL_PL3 equ $-GDT_32+3
      
        dw 0xffff   ; Limite 15-0
        dw 0        ; Base 15-0
        db 0        ; Base 23-16 
        db 0xf2     ; Derechos de Acceso: 0b11110010. De izquierda a derecha:
                    ; Bit 7: Segmento presente. Bit 6-5: DPL=11 (Privileigo más chico)
                    ; Bi4 4: '1' Para código o datos. Bit 3: '0' para datos
                    ; Bit 2: '0' (no) Segmento conforme. Bit 1: '1' permiso de escritura.
                    ; Bit 0: Accedido
        db 0xcf     ; 0b11001111. Bit 7: Granularidad=1. Bit 6: Default=1 (Segmento 32 bits)
                    ; Bit 5-4 = 00 por definicion.
                    ; Limite 19-16 = f
        db 0        ; Base 31-24 = 0x00

    ; Descriptor de la TSS (Compuerta de interrupcion)
    SEL_TSS equ $-GDT_32
        dw  LONG_TSS-1      ;Longitud = 104 Bytes
        dw  TSS_principal   ;Base donde se encuentra la TSS (15-0)
        db  0               ;Base 23-16
        db  0x89            ;Descriptor de tarea Desocupado
        dw  0

    ; 0xEC es porque el DPL vale 3
    ; no paso parametros por la pila. Ojo con retf n
    SEL_SYSTEM_CALL     equ $-GDT_32
    offset_system_call:
        dw 0                ;Offset 15-0
        dw CS_SEL_32        ;Selector de codigo de PL=0
        db 0                ;No se pasan parametros por la pila
        db 0xEC             ;Compuerta de llamada de 32 bits con DPL=3
        dw 0                ;Offset 31-16

    GDT_tam_32 equ $-GDT_32

 	;0   15 16         47
    ;|LIMIT|----BASE----|

im_gdtr_32:
    dw  GDT_tam_32 -1		; Limite (Tamaño de la GDT)
      					    ; LIMIT is 1 less than the length of the table, so if LIMIT has the value 15, then the GDT is 16 bytes long.

    dd  GDT_32   ; Dir. Lineal Base (Donde empieza la GDT)


;***************IDT_32*****************/
IDT:

Excepcion_0:
dw 0			;Offset (15-0)
dw CS_SEL_32	;Selector que usa en el handler
db 0
db 8Fh			;DPL:0 Excepcion
dw 0			;Offset (31-16)

Excepcion_1:
dw 0			;Offset (15-0)
dw CS_SEL_32	;Selector que usa en el handler
db 0
db 8Fh			;DPL:0 Excepcion
dw 0			;Offset (31-16)

Excepcion_2:
dw 0			;Offset (15-0)
dw CS_SEL_32	;Selector que usa en el handler
db 0
db 8Fh			;DPL:0 Excepcion
dw 0			;Offset (31-16)

Excepcion_3:
dw 0			;Offset (15-0)
dw CS_SEL_32	;Selector que usa en el handler
db 0
db 8Fh			;DPL:0 Excepcion
dw 0			;Offset (31-16)

Excepcion_4:
dw 0			;Offset (15-0)
dw CS_SEL_32	;Selector que usa en el handler
db 0
db 8Fh			;DPL:0 Excepcion
dw 0			;Offset (31-16)

Excepcion_5:
dw 0			;Offset (15-0)
dw CS_SEL_32	;Selector que usa en el handler
db 0
db 8Fh			;DPL:0 Excepcion
dw 0			;Offset (31-16)

Excepcion_6:
dw 0			;Offset (15-0)
dw CS_SEL_32	;Selector que usa en el handler
db 0
db 8Fh			;DPL:0 Excepcion
dw 0			;Offset (31-16)

Excepcion_7:
dw 0			;Offset (15-0)
dw CS_SEL_32	;Selector que usa en el handler
db 0
db 8Fh			;DPL:0 Excepcion
dw 0			;Offset (31-16)

Excepcion_8:
dw 0			;Offset (15-0)
dw CS_SEL_32	;Selector que usa en el handler
db 0
db 8Fh			;DPL:0 Excepcion
dw 0			;Offset (31-16)

Excepcion_9:
dw 0			;Offset (15-0)
dw CS_SEL_32	;Selector que usa en el handler
db 0
db 8Fh			;DPL:0 Excepcion
dw 0			;Offset (31-16)

Excepcion_10:
dw 0			;Offset (15-0)
dw CS_SEL_32	;Selector que usa en el handler
db 0
db 8Fh			;DPL:0 Excepcion
dw 0			;Offset (31-16)

Excepcion_11:
dw 0			;Offset (15-0)
dw CS_SEL_32	;Selector que usa en el handler
db 0
db 8Fh			;DPL:0 Excepcion
dw 0			;Offset (31-16)

Excepcion_12:
dw 0			;Offset (15-0)
dw CS_SEL_32	;Selector que usa en el handler
db 0
db 8Fh			;DPL:0 Excepcion
dw 0			;Offset (31-16)

Excepcion_13:
dw 0			;Offset (15-0)
dw CS_SEL_32	;Selector que usa en el handler
db 0
db 8Fh			;DPL:0 Excepcion
dw 0			;Offset (31-16)

Excepcion_14:
dw 0			;Offset (15-0)
dw CS_SEL_32	;Selector que usa en el handler
db 0
db 8Fh			;DPL:0 Excepcion
dw 0			;Offset (31-16)

Excepcion_15:
dw 0			;Offset (15-0)
dw CS_SEL_32	;Selector que usa en el handler
db 0
db 8Fh			;DPL:0 Excepcion
dw 0			;Offset (31-16)

Excepcion_16:
dw 0			;Offset (15-0)
dw CS_SEL_32	;Selector que usa en el handler
db 0
db 8Fh			;DPL:0 Excepcion
dw 0			;Offset (31-16)

Excepcion_17:
dw 0			;Offset (15-0)
dw CS_SEL_32	;Selector que usa en el handler
db 0
db 8Fh			;DPL:0 Excepcion
dw 0			;Offset (31-16)

Excepcion_18:
dw 0			;Offset (15-0)
dw CS_SEL_32	;Selector que usa en el handler
db 0
db 8Fh			;DPL:0 Excepcion
dw 0			;Offset (31-16)

Excepcion_19:
dw 0			;Offset (15-0)
dw CS_SEL_32	;Selector que usa en el handler
db 0
db 8Fh			;DPL:0 Excepcion
dw 0			;Offset (31-16)

;Excepciones desde la 20-31 reservados
times 12	dq 0

;Interrupciones
Interrupcion_0:
dw 0			;Offset (15-0)
dw CS_SEL_32	;Selector que usa en el handler
db 0
db 8Eh			;DPL:0 Excepcion
dw 0			;Offset (31-16)

Interrupcion_1:
dw 0			;Offset (15-0)
dw CS_SEL_32	;Selector que usa en el handler
db 0
db 8Eh			;DPL:0 Excepcion
dw 0			;Offset (31-16)

Interrupcion_2:
dw 0			;Offset (15-0)
dw CS_SEL_32	;Selector que usa en el handler
db 0
db 8Eh			;DPL:0 Excepcion
dw 0			;Offset (31-16)

Interrupcion_3:
dw 0			;Offset (15-0)
dw CS_SEL_32	;Selector que usa en el handler
db 0
db 8Eh			;DPL:0 Excepcion
dw 0			;Offset (31-16)

Interrupcion_4:
dw 0			;Offset (15-0)
dw CS_SEL_32	;Selector que usa en el handler
db 0
db 8Eh			;DPL:0 Excepcion
dw 0			;Offset (31-16)

Interrupcion_5:
dw 0			;Offset (15-0)
dw CS_SEL_32	;Selector que usa en el handler
db 0
db 8Eh			;DPL:0 Excepcion
dw 0			;Offset (31-16)

Interrupcion_6:
dw 0			;Offset (15-0)
dw CS_SEL_32	;Selector que usa en el handler
db 0
db 8Eh			;DPL:0 Excepcion
dw 0			;Offset (31-16)

Interrupcion_7:
dw 0			;Offset (15-0)
dw CS_SEL_32	;Selector que usa en el handler
db 0
db 8Eh			;DPL:0 Excepcion
dw 0			;Offset (31-16)

Interrupcion_8:
dw 0			;Offset (15-0)
dw CS_SEL_32	;Selector que usa en el handler
db 0
db 8Eh			;DPL:0 Excepcion
dw 0			;Offset (31-16)

Interrupcion_9:
dw 0			;Offset (15-0)
dw CS_SEL_32	;Selector que usa en el handler
db 0
db 8Eh			;DPL:0 Excepcion
dw 0			;Offset (31-16)

Interrupcion_10:
dw 0			;Offset (15-0)
dw CS_SEL_32	;Selector que usa en el handler
db 0
db 8Eh			;DPL:0 Excepcion
dw 0			;Offset (31-16)

Interrupcion_11:
dw 0			;Offset (15-0)
dw CS_SEL_32	;Selector que usa en el handler
db 0
db 8Eh			;DPL:0 Excepcion
dw 0			;Offset (31-16)

Interrupcion_12:
dw 0			;Offset (15-0)
dw CS_SEL_32	;Selector que usa en el handler
db 0
db 8Eh			;DPL:0 Excepcion
dw 0			;Offset (31-16)

Interrupcion_13:
dw 0			;Offset (15-0)
dw CS_SEL_32	;Selector que usa en el handler
db 0
db 8Eh			;DPL:0 Excepcion
dw 0			;Offset (31-16)

Interrupcion_14:
dw 0			;Offset (15-0)
dw CS_SEL_32	;Selector que usa en el handler
db 0
db 8Eh			;DPL:0 Excepcion
dw 0			;Offset (31-16)

Interrupcion_15:
dw 0			;Offset (15-0)
dw CS_SEL_32	;Selector que usa en el handler
db 0
db 8Eh			;DPL:0 Excepcion
dw 0			;Offset (31-16)

idtr_img: dw $-IDT
		  dd IDT
;********************************************************************************