/***************************************************************************************************************************/
/* Tarea 4 - PL=3 - Codigo en C para busqueda de valor maximo en tabla de digitos*/
/***************************************************************************************************************************/
__attribute__((section(".data_tarea4"))) unsigned int busqueda_maximo_c(unsigned int *ptr_tabla, unsigned int long_tabla){
	
	unsigned int i;
	unsigned int maximo = 0;
	
	// Algoritmo basico de busqueda de maximo
	for(i = 0; i < long_tabla; i++){
		if( maximo < *(ptr_tabla + i)){
			maximo = *(ptr_tabla + i);
		}
	}

	return maximo;
}