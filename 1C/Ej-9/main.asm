SECTION .nucleo

GLOBAL main

GLOBAL EXC_handler_0
GLOBAL EXC_handler_1
GLOBAL EXC_handler_2
GLOBAL EXC_handler_3
GLOBAL EXC_handler_4
GLOBAL EXC_handler_5
GLOBAL EXC_handler_6
GLOBAL EXC_handler_7
GLOBAL EXC_handler_8
GLOBAL EXC_handler_9
GLOBAL EXC_handler_10
GLOBAL EXC_handler_11
GLOBAL EXC_handler_12
GLOBAL EXC_handler_13
GLOBAL EXC_handler_14
GLOBAL EXC_handler_15
GLOBAL EXC_handler_16
GLOBAL EXC_handler_17
GLOBAL EXC_handler_18
GLOBAL EXC_handler_19

GLOBAL IRQ_handler_0
GLOBAL IRQ_handler_1
GLOBAL IRQ_handler_2
GLOBAL IRQ_handler_3
GLOBAL IRQ_handler_4
GLOBAL IRQ_handler_5
GLOBAL IRQ_handler_6
GLOBAL IRQ_handler_7
GLOBAL IRQ_handler_8
GLOBAL IRQ_handler_9
GLOBAL IRQ_handler_10
GLOBAL IRQ_handler_11
GLOBAL IRQ_handler_12
GLOBAL IRQ_handler_13
GLOBAL IRQ_handler_14
GLOBAL IRQ_handler_15

EXTERN      INICIO_TABLA
EXTERN      FIN_TABLA
EXTERN      BUFFER_TABLA
EXTERN      CONTADOR_TABLA
EXTERN      Excepcion_0

main:

;/*** CODIGO DE PRUEBA VIDEO ***/
;	 mov ax, DS_SEL_32
;    mov ds, ax
;    MOV BL, "A" ; Caracter
;    MOV CL, 0   ; Fila
;    MOV ESI, 0xb8000   ; Puntero
;CICLO_EXTERNO:
;    MOV CH, 0   ; Columna
;CICLO_INTERNO:
;    MOV [ESI], BL  ; Escribo en pantalla
;    MOV BYTE [ESI+1], 0x07
;    ADD ESI, 2  ; Incremento el puntero
;    INC CH     ; Incremento columna
;    CMP CH, 80 
;    JNE CICLO_INTERNO
;    INC CL    ; Incremento la fila
;    INC BL    ; Incremento el caracter
;    CMP CL, 25
;    JNE CICLO_EXTERNO
;/******************************/
;	hlt
    JMP $

SECTION .ISR

;/************** #DE = DIVIDE ERROR *************/
EXC_handler_0:
	xchg bx,bx
	xor bx,bx
    mov BL, "D" 			; Caracter
    mov CL, 0   			; Fila
    mov ESI, 0xB8000   		; Puntero
    mov CH, 0   			; Columna
    mov [ESI], BL  			; Escribo en pantalla
    mov BYTE [ESI+1], 0x07  ; Atributo

    inc CH					; Incremento Columna
    add ESI,2				; Incremento puntero
    mov BL, "E"
    mov [ESI], BL
    mov BYTE [ESI+1], 0x07
    hlt


;/************** #DB = Reserved INTEL *************/
EXC_handler_1:
	xchg bx,bx
	xor bx,bx
    mov BL, "D" 			; Caracter
    mov CL, 0   			; Fila
    mov ESI, 0xB8000   		; Puntero
    mov CH, 0   			; Columna
    mov [ESI], BL  			; Escribo en pantalla
    mov BYTE [ESI+1], 0x07  ; Atributo

    inc CH					; Incremento Columna
    add ESI,2				; Incremento puntero
    mov BL, "B"
    mov [ESI], BL
    mov BYTE [ESI+1], 0x07


;/************** NMI *************/
EXC_handler_2:
	xchg bx,bx
	xor bx,bx
    mov BL, "N" 			; Caracter
    mov CL, 0   			; Fila
    mov ESI, 0xB8000   		; Puntero
    mov CH, 0   			; Columna
    mov [ESI], BL  			; Escribo en pantalla
    mov BYTE [ESI+1], 0x07  ; Atributo

    inc CH					; Incremento Columna
    add ESI,2				; Incremento puntero
    mov BL, "M"
    mov [ESI], BL
    mov BYTE [ESI+1], 0x07

    inc CH					; Incremento Columna
    add ESI,2				; Incremento puntero
    mov BL, "I"
    mov [ESI], BL
    mov BYTE [ESI+1], 0x07


;/************* #BP = Breakpoint *******************/
EXC_handler_3:
	xchg bx,bx
	xor bx,bx
    mov BL, "B" 			; Caracter
    mov CL, 0   			; Fila
    mov ESI, 0xB8000   		; Puntero
    mov CH, 0   			; Columna
    mov [ESI], BL  			; Escribo en pantalla
    mov BYTE [ESI+1], 0x07  ; Atributo

    inc CH					; Incremento Columna
    add ESI,2				; Incremento puntero
    mov BL, "P"
    mov [ESI], BL
    mov BYTE [ESI+1], 0x07

;/************* #OF = overflow ********************/
EXC_handler_4:
	xchg bx,bx
	xor bx,bx
    mov BL, "O" 			; Caracter
    mov CL, 0   			; Fila
    mov ESI, 0xB8000   		; Puntero
    mov CH, 0   			; Columna
    mov [ESI], BL  			; Escribo en pantalla
    mov BYTE [ESI+1], 0x07  ; Atributo

    inc CH					; Incremento Columna
    add ESI,2				; Incremento puntero
    mov BL, "F"
    mov [ESI], BL
    mov BYTE [ESI+1], 0x07

;/************** #BR = Bound range *****************/
EXC_handler_5:
	xchg bx,bx
	xor bx,bx
    mov BL, "B" 			; Caracter
    mov CL, 0   			; Fila
    mov ESI, 0xB8000   		; Puntero
    mov CH, 0   			; Columna
    mov [ESI], BL  			; Escribo en pantalla
    mov BYTE [ESI+1], 0x07  ; Atributo

    inc CH					; Incremento Columna
    add ESI,2				; Incremento puntero
    mov BL, "R"
    mov [ESI], BL
    mov BYTE [ESI+1], 0x07

;/************* #UD = Invalid opcode **************/
EXC_handler_6:
	xchg bx,bx
	xor bx,bx
    mov BL, "U" 			; Caracter
    mov CL, 0   			; Fila
    mov ESI, 0xB8000   		; Puntero
    mov CH, 0   			; Columna
    mov [ESI], BL  			; Escribo en pantalla
    mov BYTE [ESI+1], 0x07  ; Atributo

    inc CH					; Incremento Columna
    add ESI,2				; Incremento puntero
    mov BL, "D"
    mov [ESI], BL
    mov BYTE [ESI+1], 0x07
    hlt

;/************ #NM = device not available **************/
EXC_handler_7:
	xchg bx,bx
	xor bx,bx
    mov BL, "N" 			; Caracter
    mov CL, 0   			; Fila
    mov ESI, 0xB8000   		; Puntero
    mov CH, 0   			; Columna
    mov [ESI], BL  			; Escribo en pantalla
    mov BYTE [ESI+1], 0x07  ; Atributo

    inc CH					; Incremento Columna
    add ESI,2				; Incremento puntero
    mov BL, "M"
    mov [ESI], BL
    mov BYTE [ESI+1], 0x07

;/************* #DF = DOUBLE FAULT **************/
EXC_handler_8:
	xchg bx,bx
	xor bx,bx
    mov BL, "D" 			; Caracter
    mov CL, 0   			; Fila
    mov ESI, 0xB8000   		; Puntero
    mov CH, 0   			; Columna
    mov [ESI], BL  			; Escribo en pantalla
    mov BYTE [ESI+1], 0x07  ; Atributo

    inc CH					; Incremento Columna
    add ESI,2				; Incremento puntero
    mov BL, "F"
    mov [ESI], BL
    mov BYTE [ESI+1], 0x07
    hlt

;/************ Coprocessor Segment Overrun ***********/
EXC_handler_9:
	xchg bx,bx
	xor bx,bx
    mov BL, "C" 			; Caracter
    mov CL, 0   			; Fila
    mov ESI, 0xB8000   		; Puntero
    mov CH, 0   			; Columna
    mov [ESI], BL  			; Escribo en pantalla
    mov BYTE [ESI+1], 0x07  ; Atributo

    inc CH					; Incremento Columna
    add ESI,2				; Incremento puntero
    mov BL, "S"
    mov [ESI], BL
    mov BYTE [ESI+1], 0x07

    inc CH					; Incremento Columna
    add ESI,2				; Incremento puntero
    mov BL, "O"
    mov [ESI], BL
    mov BYTE [ESI+1], 0x07


;/************* #TS = INVALID TSS***************/
EXC_handler_10:
	xchg bx,bx
	xor bx,bx
    mov BL, "T" 			; Caracter
    mov CL, 0   			; Fila
    mov ESI, 0xB8000   		; Puntero
    mov CH, 0   			; Columna
    mov [ESI], BL  			; Escribo en pantalla
    mov BYTE [ESI+1], 0x07  ; Atributo

    inc CH					; Incremento Columna
    add ESI,2				; Incremento puntero
    mov BL, "S"
    mov [ESI], BL
    mov BYTE [ESI+1], 0x07

;/********* #NP = SEGMENT NOT PRESENT***********/
EXC_handler_11:
	xchg bx,bx
	xor bx,bx
    mov BL, "N" 			; Caracter
    mov CL, 0   			; Fila
    mov ESI, 0xB8000   		; Puntero
    mov CH, 0   			; Columna
    mov [ESI], BL  			; Escribo en pantalla
    mov BYTE [ESI+1], 0x07  ; Atributo

    inc CH					; Incremento Columna
    add ESI,2				; Incremento puntero
    mov BL, "P"
    mov [ESI], BL
    mov BYTE [ESI+1], 0x07

;/********* #SS = STACK SEGMENT FAULT***********/
EXC_handler_12:
	xchg bx,bx
	xor bx,bx
    mov BL, "S" 			; Caracter
    mov CL, 0   			; Fila
    mov ESI, 0xB8000   		; Puntero
    mov CH, 0   			; Columna
    mov [ESI], BL  			; Escribo en pantalla
    mov BYTE [ESI+1], 0x07  ; Atributo

    inc CH					; Incremento Columna
    add ESI,2				; Incremento puntero
    mov BL, "S"
    mov [ESI], BL
    mov BYTE [ESI+1], 0x07

;/********* #GP = GENERAL PROTECTION* ***********/
EXC_handler_13:

	xchg bx,bx
	xor bx,bx
    mov BL, "G" 			; Caracter
    mov CL, 0   			; Fila
    mov ESI, 0xB8000   		; Puntero
    mov CH, 0   			; Columna
    mov [ESI], BL  			; Escribo en pantalla
    mov BYTE [ESI+1], 0x07  ; Atributo

    inc CH					; Incremento Columna
    add ESI,2				; Incremento puntero
    mov BL, "P"
    mov [ESI], BL
    mov BYTE [ESI+1], 0x07
    hlt

;/************* #PF = PAGE FAULT ****************/
EXC_handler_14:
	xchg bx,bx
	xor bx,bx
    mov BL, "P" 			; Caracter
    mov CL, 0   			; Fila
    mov ESI, 0xB8000   		; Puntero
    mov CH, 0   			; Columna
    mov [ESI], BL  			; Escribo en pantalla
    mov BYTE [ESI+1], 0x07  ; Atributo

    inc CH					; Incremento Columna
    add ESI,2				; Incremento puntero
    mov BL, "F"
    mov [ESI], BL
    mov BYTE [ESI+1], 0x07
    hlt

;/*************** Intel reserved, do not use *************/
EXC_handler_15:
;/********************************************************/

;/*************** #MF = x87 FPU Floating point *****************/
EXC_handler_16:
	xchg bx,bx
	xor bx,bx
    mov BL, "M" 			; Caracter
    mov CL, 0   			; Fila
    mov ESI, 0xB8000   		; Puntero
    mov CH, 0   			; Columna
    mov [ESI], BL  			; Escribo en pantalla
    mov BYTE [ESI+1], 0x07  ; Atributo

    inc CH					; Incremento Columna
    add ESI,2				; Incremento puntero
    mov BL, "F"
    mov [ESI], BL
    mov BYTE [ESI+1], 0x07

;/*************** #AC = Alignment Check *************************/
EXC_handler_17:
	xchg bx,bx
	xor bx,bx
    mov BL, "A" 			; Caracter
    mov CL, 0   			; Fila
    mov ESI, 0xB8000   		; Puntero
    mov CH, 0   			; Columna
    mov [ESI], BL  			; Escribo en pantalla
    mov BYTE [ESI+1], 0x07  ; Atributo

    inc CH					; Incremento Columna
    add ESI,2				; Incremento puntero
    mov BL, "C"
    mov [ESI], BL
    mov BYTE [ESI+1], 0x07

;/**************** #MC = Machine check ***************************/
EXC_handler_18:
	xchg bx,bx
	xor bx,bx
    mov BL, "M" 			; Caracter
    mov CL, 0   			; Fila
    mov ESI, 0xB8000   		; Puntero
    mov CH, 0   			; Columna
    mov [ESI], BL  			; Escribo en pantalla
    mov BYTE [ESI+1], 0x07  ; Atributo

    inc CH					; Incremento Columna
    add ESI,2				; Incremento puntero
    mov BL, "C"
    mov [ESI], BL
    mov BYTE [ESI+1], 0x07

;/******************* #XF = SIMD Floating point exc *******************/
EXC_handler_19:
	xchg bx,bx
	xor bx,bx
    mov BL, "X" 			; Caracter
    mov CL, 0   			; Fila
    mov ESI, 0xB8000   		; Puntero
    mov CH, 0   			; Columna
    mov [ESI], BL  			; Escribo en pantalla
    mov BYTE [ESI+1], 0x07  ; Atributo

    inc CH					; Incremento Columna
    add ESI,2				; Incremento puntero
    mov BL, "F"
    mov [ESI], BL
    mov BYTE [ESI+1], 0x07

;/***************TIMER****************/
IRQ_handler_0:
	pushad

	xor ebx, ebx

	;/* Para probar cantidad de veces que interrumpe
	;mov ebx, [CONTADOR_TABLA]
	;inc ebx
	;mov [CONTADOR_TABLA], ebx
	;/**********************************************

	mov al, 0x20							;Limpiamos flag de interrupcion, para que pueda volver a interrumpir
	out 0x20, al
	popad
	iretd

;/*****************KEYBOARD******************/
IRQ_handler_1:
	;xchg bx, bx
	pushad
	
	xor ebx, ebx
	mov ecx, BUFFER_TABLA

	in al, 0x60								;Leo puerto de entrada
	mov bl, al
	;xchg bx,bx
	
	cmp al, 0x1F							;¿Se presionó la S?
	je	make_S
	;jmp salida_IRQ1						; Era solo cuando probaba el codigo de la S

	cmp al, 0x15
	je make_Y

	cmp al, 0x16
	je make_U

	cmp al, 0x17 
	je make_I

	cmp al, 0x18 
	je make_O

	cmp al, 0x19
	je make_P

	cmp al, 0x1E    ;  Es la A?
	je es_A

	cmp al, 0x30    ;  Es la B?
	je es_B
	
	cmp al, 0x2E    ;  Es la C?
	je es_C


	cmp al, 0x20    ;  Es la D?
	je es_D 


	cmp al, 0x12    ;  Es la E?
	je es_E 


	cmp al, 0x21    ;  Es la F?
	je es_F 
	jmp no_es_letra

	es_A:
		mov bl, 0x0A
		je es_valida    ;  Es la A, la escribo en buffer

	es_B:
		mov bl, 0xB
		je es_valida    ;  Es la B, la escribo en buffer

	es_C:
		mov bl, 0xC
		je es_valida    ;  Es la C, la escribo en buffer

	es_D:
		mov bl, 0xD
		je es_valida    ;  Es la D, la escribo en buffer

	es_E:
		mov bl, 0xE 
		je es_valida    ;  Es la E, la escribo en buffer

	es_F:
		mov bl, 0xF 
		je es_valida    ;  Es la F, la escribo en buffer


	no_es_letra:
	; ¿Es ENTER? MakeCode = 0x1C
	cmp al, 0x1C
	je aprete_enter

	; Si llego hasta acá quiere decir que no es ninguna de las letras entre A-F
	; Me fijo si es un número
	; El make code más bajo es el del 1 (0x02) y el más alto el del 0 (0x0B). Son todos contiguos.

	cmp al, 0x02
	jae higherorequal_1 ; Igual o más grande que el make code del 1

	higherorequal_1:
    	cmp al, 0x0B
    	jbe es_numero   ; Más chico o igual que el make code del 0 -> es número
    	jmp salida_IRQ1 ; Si no cumple, salgo de la interrupcion

	es_numero:
    	;xchg bx,bx
    	dec bl 			;El make code de los numeros es mas grande en una unidad que el numero correspondiente
    	jmp es_valida


    ; Valida la LETRA
	es_valida:

		mov edx, [CONTADOR_TABLA]

		cmp edx, 0
		je primera

		cmp edx, 0x0F ; antes 0x10
		;mov [ecx], al
		je reiniciar_contador


		add ecx, edx

    	mov [ecx], bl
    	inc edx
    	mov [CONTADOR_TABLA], edx
    	jmp salida_IRQ1

    primera:
    	mov [ecx], bl
    	inc edx
    	mov [CONTADOR_TABLA], edx
    	jmp salida_IRQ1

    reiniciar_contador:
    	;mov [ecx], al
    	;mov [BUFFER_TABLA], al
    	add ecx, edx
    	mov [ecx], bl
    	mov edx, 0
    	mov [CONTADOR_TABLA], edx
    	jmp salida_IRQ1

	aprete_enter:
    	;Guardar en tabla
    	xchg bx,bx
    	cld
		mov esi, BUFFER_TABLA			;Fuente
		mov edi, INICIO_TABLA 			;Destino
		mov ecx, FIN_TABLA				;End
		sub ecx, BUFFER_TABLA			;Start
		rep movsb 
    	;Borrar buffer: KHE BERGHA
    	;Borrar contador:
    	;;mov [CONTADOR_TABLA], 0
    	mov ecx, 0
    	mov [CONTADOR_TABLA],ecx
    	jmp salida_IRQ1
	
	make_S:
		xchg bx,bx
		xor bx,bx
    	mov BL, "I" 			; Caracter
    	mov CL, 0   			; Fila
    	mov ESI, 0xB8000   		; Puntero
    	mov CH, 0   			; Columna
    	mov [ESI], BL  			; Escribo en pantalla
    	mov BYTE [ESI+1], 0x07  ; Atributo

    	inc CH					; Incremento Columna
    	add ESI,2				; Incremento puntero
    	mov BL, "1"
    	mov [ESI], BL
    	mov BYTE [ESI+1], 0x07

    	;/***PRUEBA EXC0****/
    	xor edx, edx
    	xor ecx, ecx
    	xor eax, eax
    	mov eax, 0x10
    	div ecx

		jmp salida_IRQ1

	; Genero la #DE (Divide error) = EXC0
	make_Y:
		xor edx, edx
    	xor ecx, ecx
    	xor eax, eax
    	mov eax, 0x10
    	div ecx
    	jmp salida_IRQ1

    ; Genero la #UD (Invalid opcode) = EXC6
    make_U:
    	; Manual INTEL: The UD2 instruction is guaranteed to generate an invalid opcode exception.
    	ud2
    	jmp salida_IRQ1

    ; Genero la #DF (Double Fault) = EXC8 
    make_I:
    	; Le saco el bit de presente a la Excepcion 0

    	xor eax, eax
    	mov ax, 0x0F 						; Con ese valor borro el bit de presente.
    	mov word [Excepcion_0+5], ax 

    	; Genero la EXC0
    	xor edx, edx
    	xor ecx, ecx
    	xor eax, eax
    	mov eax, 0x10
    	div ecx
    	jmp salida_IRQ1

    ; Genero la #GP (General protection) = EXC13
    make_O:
    	; Manual INTEL: Loading the SS register with the segment selector of an executable segment or a null segment selector.
    	; Otra forma: Attempting to write a 1 into a reserved bit of CR4.
    	mov ax, 0x00
		mov ss, ax
    	jmp salida_IRQ1

    ; Genero la #PF (Page fault) = EXC14
    make_P:
    	jmp salida_IRQ1

		
	salida_IRQ1:							;Le aviso al 1er PIC que terminó el Handler 
	mov al, 0x20							;Limpiamos flag de interrupcion, para que pueda volver a interrumpir
	out 0x20, al

	popad
	iretd

IRQ_handler_2:
	pushad
	mov al, 0x20							;Limpiamos flag de interrupcion, para que pueda volver a interrumpir
	out 0x20, al
	popad
	iretd

IRQ_handler_3:
	pushad
	mov al, 0x20							;Limpiamos flag de interrupcion, para que pueda volver a interrumpir
	out 0x20, al
	popad
	iretd

IRQ_handler_4:
	pushad
	mov al, 0x20							;Limpiamos flag de interrupcion, para que pueda volver a interrumpir
	out 0x20, al
	popad
	iretd

IRQ_handler_5:
	pushad
	mov al, 0x20							;Limpiamos flag de interrupcion, para que pueda volver a interrumpir
	out 0x20, al
	popad
	iretd

IRQ_handler_6:
	pushad
	mov al, 0x20							;Limpiamos flag de interrupcion, para que pueda volver a interrumpir
	out 0x20, al
	popad
	iretd

IRQ_handler_7:
	pushad
	mov al, 0x20							;Limpiamos flag de interrupcion, para que pueda volver a interrumpir
	out 0x20, al
	popad
	iretd

IRQ_handler_8:
	pushad
	mov al, 0x20							;Limpiamos flag de interrupcion, para que pueda volver a interrumpir
	out 0x20, al
	popad
	iretd

IRQ_handler_9:
	pushad
	mov al, 0x20							;Limpiamos flag de interrupcion, para que pueda volver a interrumpir
	out 0x20, al
	popad
	iretd

IRQ_handler_10:
	pushad
	mov al, 0x20							;Limpiamos flag de interrupcion, para que pueda volver a interrumpir
	out 0x20, al
	popad
	iretd

IRQ_handler_11:
	pushad
	mov al, 0x20							;Limpiamos flag de interrupcion, para que pueda volver a interrumpir
	out 0x20, al
	popad
	iretd

IRQ_handler_12:
	pushad
	mov al, 0x20							;Limpiamos flag de interrupcion, para que pueda volver a interrumpir
	out 0x20, al
	popad
	iretd

IRQ_handler_13:
	pushad
	mov al, 0x20							;Limpiamos flag de interrupcion, para que pueda volver a interrumpir
	out 0x20, al
	popad
	iretd

IRQ_handler_14:
	pushad
	mov al, 0x20							;Limpiamos flag de interrupcion, para que pueda volver a interrumpir
	out 0x20, al
	popad
	iretd

IRQ_handler_15:
	pushad
	mov al, 0x20							;Limpiamos flag de interrupcion, para que pueda volver a interrumpir
	out 0x20, al
	popad
	iretd


;/*******************************************************************/