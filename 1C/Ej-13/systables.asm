SECTION .sys_tables

GLOBAL CS_SEL_32
GLOBAL DS_SEL_32
GLOBAL GDT_tam_32
GLOBAL GDT_32
GLOBAL im_gdtr_32

GLOBAL IDT_32
GLOBAL idtr_img
GLOBAL IDT

GLOBAL Excepcion_0
GLOBAL Excepcion_1
GLOBAL Excepcion_2
GLOBAL Excepcion_3
GLOBAL Excepcion_4
GLOBAL Excepcion_5
GLOBAL Excepcion_6
GLOBAL Excepcion_7
GLOBAL Excepcion_8
GLOBAL Excepcion_9
GLOBAL Excepcion_10
GLOBAL Excepcion_11
GLOBAL Excepcion_12
GLOBAL Excepcion_13
GLOBAL Excepcion_14
GLOBAL Excepcion_15
GLOBAL Excepcion_16
GLOBAL Excepcion_17
GLOBAL Excepcion_18
GLOBAL Excepcion_19

GLOBAL Interrupcion_0
GLOBAL Interrupcion_1
GLOBAL Interrupcion_2
GLOBAL Interrupcion_3
GLOBAL Interrupcion_4
GLOBAL Interrupcion_5
GLOBAL Interrupcion_6
GLOBAL Interrupcion_7
GLOBAL Interrupcion_8
GLOBAL Interrupcion_9
GLOBAL Interrupcion_10
GLOBAL Interrupcion_11
GLOBAL Interrupcion_12
GLOBAL Interrupcion_13
GLOBAL Interrupcion_14
GLOBAL Interrupcion_15



GDT_32:

	dq 0 ;selector nulo

    ; Quiero el selector de código en: 0xFFFF0000
    CS_SEL_32 equ $-GDT_32
		
		dw 0xffff   ; Limite 15-0
        dw 0		 ; Base 15-0
        db 0x00     ; Base 23-16
        db 0x9a     ; Derechos de Acceso: 0b10011010. De izquierda a derecha:
         			; Bit 7: Segmento presente. Bit 6-5: DPL=00 (Maximo privilegio: Kernel)
         			; Bi4 4: '1' Para código o datos. Bit 3: '1' para código
         			; Bit 2: '0' (no) Segmento conforme. Bit 1: '1' permiso de lectura.
         			; Bit 0: Accedido
        db 0xcf	    ; 0b11001111. Bit 7: Granularidad=1. Bit 6: Default=1 (Segmento 32 bits)
         			; Bit 5-4 = 00 por definicion.
         			; Limite 19-16 = f
        db 0x00     ; Base 31-24 = 0xff 

        ; Me queda: BASE = 0xFFFF0000 y LIMITE = 0xFFFFF y se completa con tres F más por tener G=1
        ; Si fuera G=0 el limite es el que figura en el descriptor

    ; Quiero el selector de datos en: 0x00000000
    DS_SEL_32 equ $-GDT_32
      
        dw 0xffff   ; Limite 15-0
        dw 0		; Base 15-0
        db 0		; Base 23-16 
        db 0x92	    ; Derechos de Acceso: 0b10010010. De izquierda a derecha:
         			; Bit 7: Segmento presente. Bit 6-5: DPL=00 (Maximo privilegio: Kernel)
         			; Bi4 4: '1' Para código o datos. Bit 3: '0' para datos
         			; Bit 2: '0' (no) Segmento conforme. Bit 1: '1' permiso de lectura.
         			; Bit 0: Accedido
        db 0xcf     ; 0b11001111. Bit 7: Granularidad=1. Bit 6: Default=1 (Segmento 32 bits)
         			; Bit 5-4 = 00 por definicion.
         			; Limite 19-16 = f
        db 0		; Base 31-24 = 0x00 

        ; Me queda BASE = 0x00000000 y LIMITE = 0xFFFFF y tres F más por tener G=1 (0xFFFFFFFF)
      
    GDT_tam_32 equ $-GDT_32

 	;0   15 16         47
    ;|LIMIT|----BASE----|

im_gdtr_32:
    dw  GDT_tam_32 -1		; Limite (Tamaño de la GDT)
      					    ; LIMIT is 1 less than the length of the table, so if LIMIT has the value 15, then the GDT is 16 bytes long.

    dd  GDT_32   ; Dir. Lineal Base (Donde empieza la GDT)


;***************IDT_32*****************/
IDT:

Excepcion_0:
dw 0			;Offset (15-0)
dw CS_SEL_32	;Selector que usa en el handler
db 0
db 8Fh			;DPL:0 Excepcion
dw 0			;Offset (31-16)

Excepcion_1:
dw 0			;Offset (15-0)
dw CS_SEL_32	;Selector que usa en el handler
db 0
db 8Fh			;DPL:0 Excepcion
dw 0			;Offset (31-16)

Excepcion_2:
dw 0			;Offset (15-0)
dw CS_SEL_32	;Selector que usa en el handler
db 0
db 8Fh			;DPL:0 Excepcion
dw 0			;Offset (31-16)

Excepcion_3:
dw 0			;Offset (15-0)
dw CS_SEL_32	;Selector que usa en el handler
db 0
db 8Fh			;DPL:0 Excepcion
dw 0			;Offset (31-16)

Excepcion_4:
dw 0			;Offset (15-0)
dw CS_SEL_32	;Selector que usa en el handler
db 0
db 8Fh			;DPL:0 Excepcion
dw 0			;Offset (31-16)

Excepcion_5:
dw 0			;Offset (15-0)
dw CS_SEL_32	;Selector que usa en el handler
db 0
db 8Fh			;DPL:0 Excepcion
dw 0			;Offset (31-16)

Excepcion_6:
dw 0			;Offset (15-0)
dw CS_SEL_32	;Selector que usa en el handler
db 0
db 8Fh			;DPL:0 Excepcion
dw 0			;Offset (31-16)

Excepcion_7:
dw 0			;Offset (15-0)
dw CS_SEL_32	;Selector que usa en el handler
db 0
db 8Fh			;DPL:0 Excepcion
dw 0			;Offset (31-16)

Excepcion_8:
dw 0			;Offset (15-0)
dw CS_SEL_32	;Selector que usa en el handler
db 0
db 8Fh			;DPL:0 Excepcion
dw 0			;Offset (31-16)

Excepcion_9:
dw 0			;Offset (15-0)
dw CS_SEL_32	;Selector que usa en el handler
db 0
db 8Fh			;DPL:0 Excepcion
dw 0			;Offset (31-16)

Excepcion_10:
dw 0			;Offset (15-0)
dw CS_SEL_32	;Selector que usa en el handler
db 0
db 8Fh			;DPL:0 Excepcion
dw 0			;Offset (31-16)

Excepcion_11:
dw 0			;Offset (15-0)
dw CS_SEL_32	;Selector que usa en el handler
db 0
db 8Fh			;DPL:0 Excepcion
dw 0			;Offset (31-16)

Excepcion_12:
dw 0			;Offset (15-0)
dw CS_SEL_32	;Selector que usa en el handler
db 0
db 8Fh			;DPL:0 Excepcion
dw 0			;Offset (31-16)

Excepcion_13:
dw 0			;Offset (15-0)
dw CS_SEL_32	;Selector que usa en el handler
db 0
db 8Fh			;DPL:0 Excepcion
dw 0			;Offset (31-16)

Excepcion_14:
dw 0			;Offset (15-0)
dw CS_SEL_32	;Selector que usa en el handler
db 0
db 8Fh			;DPL:0 Excepcion
dw 0			;Offset (31-16)

Excepcion_15:
dw 0			;Offset (15-0)
dw CS_SEL_32	;Selector que usa en el handler
db 0
db 8Fh			;DPL:0 Excepcion
dw 0			;Offset (31-16)

Excepcion_16:
dw 0			;Offset (15-0)
dw CS_SEL_32	;Selector que usa en el handler
db 0
db 8Fh			;DPL:0 Excepcion
dw 0			;Offset (31-16)

Excepcion_17:
dw 0			;Offset (15-0)
dw CS_SEL_32	;Selector que usa en el handler
db 0
db 8Fh			;DPL:0 Excepcion
dw 0			;Offset (31-16)

Excepcion_18:
dw 0			;Offset (15-0)
dw CS_SEL_32	;Selector que usa en el handler
db 0
db 8Fh			;DPL:0 Excepcion
dw 0			;Offset (31-16)

Excepcion_19:
dw 0			;Offset (15-0)
dw CS_SEL_32	;Selector que usa en el handler
db 0
db 8Fh			;DPL:0 Excepcion
dw 0			;Offset (31-16)

;Excepciones desde la 20-31 reservados
times 12	dq 0

;Interrupciones
Interrupcion_0:
dw 0			;Offset (15-0)
dw CS_SEL_32	;Selector que usa en el handler
db 0
db 8Eh			;DPL:0 Excepcion
dw 0			;Offset (31-16)

Interrupcion_1:
dw 0			;Offset (15-0)
dw CS_SEL_32	;Selector que usa en el handler
db 0
db 8Eh			;DPL:0 Excepcion
dw 0			;Offset (31-16)

Interrupcion_2:
dw 0			;Offset (15-0)
dw CS_SEL_32	;Selector que usa en el handler
db 0
db 8Eh			;DPL:0 Excepcion
dw 0			;Offset (31-16)

Interrupcion_3:
dw 0			;Offset (15-0)
dw CS_SEL_32	;Selector que usa en el handler
db 0
db 8Eh			;DPL:0 Excepcion
dw 0			;Offset (31-16)

Interrupcion_4:
dw 0			;Offset (15-0)
dw CS_SEL_32	;Selector que usa en el handler
db 0
db 8Eh			;DPL:0 Excepcion
dw 0			;Offset (31-16)

Interrupcion_5:
dw 0			;Offset (15-0)
dw CS_SEL_32	;Selector que usa en el handler
db 0
db 8Eh			;DPL:0 Excepcion
dw 0			;Offset (31-16)

Interrupcion_6:
dw 0			;Offset (15-0)
dw CS_SEL_32	;Selector que usa en el handler
db 0
db 8Eh			;DPL:0 Excepcion
dw 0			;Offset (31-16)

Interrupcion_7:
dw 0			;Offset (15-0)
dw CS_SEL_32	;Selector que usa en el handler
db 0
db 8Eh			;DPL:0 Excepcion
dw 0			;Offset (31-16)

Interrupcion_8:
dw 0			;Offset (15-0)
dw CS_SEL_32	;Selector que usa en el handler
db 0
db 8Eh			;DPL:0 Excepcion
dw 0			;Offset (31-16)

Interrupcion_9:
dw 0			;Offset (15-0)
dw CS_SEL_32	;Selector que usa en el handler
db 0
db 8Eh			;DPL:0 Excepcion
dw 0			;Offset (31-16)

Interrupcion_10:
dw 0			;Offset (15-0)
dw CS_SEL_32	;Selector que usa en el handler
db 0
db 8Eh			;DPL:0 Excepcion
dw 0			;Offset (31-16)

Interrupcion_11:
dw 0			;Offset (15-0)
dw CS_SEL_32	;Selector que usa en el handler
db 0
db 8Eh			;DPL:0 Excepcion
dw 0			;Offset (31-16)

Interrupcion_12:
dw 0			;Offset (15-0)
dw CS_SEL_32	;Selector que usa en el handler
db 0
db 8Eh			;DPL:0 Excepcion
dw 0			;Offset (31-16)

Interrupcion_13:
dw 0			;Offset (15-0)
dw CS_SEL_32	;Selector que usa en el handler
db 0
db 8Eh			;DPL:0 Excepcion
dw 0			;Offset (31-16)

Interrupcion_14:
dw 0			;Offset (15-0)
dw CS_SEL_32	;Selector que usa en el handler
db 0
db 8Eh			;DPL:0 Excepcion
dw 0			;Offset (31-16)

Interrupcion_15:
dw 0			;Offset (15-0)
dw CS_SEL_32	;Selector que usa en el handler
db 0
db 8Eh			;DPL:0 Excepcion
dw 0			;Offset (31-16)

idtr_img: dw $-IDT
		  dd IDT