inicio_reset_vect:

	jmp word 0xf000:0 ; 0xf0000 (selector y offset). El word es para indicar el tamaño ya que jmp es una instr de 32 bits
	times 16 - ($-inicio_reset_vect) db 0

; nasm -f bin reset_vect.s -o reset_vect.bin
; ls