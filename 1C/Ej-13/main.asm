SECTION .datos
INDICE_TABLA		equ		dir_lineal_datos
BUFFER 				equ 	dir_lineal_datos + 4
CONTADOR_BUFFER		equ		BUFFER + 4
TIMER_TICK			equ 	CONTADOR_BUFFER + 4
FLAG_TIMER1 		equ 	TIMER_TICK + 4


SECTION .nucleo

GLOBAL main

GLOBAL EXC_handler_0
GLOBAL EXC_handler_1
GLOBAL EXC_handler_2
GLOBAL EXC_handler_3
GLOBAL EXC_handler_4
GLOBAL EXC_handler_5
GLOBAL EXC_handler_6
GLOBAL EXC_handler_7
GLOBAL EXC_handler_8
GLOBAL EXC_handler_9
GLOBAL EXC_handler_10
GLOBAL EXC_handler_11
GLOBAL EXC_handler_12
GLOBAL EXC_handler_13
GLOBAL EXC_handler_14
GLOBAL EXC_handler_15
GLOBAL EXC_handler_16
GLOBAL EXC_handler_17
GLOBAL EXC_handler_18
GLOBAL EXC_handler_19

GLOBAL IRQ_handler_0
GLOBAL IRQ_handler_1
GLOBAL IRQ_handler_2
GLOBAL IRQ_handler_3
GLOBAL IRQ_handler_4
GLOBAL IRQ_handler_5
GLOBAL IRQ_handler_6
GLOBAL IRQ_handler_7
GLOBAL IRQ_handler_8
GLOBAL IRQ_handler_9
GLOBAL IRQ_handler_10
GLOBAL IRQ_handler_11
GLOBAL IRQ_handler_12
GLOBAL IRQ_handler_13
GLOBAL IRQ_handler_14
GLOBAL IRQ_handler_15


EXTERN      Excepcion_0
EXTERN		dir_lineal_VIDEO
EXTERN   	dir_lineal_datos

EXTERN		dir_lineal_digtable
INICIO_TABLA	equ		dir_lineal_digtable
BUFFER_TABLA	equ		dir_lineal_digtable + 0x1000
FIN_TABLA		equ		BUFFER_TABLA + 0x1000
CONTADOR_TABLA 	equ		FIN_TABLA + 0x1000

main:
	mov eax, [FLAG_TIMER1]
	cmp eax, 1
	jne idle

	llamar_tarea1:
		call tarea_1

	idle:
		hlt
		jmp main
	

; Antes
;	 hlt
;    JMP $


SECTION .data_tarea1
tarea_1:

	; VERIFICAR: Pagine solo 4K de la tabla. hice un resb de 4K o de 64K?
	;xchg bx, bx
	pushad

	xor eax, eax 				; Limpio eax. Aca voy a poner el puntero a la tabla y lo voy a ir moviendo para recorrerla
	xor ecx, ecx 				; Limpio ecx. Contador para manejo del loop
	xor ebx, ebx 				; Variable donde voy a guardar la sumatoria de los digitos de la tabla

	mov eax, INICIO_TABLA 		; Guardo en eax el puntero a la tabla
	mov ecx, 1023   			; Inicializo contador para el manejo del loop 
	 							; En una tabla de 4KB, entran 0x400 numeros de 4 bytes (es decir 1024)
	mov ebx, [INICIO_TABLA]     ; Pongo en la variable sumatoria el valor del primer elemento de la tabla 
	sumatoria:
		add eax, 0x04 			; Incremento el puntero 
		add ebx, [eax] 			; Guardo el valor de la entrada correspondiente en la sumatoria 

		dec ecx 				; Decremento ecx 
		jnz sumatoria  			; Si ecx es cero -> Termine el loop


	; Hasta aca tengo guardada la sumatoria de la tabla en ebx
	; mov ebx, 0x12345678        ; Asignacion para probar que se imprima todo ok

	;xchg bx,bx
	; TEST PF -> 
	; mov eax, [ebx]
	; Imprimo numero en pantalla
	xor eax, eax
	xor ecx, ecx
	mov ecx, 8                   ; Variable que controla el loop de impresion (8 veces)
	mov eax, ebx 				 ; Hago una copia de la sumatoria en eax
    mov esi, dir_lineal_VIDEO    ; Puntero a video
    
    print:
    	mov eax, ebx 			 ; Hago una copia de la sumatoria en eax
    	and eax, 0xF0000000      ; Mascara para quedarme con el byte mas sign. Voy a imprimir del mas sign. al menos
    	shr eax, 4*7 			 ; Muevo el byte mas sign. a la posición menos sign -> 0x0000000X
    	;and al, 0x0F            ; Me quedo con los 4 bits menos significativos (Numero que quiero imprimir)

    	cmp al, 0x09 			 ; Convierto a ascii. Si el numero esta entre 0 y 9, le sumo 48 para la conversion.
    	jae sumar_55 			 ; Si está entre A y F, hay que sumar 55
    	add eax, 48
    	jmp pantalla

    sumar_55:
    	add eax, 55              ; Si estoy entre A y F

    pantalla:
        mov [esi], al            ; Escribo en pantalla
        mov byte [esi+1], 0x07   ; Atributo
        shl ebx,4                ; Shifteo a la izquierda ebx para agarrar el proximo numero que quiero imprimir 

        add esi,2                ; Incremento puntero 

        dec ecx
        jnz print  

 

	mov [FLAG_TIMER1], eax 		; Pongo a cero FLAG_TIMER1

	; Crear una variable que se llame suma y guardarla en algun lado?
	popad
	ret


SECTION .ISR

;/************** #DE = DIVIDE ERROR *************/
EXC_handler_0:
	xchg bx,bx
	xor bx,bx
    mov BL, "D" 			; Caracter
    mov CL, 0   			; Fila
    ;mov ESI, 0xB8000   		; Puntero
    mov esi, dir_lineal_VIDEO
    mov CH, 0   			; Columna
    mov [ESI], BL  			; Escribo en pantalla
    mov BYTE [ESI+1], 0x07  ; Atributo

    inc CH					; Incremento Columna
    add ESI,2				; Incremento puntero
    mov BL, "E"
    mov [ESI], BL
    mov BYTE [ESI+1], 0x07
    hlt


;/************** #DB = Reserved INTEL *************/
EXC_handler_1:
	xchg bx,bx
	xor bx,bx
    mov BL, "D" 			; Caracter
    mov CL, 0   			; Fila
    ;mov ESI, 0xB8000   		; Puntero
    mov esi, dir_lineal_VIDEO
    mov CH, 0   			; Columna
    mov [ESI], BL  			; Escribo en pantalla
    mov BYTE [ESI+1], 0x07  ; Atributo

    inc CH					; Incremento Columna
    add ESI,2				; Incremento puntero
    mov BL, "B"
    mov [ESI], BL
    mov BYTE [ESI+1], 0x07


;/************** NMI *************/
EXC_handler_2:
	xchg bx,bx
	xor bx,bx
    mov BL, "N" 			; Caracter
    mov CL, 0   			; Fila
    ;mov ESI, 0xB8000   		; Puntero
    mov esi, dir_lineal_VIDEO
    mov CH, 0   			; Columna
    mov [ESI], BL  			; Escribo en pantalla
    mov BYTE [ESI+1], 0x07  ; Atributo

    inc CH					; Incremento Columna
    add ESI,2				; Incremento puntero
    mov BL, "M"
    mov [ESI], BL
    mov BYTE [ESI+1], 0x07

    inc CH					; Incremento Columna
    add ESI,2				; Incremento puntero
    mov BL, "I"
    mov [ESI], BL
    mov BYTE [ESI+1], 0x07


;/************* #BP = Breakpoint *******************/
EXC_handler_3:
	xchg bx,bx
	xor bx,bx
    mov BL, "B" 			; Caracter
    mov CL, 0   			; Fila
    ;mov ESI, 0xB8000   		; Puntero
    mov esi, dir_lineal_VIDEO
    mov CH, 0   			; Columna
    mov [ESI], BL  			; Escribo en pantalla
    mov BYTE [ESI+1], 0x07  ; Atributo

    inc CH					; Incremento Columna
    add ESI,2				; Incremento puntero
    mov BL, "P"
    mov [ESI], BL
    mov BYTE [ESI+1], 0x07

;/************* #OF = overflow ********************/
EXC_handler_4:
	xchg bx,bx
	xor bx,bx
    mov BL, "O" 			; Caracter
    mov CL, 0   			; Fila
    ;mov ESI, 0xB8000   		; Puntero
    mov esi, dir_lineal_VIDEO
    mov CH, 0   			; Columna
    mov [ESI], BL  			; Escribo en pantalla
    mov BYTE [ESI+1], 0x07  ; Atributo

    inc CH					; Incremento Columna
    add ESI,2				; Incremento puntero
    mov BL, "F"
    mov [ESI], BL
    mov BYTE [ESI+1], 0x07

;/************** #BR = Bound range *****************/
EXC_handler_5:
	xchg bx,bx
	xor bx,bx
    mov BL, "B" 			; Caracter
    mov CL, 0   			; Fila
    ;mov ESI, 0xB8000   		; Puntero
    mov esi, dir_lineal_VIDEO
    mov CH, 0   			; Columna
    mov [ESI], BL  			; Escribo en pantalla
    mov BYTE [ESI+1], 0x07  ; Atributo

    inc CH					; Incremento Columna
    add ESI,2				; Incremento puntero
    mov BL, "R"
    mov [ESI], BL
    mov BYTE [ESI+1], 0x07

;/************* #UD = Invalid opcode **************/
EXC_handler_6:
	xchg bx,bx
	xor bx,bx
    mov BL, "U" 			; Caracter
    mov CL, 0   			; Fila
    ;mov ESI, 0xB8000   		; Puntero
    mov esi, dir_lineal_VIDEO
    mov CH, 0   			; Columna
    mov [ESI], BL  			; Escribo en pantalla
    mov BYTE [ESI+1], 0x07  ; Atributo

    inc CH					; Incremento Columna
    add ESI,2				; Incremento puntero
    mov BL, "D"
    mov [ESI], BL
    mov BYTE [ESI+1], 0x07
    hlt

;/************ #NM = device not available **************/
EXC_handler_7:
	xchg bx,bx
	xor bx,bx
    mov BL, "N" 			; Caracter
    mov CL, 0   			; Fila
    ;mov ESI, 0xB8000   		; Puntero
    mov esi, dir_lineal_VIDEO
    mov CH, 0   			; Columna
    mov [ESI], BL  			; Escribo en pantalla
    mov BYTE [ESI+1], 0x07  ; Atributo

    inc CH					; Incremento Columna
    add ESI,2				; Incremento puntero
    mov BL, "M"
    mov [ESI], BL
    mov BYTE [ESI+1], 0x07

;/************* #DF = DOUBLE FAULT **************/
EXC_handler_8:
	xchg bx,bx
	xor bx,bx
    mov BL, "D" 			; Caracter
    mov CL, 0   			; Fila
    ;mov ESI, 0xB8000   		; Puntero
    mov esi, dir_lineal_VIDEO
    mov CH, 0   			; Columna
    mov [ESI], BL  			; Escribo en pantalla
    mov BYTE [ESI+1], 0x07  ; Atributo

    inc CH					; Incremento Columna
    add ESI,2				; Incremento puntero
    mov BL, "F"
    mov [ESI], BL
    mov BYTE [ESI+1], 0x07
    hlt

;/************ Coprocessor Segment Overrun ***********/
EXC_handler_9:
	xchg bx,bx
	xor bx,bx
    mov BL, "C" 			; Caracter
    mov CL, 0   			; Fila
    ;mov ESI, 0xB8000   		; Puntero
    mov esi, dir_lineal_VIDEO
    mov CH, 0   			; Columna
    mov [ESI], BL  			; Escribo en pantalla
    mov BYTE [ESI+1], 0x07  ; Atributo

    inc CH					; Incremento Columna
    add ESI,2				; Incremento puntero
    mov BL, "S"
    mov [ESI], BL
    mov BYTE [ESI+1], 0x07

    inc CH					; Incremento Columna
    add ESI,2				; Incremento puntero
    mov BL, "O"
    mov [ESI], BL
    mov BYTE [ESI+1], 0x07


;/************* #TS = INVALID TSS***************/
EXC_handler_10:
	xchg bx,bx
	xor bx,bx
    mov BL, "T" 			; Caracter
    mov CL, 0   			; Fila
    ;mov ESI, 0xB8000   		; Puntero
    mov esi, dir_lineal_VIDEO
    mov CH, 0   			; Columna
    mov [ESI], BL  			; Escribo en pantalla
    mov BYTE [ESI+1], 0x07  ; Atributo

    inc CH					; Incremento Columna
    add ESI,2				; Incremento puntero
    mov BL, "S"
    mov [ESI], BL
    mov BYTE [ESI+1], 0x07

;/********* #NP = SEGMENT NOT PRESENT***********/
EXC_handler_11:
	xchg bx,bx
	xor bx,bx
    mov BL, "N" 			; Caracter
    mov CL, 0   			; Fila
    ;mov ESI, 0xB8000   		; Puntero
    mov esi, dir_lineal_VIDEO
    mov CH, 0   			; Columna
    mov [ESI], BL  			; Escribo en pantalla
    mov BYTE [ESI+1], 0x07  ; Atributo

    inc CH					; Incremento Columna
    add ESI,2				; Incremento puntero
    mov BL, "P"
    mov [ESI], BL
    mov BYTE [ESI+1], 0x07

;/********* #SS = STACK SEGMENT FAULT***********/
EXC_handler_12:
	xchg bx,bx
	xor bx,bx
    mov BL, "S" 			; Caracter
    mov CL, 0   			; Fila
   ;mov ESI, 0xB8000   		; Puntero
    mov esi, dir_lineal_VIDEO
    mov CH, 0   			; Columna
    mov [ESI], BL  			; Escribo en pantalla
    mov BYTE [ESI+1], 0x07  ; Atributo

    inc CH					; Incremento Columna
    add ESI,2				; Incremento puntero
    mov BL, "S"
    mov [ESI], BL
    mov BYTE [ESI+1], 0x07

;/********* #GP = GENERAL PROTECTION* ***********/
EXC_handler_13:

	xchg bx,bx
	xor bx,bx
    mov BL, "G" 			; Caracter
    mov CL, 0   			; Fila
    ;mov ESI, 0xB8000   		; Puntero
    mov esi, dir_lineal_VIDEO
    mov CH, 0   			; Columna
    mov [ESI], BL  			; Escribo en pantalla
    mov BYTE [ESI+1], 0x07  ; Atributo

    inc CH					; Incremento Columna
    add ESI,2				; Incremento puntero
    mov BL, "P"
    mov [ESI], BL
    mov BYTE [ESI+1], 0x07
    hlt

;/************* #PF = PAGE FAULT ****************/
EXC_handler_14:
	; Borro pantalla
	mov edi, dir_lineal_VIDEO 	;Indice destino, para rep stosw
	mov ax, 0x720				;Atributo y caracter espacio
	;mov ax, 0x741				;Atributo y caracter A
	mov cx, 80*25				;Contador al total de caracteres de la pantalla
	cld							;Flag de direcciones a cero, copia ascendente
	rep stosw					;Almacena ax en es:[di] (2 bytes) 

	xchg bx,bx
	xor bx,bx
    mov BL, "P" 			; Caracter
    mov CL, 0   			; Fila
    ;mov ESI, 0xB8000   		; Puntero
    mov esi, dir_lineal_VIDEO
    mov CH, 0   			; Columna
    mov [ESI], BL  			; Escribo en pantalla
    mov BYTE [ESI+1], 0x07  ; Atributo

    inc CH					; Incremento Columna
    add ESI,2				; Incremento puntero
    mov BL, "F"
    mov [ESI], BL
    mov BYTE [ESI+1], 0x07
    hlt

;/*************** Intel reserved, do not use *************/
EXC_handler_15:
;/********************************************************/

;/*************** #MF = x87 FPU Floating point *****************/
EXC_handler_16:
	xchg bx,bx
	xor bx,bx
    mov BL, "M" 			; Caracter
    mov CL, 0   			; Fila
    mov esi, dir_lineal_VIDEO
    ;mov ESI, 0xB8000   		; Puntero
    mov CH, 0   			; Columna
    mov [ESI], BL  			; Escribo en pantalla
    mov BYTE [ESI+1], 0x07  ; Atributo

    inc CH					; Incremento Columna
    add ESI,2				; Incremento puntero
    mov BL, "F"
    mov [ESI], BL
    mov BYTE [ESI+1], 0x07

;/*************** #AC = Alignment Check *************************/
EXC_handler_17:
	xchg bx,bx
	xor bx,bx
    mov BL, "A" 			; Caracter
    mov CL, 0   			; Fila
    mov esi, dir_lineal_VIDEO
    ;mov ESI, 0xB8000   		; Puntero
    mov CH, 0   			; Columna
    mov [ESI], BL  			; Escribo en pantalla
    mov BYTE [ESI+1], 0x07  ; Atributo

    inc CH					; Incremento Columna
    add ESI,2				; Incremento puntero
    mov BL, "C"
    mov [ESI], BL
    mov BYTE [ESI+1], 0x07

;/**************** #MC = Machine check ***************************/
EXC_handler_18:
	xchg bx,bx
	xor bx,bx
    mov BL, "M" 			; Caracter
    mov CL, 0   			; Fila
    mov esi, dir_lineal_VIDEO
    ;mov ESI, 0xB8000   		; Puntero
    mov CH, 0   			; Columna
    mov [ESI], BL  			; Escribo en pantalla
    mov BYTE [ESI+1], 0x07  ; Atributo

    inc CH					; Incremento Columna
    add ESI,2				; Incremento puntero
    mov BL, "C"
    mov [ESI], BL
    mov BYTE [ESI+1], 0x07

;/******************* #XF = SIMD Floating point exc *******************/
EXC_handler_19:
	xchg bx,bx
	xor bx,bx
    mov BL, "X" 			; Caracter
    mov CL, 0   			; Fila
    mov esi, dir_lineal_VIDEO
    ;mov ESI, 0xB8000   		; Puntero
    mov CH, 0   			; Columna
    mov [ESI], BL  			; Escribo en pantalla
    mov BYTE [ESI+1], 0x07  ; Atributo

    inc CH					; Incremento Columna
    add ESI,2				; Incremento puntero
    mov BL, "F"
    mov [ESI], BL
    mov BYTE [ESI+1], 0x07

;/***************TIMER****************/
IRQ_handler_0:
	pushad

	;xchg bx, bx 				; BREAKPOINT PARA VER QUE EL TIMER ESTA ANDANDO
	xor ecx, ecx 				; Limpio ecx 
	mov ecx, [TIMER_TICK] 		; Pongo en ecx el valor del timer_tick

	cmp ecx, 10 				; Si conté diez ticks, hay que habilitar a la tarea 1
	je habilitar_tarea1
	jmp incrementar_tick

	habilitar_tarea1:
		;xchg bx,bx
		xor edx, edx
		mov edx, 1
		mov [FLAG_TIMER1], edx 	; Habilito flag de tarea 1
		xor edx, edx
		mov [TIMER_TICK], edx 	; Borro el buffer donde cuento los ticks
		jmp exit

	incrementar_tick:
		inc ecx
		mov [TIMER_TICK], ecx

	exit:
		mov al, 0x20			;Limpiamos flag de interrupcion, para que pueda volver a interrumpir
		out 0x20, al
		popad
		iretd

;/*****************KEYBOARD******************/
IRQ_handler_1:
	;xchg bx, bx
	pushad
	
	xor ebx, ebx
	;mov ecx, BUFFER_TABLA
	mov ecx, BUFFER

	in al, 0x60								;Leo puerto de entrada
	mov bl, al
	;xchg bx,bx
	
	cmp al, 0x1F							;¿Se presionó la S?
	je	make_S
	;jmp salida_IRQ1						; Era solo cuando probaba el codigo de la S

	cmp al, 0x15
	je make_Y

	cmp al, 0x16
	je make_U

	cmp al, 0x17 
	je make_I

	cmp al, 0x18 
	je make_O

	cmp al, 0x19
	je make_P

	cmp al, 0x1E    ;  Es la A?
	je es_A

	cmp al, 0x30    ;  Es la B?
	je es_B
	
	cmp al, 0x2E    ;  Es la C?
	je es_C


	cmp al, 0x20    ;  Es la D?
	je es_D 


	cmp al, 0x12    ;  Es la E?
	je es_E 


	cmp al, 0x21    ;  Es la F?
	je es_F 
	jmp no_es_letra

	es_A:
		mov bl, 0x0A
		je es_valida    ;  Es la A, la escribo en buffer

	es_B:
		mov bl, 0xB
		je es_valida    ;  Es la B, la escribo en buffer

	es_C:
		mov bl, 0xC
		je es_valida    ;  Es la C, la escribo en buffer

	es_D:
		mov bl, 0xD
		je es_valida    ;  Es la D, la escribo en buffer

	es_E:
		mov bl, 0xE 
		je es_valida    ;  Es la E, la escribo en buffer

	es_F:
		mov bl, 0xF 
		je es_valida    ;  Es la F, la escribo en buffer


	no_es_letra:
	; ¿Es ENTER? MakeCode = 0x1C
	cmp al, 0x1C
	je aprete_enter

	; Si llego hasta acá quiere decir que no es ninguna de las letras entre A-F
	; Me fijo si es un número
	; El make code más bajo es el del 1 (0x02) y el más alto el del 0 (0x0B). Son todos contiguos.

	cmp al, 0x02
	jae higherorequal_1 ; Igual o más grande que el make code del 1

	higherorequal_1:
    	cmp al, 0x0B
    	jbe es_numero   ; Más chico o igual que el make code del 0 -> es número
    	jmp salida_IRQ1 ; Si no cumple, salgo de la interrupcion

	es_numero:
    	cmp al, 0x0B
    	je es_cero

    	dec bl 			;El make code de los numeros es mas grande en una unidad que el numero correspondiente
    	jmp es_valida

    	es_cero:
    	mov bl, 0x00
    	jmp es_valida


    ; Valida la LETRA
	es_valida:

		mov edx, [CONTADOR_BUFFER]

		cmp edx, 0
		je primera

		cmp edx, 0x07
		je reiniciar_contador

    	rol ebx, 28
    	shld [ecx], ebx, 4

    	;Antes lo tenia mal!
    	;rol ebx, 12
    	;shld [ecx], bx, 4

    	inc edx
    	mov [CONTADOR_BUFFER], edx
    	jmp salida_IRQ1

    primera:
    	rol ebx,28

    	shld [ecx], ebx, 4

    	inc edx
    	mov [CONTADOR_BUFFER], edx
    	jmp salida_IRQ1

    reiniciar_contador:
    	rol ebx,28
    	shld [ecx], ebx, 4
    	
    	mov edx, 0
    	mov [CONTADOR_BUFFER], edx
    	jmp salida_IRQ1

	aprete_enter:
    	;Guardar en tabla
    	xchg bx,bx

		mov ecx, [BUFFER] 					; Cargo el contenido del buffer en ecx
		xor edx, edx  						; Pongo a cero edx
		xor eax, eax 						; Pongo a cero eax

		mov edx, [INDICE_TABLA] 			; Cargo el indice de la tabla en edx. Me va a servir para mover el puntero
											; desdes la direccion 0x00410000. Este indice toma valores multiplos de 4.

		mov eax, INICIO_TABLA 				; Cargo en eax el puntero al inicio de la tabla (0x00410000)
		add eax, edx 						; Incremento el puntero para guardar una nueva entrada en la tabla
		mov [eax], ecx  					; Copio el contenido del buffer en la entrada correspondiente 

		add edx, 0x04   					; Actualizo el puntero
		mov [INDICE_TABLA], edx  
 
    	mov ecx, 0 						
    	mov [CONTADOR_BUFFER],ecx   		; Borro el contador del buffer que me sirve para manejar la cola circular
    	 									; Es como el "indice" del buffer
    	mov [BUFFER], ecx  					; Borro el buffer

    	; NOTA:
    	; Lo correcto seria implementar un control para no pasarme de la tabla. Es decir, si por 
    	; alguna razón yo llenara TODA la tabla y llegase al final, deberia volver al principio y escribir desde ahi
    	; Seria algo parecido al buffer circular.
    	; Como no es el objetivo del ejercicio (yo quiero meter valores para generar el fallo de pagina) y
    	; no va a pasar ni en pedo que se me va a llenar la tabla, lo dejo asi. 

    	jmp salida_IRQ1
	
	make_S:
		xchg bx,bx
		xor bx,bx
    	mov BL, "I" 			; Caracter
    	mov CL, 0   			; Fila
    	mov esi, dir_lineal_VIDEO
    	;mov ESI, 0xB8000   		; Puntero
    	mov CH, 0   			; Columna
    	mov [ESI], BL  			; Escribo en pantalla
    	mov BYTE [ESI+1], 0x07  ; Atributo

    	inc CH					; Incremento Columna
    	add ESI,2				; Incremento puntero
    	mov BL, "1"
    	mov [ESI], BL
    	mov BYTE [ESI+1], 0x07

    	;/***PRUEBA EXC0****/
    	xor edx, edx
    	xor ecx, ecx
    	xor eax, eax
    	mov eax, 0x10
    	div ecx

		jmp salida_IRQ1

	; Genero la #DE (Divide error) = EXC0
	make_Y:
		xor edx, edx
    	xor ecx, ecx
    	xor eax, eax
    	mov eax, 0x10
    	div ecx
    	jmp salida_IRQ1

    ; Genero la #UD (Invalid opcode) = EXC6
    make_U:
    	; Manual INTEL: The UD2 instruction is guaranteed to generate an invalid opcode exception.
    	ud2
    	jmp salida_IRQ1

    ; Genero la #DF (Double Fault) = EXC8 
    make_I:
    	; Le saco el bit de presente a la Excepcion 0

    	xor eax, eax
    	mov ax, 0x0F 						; Con ese valor borro el bit de presente.
    	mov word [Excepcion_0+5], ax 

    	; Genero la EXC0
    	xor edx, edx
    	xor ecx, ecx
    	xor eax, eax
    	mov eax, 0x10
    	div ecx
    	jmp salida_IRQ1

    ; Genero la #GP (General protection) = EXC13
    make_O:
    	; Manual INTEL: Loading the SS register with the segment selector of an executable segment or a null segment selector.
    	; Otra forma: Attempting to write a 1 into a reserved bit of CR4.
    	mov ax, 0x00
		mov ss, ax
    	jmp salida_IRQ1

    ; Genero la #PF (Page fault) = EXC14
    make_P:
    	jmp salida_IRQ1

		
	salida_IRQ1:							;Le aviso al 1er PIC que terminó el Handler 
	mov al, 0x20							;Limpiamos flag de interrupcion, para que pueda volver a interrumpir
	out 0x20, al

	popad
	iretd

IRQ_handler_2:
	pushad
	mov al, 0x20							;Limpiamos flag de interrupcion, para que pueda volver a interrumpir
	out 0x20, al
	popad
	iretd

IRQ_handler_3:
	pushad
	mov al, 0x20							;Limpiamos flag de interrupcion, para que pueda volver a interrumpir
	out 0x20, al
	popad
	iretd

IRQ_handler_4:
	pushad
	mov al, 0x20							;Limpiamos flag de interrupcion, para que pueda volver a interrumpir
	out 0x20, al
	popad
	iretd

IRQ_handler_5:
	pushad
	mov al, 0x20							;Limpiamos flag de interrupcion, para que pueda volver a interrumpir
	out 0x20, al
	popad
	iretd

IRQ_handler_6:
	pushad
	mov al, 0x20							;Limpiamos flag de interrupcion, para que pueda volver a interrumpir
	out 0x20, al
	popad
	iretd

IRQ_handler_7:
	pushad
	mov al, 0x20							;Limpiamos flag de interrupcion, para que pueda volver a interrumpir
	out 0x20, al
	popad
	iretd

IRQ_handler_8:
	pushad
	mov al, 0x20							;Limpiamos flag de interrupcion, para que pueda volver a interrumpir
	out 0x20, al
	popad
	iretd

IRQ_handler_9:
	pushad
	mov al, 0x20							;Limpiamos flag de interrupcion, para que pueda volver a interrumpir
	out 0x20, al
	popad
	iretd

IRQ_handler_10:
	pushad
	mov al, 0x20							;Limpiamos flag de interrupcion, para que pueda volver a interrumpir
	out 0x20, al
	popad
	iretd

IRQ_handler_11:
	pushad
	mov al, 0x20							;Limpiamos flag de interrupcion, para que pueda volver a interrumpir
	out 0x20, al
	popad
	iretd

IRQ_handler_12:
	pushad
	mov al, 0x20							;Limpiamos flag de interrupcion, para que pueda volver a interrumpir
	out 0x20, al
	popad
	iretd

IRQ_handler_13:
	pushad
	mov al, 0x20							;Limpiamos flag de interrupcion, para que pueda volver a interrumpir
	out 0x20, al
	popad
	iretd

IRQ_handler_14:
	pushad
	mov al, 0x20							;Limpiamos flag de interrupcion, para que pueda volver a interrumpir
	out 0x20, al
	popad
	iretd

IRQ_handler_15:
	pushad
	mov al, 0x20							;Limpiamos flag de interrupcion, para que pueda volver a interrumpir
	out 0x20, al
	popad
	iretd


;/*******************************************************************/