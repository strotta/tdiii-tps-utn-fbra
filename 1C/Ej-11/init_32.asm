EXTERN		__sys_tables_start
EXTERN		__sys_tables_end
EXTERN      __sys_tables_LMA
EXTERN		__nucleo_start
EXTERN		__nucleo_end
EXTERN		__nucleo_LMA
EXTERN		__ISR_LMA
EXTERN		__ISR_end
EXTERN		__ISR_start
EXTERN		__tabla_dig_start
EXTERN		__tabla_dig_end
EXTERN		__tabla_pag_start
EXTERN		__tabla_pag_end
EXTERN		__pila_start
EXTERN		__pila_end
EXTERN		__datos_start
EXTERN		__datos_end
EXTERN 		__tarea1_LMA
EXTERN 		__data_tarea1_LMA
EXTERN 		__tarea1_start
EXTERN		__tarea1_end
EXTERN		__bss_tarea1_start
EXTERN 		__bss_tarea1_end
EXTERN 		__data_tarea1_start
EXTERN 		__data_tarea1_end
EXTERN 		__pila_tarea1_start
EXTERN 		__pila_tarea1_end
	
EXTERN 		INICIO_TABLA
EXTERN 		FIN_TABLA
EXTERN 		BUFFER_TABLA
EXTERN 		CONTADOR_TABLA

EXTERN 		INICIO_TABLAS_PAGINACION
EXTERN		INICIO_DTP
EXTERN		INICIO_TP0
EXTERN		INICIO_TP1
EXTERN		INICIO_TP2
EXTERN		INICIO_TP3

long_pila	equ		0x1000

; dd reserva 4 bytes
; Cada entrada de la tabla son 8 bytes
; La tabla va a tener una longitud de 64K
SECTION .tabla_dig		nobits
;times	4096	db 		0
;resb 4096
resb 0x10000

; Reservo 4Mb por las dudas
SECTION .tabla_pag		nobits
resb	0x100000

; De seccion pila a pila tarea 1 hay 12K (0x3000 = 1FFFE000-1FFFB000)
; Para reservar eso debo usar resb 0x3000 pero además inicializar tres paginas en el arbol
; Por ahora solo voy a usar 4K = 0x1000
SECTION .pila 			nobits
resb	long_pila


SECTION .reset_vect

; Incrusta binario compilado del reset
incbin "reset_vect.bin"


SECTION .rom_inicio 

; binario compilado de pasaje a modo protegido, ya que estoy en 16 bits
incbin "init_16.bin"


SECTION .init_32

EXTERN CS_SEL_32
EXTERN DS_SEL_32
EXTERN GDT_tam_32
EXTERN GDT_32
EXTERN im_gdtr_32

EXTERN IDT_32
EXTERN idtr_img
EXTERN IDT

EXTERN main

EXTERN Excepcion_0
EXTERN Excepcion_1
EXTERN Excepcion_2
EXTERN Excepcion_3
EXTERN Excepcion_4
EXTERN Excepcion_5
EXTERN Excepcion_6
EXTERN Excepcion_7
EXTERN Excepcion_8
EXTERN Excepcion_9
EXTERN Excepcion_10
EXTERN Excepcion_11
EXTERN Excepcion_12
EXTERN Excepcion_13
EXTERN Excepcion_14
EXTERN Excepcion_15
EXTERN Excepcion_16
EXTERN Excepcion_17
EXTERN Excepcion_18
EXTERN Excepcion_19

EXTERN Interrupcion_0
EXTERN Interrupcion_1
EXTERN Interrupcion_2
EXTERN Interrupcion_3
EXTERN Interrupcion_4
EXTERN Interrupcion_5
EXTERN Interrupcion_6
EXTERN Interrupcion_7
EXTERN Interrupcion_8
EXTERN Interrupcion_9
EXTERN Interrupcion_10
EXTERN Interrupcion_11
EXTERN Interrupcion_12
EXTERN Interrupcion_13
EXTERN Interrupcion_14
EXTERN Interrupcion_15

EXTERN EXC_handler_0
EXTERN EXC_handler_1
EXTERN EXC_handler_2
EXTERN EXC_handler_3
EXTERN EXC_handler_4
EXTERN EXC_handler_5
EXTERN EXC_handler_6
EXTERN EXC_handler_7
EXTERN EXC_handler_8
EXTERN EXC_handler_9
EXTERN EXC_handler_10
EXTERN EXC_handler_11
EXTERN EXC_handler_12
EXTERN EXC_handler_13
EXTERN EXC_handler_14
EXTERN EXC_handler_15
EXTERN EXC_handler_16
EXTERN EXC_handler_17
EXTERN EXC_handler_18
EXTERN EXC_handler_19

EXTERN IRQ_handler_0
EXTERN IRQ_handler_1
EXTERN IRQ_handler_2
EXTERN IRQ_handler_3
EXTERN IRQ_handler_4
EXTERN IRQ_handler_5
EXTERN IRQ_handler_6
EXTERN IRQ_handler_7
EXTERN IRQ_handler_8
EXTERN IRQ_handler_9
EXTERN IRQ_handler_10
EXTERN IRQ_handler_11
EXTERN IRQ_handler_12
EXTERN IRQ_handler_13
EXTERN IRQ_handler_14
EXTERN IRQ_handler_15

EXTERN dir_fisica_systables
EXTERN dir_fisica_nucleo
EXTERN dir_fisica_ISR
EXTERN dir_fisica_pagtables
EXTERN dir_fisica_digtable
EXTERN dir_fisica_pila
EXTERN dir_fisica_datos
EXTERN dir_fisica_tarea1_text
EXTERN dir_fisica_tarea1_bss
EXTERN dir_fisica_tarea1_data
EXTERN dir_fisica_pila_tarea1

cargar_GDT_32:

    mov ax, 0x10
    mov ds, ax
    mov es, ax

	cli
    xor esi,esi
    xor edi,edi

    ;/***Copio la seccion de tablas de ROM a RAM***/
    cld 									;Segun el DF incrementa o decrementa SI y DI
	mov esi, __sys_tables_LMA				;Fuente
	;mov edi, 0x00100000						;Destino
	mov edi, dir_fisica_systables
	mov ecx, __sys_tables_end
	sub ecx, __sys_tables_start
	rep movsb 
	;/***********************************/

	;/***Copio el código principal de ROM a RAM****/
	cld 									;Segun el DF incrementa o decrementa SI y DI
	mov esi, __nucleo_LMA					;Fuente
	;mov edi, 0x00300000						;Destino
	mov edi, dir_fisica_nucleo
	mov ecx, __nucleo_end
	sub ecx, __nucleo_start
	rep movsb
	;/***********************************/

	;/***Copio el código ISR de ROM a RAM****/
	cld 									;Segun el DF incrementa o decrementa SI y DI
	mov esi, __ISR_LMA						;Fuente
	;mov edi, 0x00000000						;Destino
	mov edi, dir_fisica_ISR
	mov ecx, __ISR_end
	sub ecx, __ISR_start
	rep movsb
	;/***********************************/

	;/***Copio seccion de tabla de paginacion
	cld
	mov ax, 0x00
	;mov edi, 0x00110000
	mov edi, dir_fisica_pagtables
	mov ecx, __tabla_pag_end
	sub ecx, __tabla_pag_start
	rep stosb
	;/***********************************/

	;/***Copio seccion de tabla de digitos
	cld
	mov ax, 0x00
	;mov edi, 0x00310000
	mov edi, dir_fisica_digtable
	mov ecx, __tabla_dig_end
	sub ecx, __tabla_dig_start
	rep stosb
	;/***********************************/

	;/***Copio seccion datos
	cld
	mov ax, 0x00
	;mov edi, 0x00310000
	mov edi, dir_fisica_datos
	mov ecx, __datos_end
	sub ecx, __datos_start
	rep stosb
	;/***********************************/


	;/***Copio seccion de pila
	cld
	mov ax, 0x00
	;mov edi, 0x00310000
	mov edi, dir_fisica_pila
	mov ecx, __pila_end
	sub ecx, __pila_start
	rep stosb
	;/***********************************/


	;/***Copio el código de la TAREA1 de ROM a RAM****/
	cld 									;Segun el DF incrementa o decrementa SI y DI
	mov esi, __tarea1_LMA						;Fuente
	;mov edi, 0x00000000					;Destino
	mov edi, dir_fisica_tarea1_text
	mov ecx, __tarea1_end
	sub ecx, __tarea1_start
	rep movsb
	;/***********************************/

	;/***Copio seccion bss de la tarea 1
	cld
	mov ax, 0x00
	;mov edi, 0x00310000
	mov edi, dir_fisica_tarea1_bss
	mov ecx, __bss_tarea1_end
	sub ecx, __bss_tarea1_start
	rep stosb
	;/***********************************/

	;/***Copio seccion .data de la TAREA1 de ROM a RAM****/
	cld 									;Segun el DF incrementa o decrementa SI y DI
	mov esi, __data_tarea1_LMA						;Fuente
	;mov edi, 0x00000000					;Destino
	mov edi, dir_fisica_tarea1_data
	mov ecx, __data_tarea1_end
	sub ecx, __data_tarea1_start
	rep movsb
	;/***********************************/

	;/***Copio seccion de pila de tarea 1
	cld
	mov ax, 0x00
	;mov edi, 0x00310000
	mov edi, dir_fisica_pila_tarea1
	mov ecx, __pila_tarea1_end
	sub ecx, __pila_tarea1_start
	rep stosb
	;/***********************************/


	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; PAGINACION ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	mov edi, INICIO_TABLAS_PAGINACION
	mov ecx, 5*0x400 						; Cantidad de entradas del DTP + Entradas de las TP
	xor eax, eax							; Poner a cero esas entradas
	rep stosd								; Copio lo que tengo en eax a ES:EDI

	; Rutina inicializacion de DTP
	; Atributos: 0x03 => R/W y presente

	; Entrada de la DTP correspondiente a la traduccion de la ROM
	mov DWORD [INICIO_TABLAS_PAGINACION + 0x3FF*4], INICIO_TP0 + 0x03

	; Primeros 4Mb:
	; ISR, VIDEO, Tablas de sistema, Tablas de paginación
	mov DWORD [INICIO_TABLAS_PAGINACION + 0x0*4], INICIO_TP1 + 0x03

	; Segundos 4Mb
	mov DWORD [INICIO_TABLAS_PAGINACION + 0x1*4], INICIO_TP2 + 0x03

	; Tabla de pagina para la pila
	mov DWORD [INICIO_TABLAS_PAGINACION + 0x7F*4], INICIO_TP3 + 0x03

	; Rutina de inicializacion de las TP

	; La TP0 voy a usarla para paginar en identity mapping la ROM
	; Como la ROM es de 64KB y cada pagina es de 4KB, necesito inicializar 16 entradas de la TP0

	mov edi, INICIO_TP0 + 0x3F0*4
	mov eax, 0xFFFF0000 + 0x03 		; 0x03: Atributos => presente y R/W
	mov ecx, 16						; Cantidad de entradas a inicializar = 16

	ciclo:
		mov [edi], eax		
		add eax, 0x1000
		add edi, 4
		dec ecx
		jnz ciclo

	;;;;; TP1 ;;;;;
	mov DWORD [INICIO_TP1 + 0x0*4], 0x00000000 + 0x03 		; Entrada 0 <= Dir. Física ISR.
	mov DWORD [INICIO_TP1 + 0x010*4], 0x000B8000 + 0x03 	; Entrada 0x010 <= Dir. Fisica VIDEO
	mov DWORD [INICIO_TP1 + 0x100*4], 0x00100000 + 0x03 	; Entrada 0x100 <= Dir. Fisica Tablas del sistema
	mov DWORD [INICIO_TP1 + 0x110*4], 0x00110000 + 0x03 	; Entrada 0x110 <= Dir. Fisica Tablas Paginación

	;;;;; TP2 ;;;;;
	mov DWORD [INICIO_TP2 + 0x0*4], 0x00400000 + 0x03       ; Entrada 0 <= Dir. Física Nucleo
	mov DWORD [INICIO_TP2 + 0x010*4], 0x00410000 + 0x03 	; Entrada 0x01 <= Dir. Física Tabla digitos
															; Con esto solo pagino 4K 

	;;; ACA ESTOY PAGINANDO LOS 64KB de la tabla de digitos ;;;
	;;; Si se descomenta este bloque hay que comentar la linea de arriba;;;
	;;; Deshabilite la paginacion de 64KB quedandome solo con 4KB porque sino se superpone con la seccion de la pila tarea 1;;;;
	;mov edi, INICIO_TP2 + 0x010*4
	;mov eax, 0x00410000 + 0x03
	;mov ecx, 16

	;ciclo2:
	;	mov [edi], eax		
	;	add eax, 0x1000
	;	add edi, 4
	;	dec ecx
	;	jnz ciclo2
	;;;;;;;;;;;;;


	; REVISAR. PASA ALGO RARO AL DESCOMENTAR las PRIMERAS TRES LINEAS Y LA ULTIMA
	mov DWORD [INICIO_TP2 + 0x110*4], 0x00421000 + 0x03  	; Entrada 0x110 <= Dir. Fisica Text Tarea 1
	mov DWORD [INICIO_TP2 + 0x111*4], 0x00422000 + 0x03 	; Entrada 0x111 <= Dir. Fisica Bss Tarea 1
	mov DWORD [INICIO_TP2 + 0x112*4], 0x00423000 + 0x03 	; Entrada 0x112 <= Dir. Fisica Data Atrea 1
	mov DWORD [INICIO_TP2 + 0x0E0*4], 0x004E0000 + 0x03   	; Entrada 0x0E0 <= Dir. Fisica Datos
	mov DWORD [INICIO_TP2 + 0x013*4], 0x1FFFE000 + 0x03 	; Entrada 0x013 <= Dir. Fisica Pila Tarea 1


	; OJO! PROBAR PAGINAR SOLO 4K la tabla de digitos y descomentar las lineas de arriba

	;;;; TP3 ;;;;;
	mov DWORD [INICIO_TP3 + 0x3FB*4], 0x1FFFB000 + 0x03       ; Entrada 0 <= Dir. Física Nucleo

	xchg bx,bx
	; Habilito paginación
	mov eax, INICIO_TABLAS_PAGINACION
	mov cr3, eax							; Apunto al directorio de páginas

	; ¿Páginas de qué tamaño?

	mov eax, cr0							; Activo paginación tocando bit 31 del cr0
	or eax, 0x80000000
	mov cr0, eax
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	xchg bx,bx
    lgdt [im_gdtr_32]
    lidt [idtr_img]

    mov ax, DS_SEL_32
    mov ds, ax
    mov es, ax

    ;/*Inicializo la PILA*/
    mov ss, ax
    ;mov esp, 0x1FFFB000
    mov esp, dir_fisica_pila
    add esp, long_pila      				; long de la pila
    ;/********************/

    ;/*************CARGA DE HANDLERS***************/
    mov eax, EXC_handler_0
	mov word[Excepcion_0], ax				;Cargo la dirección del IRQ_handler_1 en el Offset 15-0 de su entrada en la IDT
	shr eax, 16								;Shifteo 16 veces a la derecha para quedarme con la parte alta
	mov word[Excepcion_0+6], ax				;Cargo la parte alta en el offset 31-16 de la entrada de la IDT

	mov eax, EXC_handler_1
	mov word[Excepcion_1], ax				;Cargo la dirección del IRQ_handler_1 en el Offset 15-0 de su entrada en la IDT
	shr eax, 16								;Shifteo 16 veces a la derecha para quedarme con la parte alta
	mov word[Excepcion_1+6], ax				;Cargo la parte alta en el offset 31-16 de la entrada de la IDT

	mov eax, EXC_handler_2
	mov word[Excepcion_2], ax				;Cargo la dirección del IRQ_handler_1 en el Offset 15-0 de su entrada en la IDT
	shr eax, 16								;Shifteo 16 veces a la derecha para quedarme con la parte alta
	mov word[Excepcion_2+6], ax				;Cargo la parte alta en el offset 31-16 de la entrada de la IDT

	mov eax, EXC_handler_3
	mov word[Excepcion_3], ax				;Cargo la dirección del IRQ_handler_1 en el Offset 15-0 de su entrada en la IDT
	shr eax, 16								;Shifteo 16 veces a la derecha para quedarme con la parte alta
	mov word[Excepcion_3+6], ax				;Cargo la parte alta en el offset 31-16 de la entrada de la IDT

	mov eax, EXC_handler_4
	mov word[Excepcion_4], ax				;Cargo la dirección del IRQ_handler_1 en el Offset 15-0 de su entrada en la IDT
	shr eax, 16								;Shifteo 16 veces a la derecha para quedarme con la parte alta
	mov word[Excepcion_4+6], ax				;Cargo la parte alta en el offset 31-16 de la entrada de la IDT

	mov eax, EXC_handler_5
	mov word[Excepcion_5], ax				;Cargo la dirección del IRQ_handler_1 en el Offset 15-0 de su entrada en la IDT
	shr eax, 16								;Shifteo 16 veces a la derecha para quedarme con la parte alta
	mov word[Excepcion_5+6], ax				;Cargo la parte alta en el offset 31-16 de la entrada de la IDT

	mov eax, EXC_handler_6
	mov word[Excepcion_6], ax				;Cargo la dirección del IRQ_handler_1 en el Offset 15-0 de su entrada en la IDT
	shr eax, 16								;Shifteo 16 veces a la derecha para quedarme con la parte alta
	mov word[Excepcion_6+6], ax				;Cargo la parte alta en el offset 31-16 de la entrada de la IDT

	mov eax, EXC_handler_7
	mov word[Excepcion_7], ax				;Cargo la dirección del IRQ_handler_1 en el Offset 15-0 de su entrada en la IDT
	shr eax, 16								;Shifteo 16 veces a la derecha para quedarme con la parte alta
	mov word[Excepcion_7+6], ax				;Cargo la parte alta en el offset 31-16 de la entrada de la IDT

	mov eax, EXC_handler_8
	mov word[Excepcion_8], ax				;Cargo la dirección del IRQ_handler_1 en el Offset 15-0 de su entrada en la IDT
	shr eax, 16								;Shifteo 16 veces a la derecha para quedarme con la parte alta
	mov word[Excepcion_8+6], ax				;Cargo la parte alta en el offset 31-16 de la entrada de la IDT

	mov eax, EXC_handler_9
	mov word[Excepcion_9], ax				;Cargo la dirección del IRQ_handler_1 en el Offset 15-0 de su entrada en la IDT
	shr eax, 16								;Shifteo 16 veces a la derecha para quedarme con la parte alta
	mov word[Excepcion_9+6], ax				;Cargo la parte alta en el offset 31-16 de la entrada de la IDT

	mov eax, EXC_handler_10
	mov word[Excepcion_10], ax				;Cargo la dirección del IRQ_handler_1 en el Offset 15-0 de su entrada en la IDT
	shr eax, 16								;Shifteo 16 veces a la derecha para quedarme con la parte alta
	mov word[Excepcion_10+6], ax				;Cargo la parte alta en el offset 31-16 de la entrada de la IDT

	mov eax, EXC_handler_11
	mov word[Excepcion_11], ax				;Cargo la dirección del IRQ_handler_1 en el Offset 15-0 de su entrada en la IDT
	shr eax, 16								;Shifteo 16 veces a la derecha para quedarme con la parte alta
	mov word[Excepcion_11+6], ax				;Cargo la parte alta en el offset 31-16 de la entrada de la IDT

	mov eax, EXC_handler_12
	mov word[Excepcion_12], ax				;Cargo la dirección del IRQ_handler_1 en el Offset 15-0 de su entrada en la IDT
	shr eax, 16								;Shifteo 16 veces a la derecha para quedarme con la parte alta
	mov word[Excepcion_12+6], ax				;Cargo la parte alta en el offset 31-16 de la entrada de la IDT

	mov eax, EXC_handler_13
	mov word[Excepcion_13], ax				;Cargo la dirección del IRQ_handler_1 en el Offset 15-0 de su entrada en la IDT
	shr eax, 16								;Shifteo 16 veces a la derecha para quedarme con la parte alta
	mov word[Excepcion_13+6], ax				;Cargo la parte alta en el offset 31-16 de la entrada de la IDT

	mov eax, EXC_handler_14
	mov word[Excepcion_14], ax				;Cargo la dirección del IRQ_handler_1 en el Offset 15-0 de su entrada en la IDT
	shr eax, 16								;Shifteo 16 veces a la derecha para quedarme con la parte alta
	mov word[Excepcion_14+6], ax				;Cargo la parte alta en el offset 31-16 de la entrada de la IDT

	mov eax, EXC_handler_15
	mov word[Excepcion_15], ax				;Cargo la dirección del IRQ_handler_1 en el Offset 15-0 de su entrada en la IDT
	shr eax, 16								;Shifteo 16 veces a la derecha para quedarme con la parte alta
	mov word[Excepcion_15+6], ax				;Cargo la parte alta en el offset 31-16 de la entrada de la IDT

	mov eax, EXC_handler_16
	mov word[Excepcion_16], ax				;Cargo la dirección del IRQ_handler_1 en el Offset 15-0 de su entrada en la IDT
	shr eax, 16								;Shifteo 16 veces a la derecha para quedarme con la parte alta
	mov word[Excepcion_16+6], ax				;Cargo la parte alta en el offset 31-16 de la entrada de la IDT

	mov eax, EXC_handler_17
	mov word[Excepcion_17], ax				;Cargo la dirección del IRQ_handler_1 en el Offset 15-0 de su entrada en la IDT
	shr eax, 16								;Shifteo 16 veces a la derecha para quedarme con la parte alta
	mov word[Excepcion_17+6], ax				;Cargo la parte alta en el offset 31-16 de la entrada de la IDT

	mov eax, EXC_handler_18
	mov word[Excepcion_18], ax				;Cargo la dirección del IRQ_handler_1 en el Offset 15-0 de su entrada en la IDT
	shr eax, 16								;Shifteo 16 veces a la derecha para quedarme con la parte alta
	mov word[Excepcion_18+6], ax				;Cargo la parte alta en el offset 31-16 de la entrada de la IDT

	mov eax, EXC_handler_19
	mov word[Excepcion_19], ax				;Cargo la dirección del IRQ_handler_1 en el Offset 15-0 de su entrada en la IDT
	shr eax, 16								;Shifteo 16 veces a la derecha para quedarme con la parte alta
	mov word[Excepcion_19+6], ax				;Cargo la parte alta en el offset 31-16 de la entrada de la IDT



	mov eax, IRQ_handler_0
	mov word[Interrupcion_0], ax			;Cargo la dirección del IRQ_handler_1 en el Offset 15-0 de su entrada en la IDT
	shr eax, 16								;Shifteo 16 veces a la derecha para quedarme con la parte alta
	mov word[Interrupcion_0+6], ax			;Cargo la parte alta en el offset 31-16 de la entrada de la IDT

	mov eax, IRQ_handler_1
	mov word[Interrupcion_1], ax			;Cargo la dirección del IRQ_handler_1 en el Offset 15-0 de su entrada en la IDT
	shr eax, 16								;Shifteo 16 veces a la derecha para quedarme con la parte alta
	mov word[Interrupcion_1+6], ax			;Cargo la parte alta en el offset 31-16 de la entrada de la IDT

	mov eax, IRQ_handler_2
	mov word[Interrupcion_2], ax			;Cargo la dirección del IRQ_handler_1 en el Offset 15-0 de su entrada en la IDT
	shr eax, 16								;Shifteo 16 veces a la derecha para quedarme con la parte alta
	mov word[Interrupcion_2+6], ax			;Cargo la parte alta en el offset 31-16 de la entrada de la IDT

	mov eax, IRQ_handler_3
	mov word[Interrupcion_3], ax			;Cargo la dirección del IRQ_handler_1 en el Offset 15-0 de su entrada en la IDT
	shr eax, 16								;Shifteo 16 veces a la derecha para quedarme con la parte alta
	mov word[Interrupcion_3+6], ax			;Cargo la parte alta en el offset 31-16 de la entrada de la IDT

	mov eax, IRQ_handler_4
	mov word[Interrupcion_4], ax			;Cargo la dirección del IRQ_handler_1 en el Offset 15-0 de su entrada en la IDT
	shr eax, 16								;Shifteo 16 veces a la derecha para quedarme con la parte alta
	mov word[Interrupcion_4+6], ax			;Cargo la parte alta en el offset 31-16 de la entrada de la IDT

	mov eax, IRQ_handler_5
	mov word[Interrupcion_5], ax			;Cargo la dirección del IRQ_handler_1 en el Offset 15-0 de su entrada en la IDT
	shr eax, 16								;Shifteo 16 veces a la derecha para quedarme con la parte alta
	mov word[Interrupcion_5+6], ax			;Cargo la parte alta en el offset 31-16 de la entrada de la IDT

	mov eax, IRQ_handler_6
	mov word[Interrupcion_6], ax			;Cargo la dirección del IRQ_handler_1 en el Offset 15-0 de su entrada en la IDT
	shr eax, 16								;Shifteo 16 veces a la derecha para quedarme con la parte alta
	mov word[Interrupcion_6+6], ax			;Cargo la parte alta en el offset 31-16 de la entrada de la IDT

	mov eax, IRQ_handler_7
	mov word[Interrupcion_7], ax			;Cargo la dirección del IRQ_handler_1 en el Offset 15-0 de su entrada en la IDT
	shr eax, 16								;Shifteo 16 veces a la derecha para quedarme con la parte alta
	mov word[Interrupcion_7+6], ax			;Cargo la parte alta en el offset 31-16 de la entrada de la IDT

	mov eax, IRQ_handler_8
	mov word[Interrupcion_8], ax			;Cargo la dirección del IRQ_handler_1 en el Offset 15-0 de su entrada en la IDT
	shr eax, 16								;Shifteo 16 veces a la derecha para quedarme con la parte alta
	mov word[Interrupcion_8+6], ax			;Cargo la parte alta en el offset 31-16 de la entrada de la IDT

	mov eax, IRQ_handler_9
	mov word[Interrupcion_9], ax			;Cargo la dirección del IRQ_handler_1 en el Offset 15-0 de su entrada en la IDT
	shr eax, 16								;Shifteo 16 veces a la derecha para quedarme con la parte alta
	mov word[Interrupcion_9+6], ax			;Cargo la parte alta en el offset 31-16 de la entrada de la IDT

	mov eax, IRQ_handler_10
	mov word[Interrupcion_10], ax			;Cargo la dirección del IRQ_handler_1 en el Offset 15-0 de su entrada en la IDT
	shr eax, 16								;Shifteo 16 veces a la derecha para quedarme con la parte alta
	mov word[Interrupcion_10+6], ax			;Cargo la parte alta en el offset 31-16 de la entrada de la IDT

	mov eax, IRQ_handler_11
	mov word[Interrupcion_11], ax			;Cargo la dirección del IRQ_handler_1 en el Offset 15-0 de su entrada en la IDT
	shr eax, 16								;Shifteo 16 veces a la derecha para quedarme con la parte alta
	mov word[Interrupcion_11+6], ax			;Cargo la parte alta en el offset 31-16 de la entrada de la IDT

	mov eax, IRQ_handler_12
	mov word[Interrupcion_12], ax			;Cargo la dirección del IRQ_handler_1 en el Offset 15-0 de su entrada en la IDT
	shr eax, 16								;Shifteo 16 veces a la derecha para quedarme con la parte alta
	mov word[Interrupcion_12+6], ax			;Cargo la parte alta en el offset 31-16 de la entrada de la IDT

	mov eax, IRQ_handler_13
	mov word[Interrupcion_13], ax			;Cargo la dirección del IRQ_handler_1 en el Offset 15-0 de su entrada en la IDT
	shr eax, 16								;Shifteo 16 veces a la derecha para quedarme con la parte alta
	mov word[Interrupcion_13+6], ax			;Cargo la parte alta en el offset 31-16 de la entrada de la IDT

	mov eax, IRQ_handler_14
	mov word[Interrupcion_14], ax			;Cargo la dirección del IRQ_handler_1 en el Offset 15-0 de su entrada en la IDT
	shr eax, 16								;Shifteo 16 veces a la derecha para quedarme con la parte alta
	mov word[Interrupcion_14+6], ax			;Cargo la parte alta en el offset 31-16 de la entrada de la IDT

	mov eax, IRQ_handler_15
	mov word[Interrupcion_15], ax			;Cargo la dirección del IRQ_handler_1 en el Offset 15-0 de su entrada en la IDT
	shr eax, 16								;Shifteo 16 veces a la derecha para quedarme con la parte alta
	mov word[Interrupcion_15+6], ax			;Cargo la parte alta en el offset 31-16 de la entrada de la IDT


	;/*+*******************************************/


	;Para Interrupciones y Excepciones

	push ebp
	mov ebp, esp

	mov bx, 0x2028							; ¿Qué hace ésto?
	call InitPIC							;Inicializamos los PICs con todas las interrrupciones deshabilitadas

	mov esp, ebp
	pop ebp

	xor eax, eax
		
	;mov al, 0xFD							;Habilito solo IRQ1 => Teclado
	mov al, 0xFC							;Habilito IRQ1	=> Teclado	IRQ0	=> Timer
	out 0x21, al 							;11111100
	sti

    jmp CS_SEL_32:main


;--------------------------------------------------------------------------------
; Inicializacion del controlador de interrupciones
; Corre la base de los tipos de interrupción de ambos PICs 8259A de la PC a los 8 tipos consecutivos a 
; partir de los valores base que recibe en BH para el PIC Nº1 y BL para el PIC Nº2.
; A su retorno las Interrupciones de ambos PICs están deshabilitadas.
;--------------------------------------------------------------------------------
InitPIC:
										; Inicialización PIC Nº1
										; ICW1
	mov		al, 11h         			; IRQs activas x flanco, cascada, y ICW4
	out     20h, al  
										; ICW2
	mov     al, bh          			; El PIC Nº1 arranca en INT tipo (BH)
	out     21h, al
										; ICW3
	mov     al, 04h         			; PIC1 Master, Slave ingresa Int.x IRQ2
	out     21h, al
										; ICW4
	mov     al, 01h         			; Modo 8086
	out     21h, al
										; Antes de inicializar el PIC Nº2, deshabilitamos 
										; las Interrupciones del PIC1
	mov     al, 0FFh
	out     21h, al
										; Ahora inicializamos el PIC Nº2
										; ICW1
	mov     al, 11h        			  	; IRQs activas x flanco,cascada, y ICW4
	out     0A0h, al  
										; ICW2
	mov    	al, bl          			; El PIC Nº2 arranca en INT tipo (BL)
	out     0A1h, al
										; ICW3
	mov     al, 02h         			; PIC2 Slave, ingresa Int x IRQ2
	out     0A1h, al
										; ICW4
	mov     al, 01h         			; Modo 8086
	out     0A1h, al
										; Enmascaramos el resto de las Interrupciones 
										; (las del PIC Nº2)
	mov     al, 0FFh
	out     0A1h, al

	;Inicialización del timer del sistema
    mov ebx,100
    call Init_PIT
    
	ret

;/*********************Inicialización del TIMER = 100ms ***********************/
;-------------------------------------------------------------------------------
;7 6 5 4 3 2 1 0   <-- Número de bit de la palabra de control
;| | | | | | | |
;| | | | | | | +-- Modo BCD:
;| | | | | | |     0 - El contador trabajará en formato binario de 16 bits
;| | | | | | |     1 - El contador trabajará en formato BCD con cuatro dígitos 
;| | | | | | |          decimales
;| | | | +-+-+---- Modo de operación para el contador:
;| | | |           000 - Modo 0. Interrupt on Terminal Count (Interrumpe al terminar el conteo)
;| | | |           001 - Modo 1. Hardware Retriggerable One-Shot (Disparo programable)
;| | | |           X10 - Modo 2. Rate Generator (Generador de impulsos). El valor del bit más significativo no importa
;| | | |           X11 - Modo 3. Square Wave(Generador de onda cuadrada). El valor del bit más significativo no importa
;| | | |           100 - Modo 4. Software Triggered Strobe (Strobe disparado por software)
;| | | |           101 - Modo 5. Hardware Triggered Strobe (Retriggerable) (Strobe disparado por hardware)
;| | | |
;| | +-+---------- Modo de acceso (lectura/escritura) para el valor del contador:
;| |               00 - Counter Latch. El valor puede ser leído de la manera en que fue ajustado previamente.
;| |                                   El valor es mantenido hasta que es leído o sobreescrito.
;| |               01 - Lee (o escribe) solo el byte menos significativo del contador (bits 0-7)
;| |               10 - Lee (o escribe) solo el byte más significativo del contador (bits 8-15)
;| |               11 - Primero se lee (o escribe) el byte menos significativo del contador, y luego el byte más significativo
;| |
;+-+-------------- Selección del contador:
;                  00 - Se selecciona el contador 0
;                  01 - Se selecciona el contador 1
;                  10 - Se selecciona el contador 2
;                  11 - No usado. (solo hay 3 contadores)
;                  (Los demás bits de la palabra de control indican cómo será programado el contador seleccionado)

Init_PIT:
    pushad
    pushfd
    
    cli
    
    mov al, 00110110b
    out 0x43, al 			; 43h = Registro de control
    mov ax, 11931			; Los 3 contadores del PIT reciben una señal de clock de 1.19318 MHz 
                            ; 11931 * (1/1.19318 MHz) = 10 interrupciones por segundo
                            ; El intervalo entre interrupciones es de 1 ms.
    out 0x40, al
    mov al, ah
    out 0x40, al 			; 40h = contador 0
 
    popfd
 
    popad
    ret	

;/*********************************************************************************************************************/



