;BITS 16

%include "init_pci.inc"
jmp init_a20

ALIGN 8

GDT:

	dq 0 ;selector nulo

    ; Quiero el selector de código en: 0xFFFF0000
    CS_SEL equ $-GDT
		
		dw 0xffff   ; Limite 15-0
        dw 0		; Base 15-0
        db 0xff     ; Base 23-16
        db 0x9a     ; Derechos de Acceso: 0b10011010. De izquierda a derecha:
         			; Bit 7: Segmento presente. Bit 6-5: DPL=00 (Maximo privilegio: Kernel)
         			; Bi4 4: '1' Para código o datos. Bit 3: '1' para código
         			; Bit 2: '0' (no) Segmento conforme. Bit 1: '1' permiso de lectura.
         			; Bit 0: Accedido
        db 0xcf	    ; 0b11001111. Bit 7: Granularidad=1. Bit 6: Default=1 (Segmento 32 bits)
         			; Bit 5-4 = 00 por definicion.
         			; Limite 19-16 = f
        db 0xff     ; Base 31-24 = 0xff 

        ; Me queda: BASE = 0xFFFF0000 y LIMITE = 0xFFFFF y se completa con tres F más por tener G=1
        ; Si fuera G=0 el limite es el que figura en el descriptor

    ; Quiero el selector de datos en: 0x00000000

    DS_SEL equ $-GDT
      
        dw 0xffff   ; Limite 15-0
        dw 0		; Base 15-0
        db 0		; Base 23-16 
        db 0x92	    ; Derechos de Acceso: 0b10010010. De izquierda a derecha:
         			; Bit 7: Segmento presente. Bit 6-5: DPL=00 (Maximo privilegio: Kernel)
         			; Bi4 4: '1' Para código o datos. Bit 3: '0' para datos
         			; Bit 2: '0' (no) Segmento conforme. Bit 1: '1' permiso de lectura.
         			; Bit 0: Accedido
        db 0xcf     ; 0b11001111. Bit 7: Granularidad=1. Bit 6: Default=1 (Segmento 32 bits)
         			; Bit 5-4 = 00 por definicion.
         			; Limite 19-16 = f
        db 0		; Base 31-24 = 0x00 

        ; Me queda BASE = 0x00000000 y LIMITE = 0xFFFFF y tres F más por tener G=1 (0xFFFFFFFF)
      
    GDT_tam equ $-GDT

im_gdtr:

 	;0   15 16         47
    ;|LIMIT|----BASE----|
 
    dw  GDT_tam -1		; Limite (Tamaño de la GDT)
      					; LIMIT is 1 less than the length of the table, so if LIMIT has the value 15, then the GDT is 16 bytes long.

    dd  0xf0000 + GDT   ; Dir. Lineal Base (Donde empieza la GDT)


;/****************************************************************************************/
;/*****************HABILITACION GATE A20 - CODIGO DEL CAMPUS VIRTUAL *********************/
;/****************************************************************************************/

HabilitarA20:
	mov		ax, 0xFFFF					; AX=0xFFFF
	mov		es, ax
	cmp		word [es:0x7E0E], 0xAA55 	; vemos si 0x107E0E coincide con 0x7E0E
	je		GateA20_disabled			; Coincide => A20 deshabilitada
	rol		word [0x7DFE], 1			; Modifico 0x7E0E
	cmp		word [es:0x7E0E], 0x55AA	; Cambió 0x107E0E?
	jne		GateA20_enabled				; Si no cambió entonces A20 está enabled
GateA20_disabled:
	mov		al, 0xDF					; Comando de habilitación de A20
	call	Gate_A20					; habilitar Gate A20
	cmp		al, 0						; OK?
	je		GateA20_enabled				; OK => seguimos...
Fail:
	hlt									; colgamos el micro sin pasar por MP
	jmp		Fail
GateA20_enabled:
	mov		word [0x7DFE], 0xAA55		; restituimos la firma del bootloader
	ret

%define 	port_a  	060h			; Direccion de E/S del Port A del 8042
%define 	ctrl_port	064h			; port de Estados del 8042

Gate_A20:
	cli									; Mientras usa el 8042, no INTR

	call   	_8042_empty?				; Ve si buffer del 8042 está vacío.
	jnz    	gate_a20_exit				; No lo está =>retorna con AL=2.

	mov    	al, 0xD1					; Comando Write port del 8042..
	out    	ctrl_port,al				; ...se env¡a al port 64h.

	call   	_8042_empty?				; Espera se acepte el comando.
	jnz    	gate_a20_exit				; Si no se acepta, Ret con AL=2

	mov    	al,ah						; Pone en AL el dato a escribir.
	out  	port_a, al					; Lo env¡a al 8042.
	call	_8042_empty?				; Espera se acepte el comando.
gate_a20_exit:
	ret

_8042_empty?:
	push   	cx               			; salva CX.
	sub    	cx, cx          			; CX = 0 : valor de time out.
empty_8042_01: 
	in      al, ctrl_port  		  		; Lee port de estado del 8042.
	and    	al, 00000010b    			; si el bit 1 est  seteado o...
	loopnz 	empty_8042_01   			; no alcanzó time out, espera.
	pop   	cx          		     	; recupera cx
	ret                 	         	; retorna con AL=0, si se limpió bit 1, o AL=2 si no.


;/****************************************************************************************/
;/******************************** LLAMADO INIT A20 **************************************/
;/****************************************************************************************/

init_a20:
	push ebp
	mov ebp,esp
	call HabilitarA20
	mov esp, ebp
	pop ebp
; ESTO ESTA BIEN?


;/****************************************************************************************/
;/*************************** PASAJE A MODO PROTEGIDO ************************************/
;/****************************************************************************************/

pasaje_aMP:
	cli 				; Apago interrupciones
    xchg bx,bx
    lgdt[cs:im_gdtr]
    mov eax, cr0
    or  eax, 1
    mov cr0, eax
    jmp CS_SEL:inicio_32

    BITS 32
    inicio_32:
