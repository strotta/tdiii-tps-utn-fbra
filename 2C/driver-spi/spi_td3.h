#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/platform_device.h>
#include <linux/errno.h>
#include <linux/version.h>
#include <linux/types.h>
#include <linux/kdev_t.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/slab.h>
#include <linux/io.h>
#include <asm/uaccess.h>
#include <asm/io.h>
#include <linux/ioport.h>
#include <linux/delay.h>
#include <linux/interrupt.h>
#include <linux/gpio.h>
#include <linux/wait.h>
#include <linux/sched.h>
#include <linux/semaphore.h>
#include <linux/spinlock.h>
#include <linux/list.h>
#include <linux/device.h>
#include <linux/of.h>
#include <linux/of_irq.h>
#include <linux/of_platform.h>
#include <linux/of_address.h>
#include <linux/uaccess.h>

#define 	FIRST_MINOR		0
#define 	NBR_DEV 		1
#define		DEBUG
#define		Rx_MEMORY_SYZE		64
#define		Tx_MEMORY_SYZE		64
#define		MAX_BYTE_READ		32
#define		MAX_BYTE_WRITE		32

/***** HW DEFS ******/

#define		GPIO_CS			115
/************************************************************************************************************************************************/
// El manual usado fue el: 
// AM335x and AMIC110 SitaraTM Processors, Technical Reference Manual (Rev.P), October 2011 – Revised March 2017
// 5113 páginas
/************************************************************************************************************************************************/


/************************************************************************************************************************************************/
// Direcciones del SPI0
#define	  	SPI0_IRQ			65						// Numero de interrupcion para spi0 (McSPI0). Pag. 544
#define		SPI0_ADDRESS		0x48030000 				// McSPIO0 Registers. Pag. 181 
#define 	SPI0_LENGTH  		0x1000
/************************************************************************************************************************************************/


/************************************************************************************************************************************************/
// SPI Registers. Ver página 4920 del manual
// Offsets para los registros del spi
#define		MCSPI_REVISION		0x0
#define		MCSPI_SYSCONFIG 	0x110
#define		MCSPI_SYSSTATUS		0x114 
#define		MCSPI_IRQSTATUS		0x118
#define		MCSPI_IRQENABLE		0x11C
#define 	MCSPI_SYST 			0x124 
#define		MCSPI_MODULECTRL	0x128
#define 	MCSPI_CH0CONF 		0x12C
#define 	MCSPI_CH0STAT		0x130
#define 	MCSPI_CH0CTRL 		0x134
#define 	MCSPI_TX0 			0x138
#define 	MCSPI_RX0 			0x13C
#define 	MCSPI_XFERLEVEL 	0x17C

/************************************************************************************************************************************************/


/************************************************************************************************************************************************/
// Todos estos registros son necesarios para habilitar el clck del spi0
// Clock module register (CM_PER). Ver página 179.
#define		CMPER_ADDR			0x44E00000					// Direccion base de la zona de memoria de los registros de los clcks		
#define   	CMPER_LEN			0x400 					 	// Tamaño de la zona de memoria de los registros de los clcks (1kb)

#define 	CM_PER_SPI0_CLKCTRL		0x4C
#define 	CM_PER_SPI0_ENABLED		0x02 					// Module is explicity enabled. Pag. 1269
#define 	CM_PER_L4LS				0x00
#define		CM_PER_L4LS_ENABLED		0x02 					// Pagina 1253. SW_WKUP.

// Debo tocar el registro L4LS porque es el que esta relacionado con el SPI_GCLK

/************************************************************************************************************************************************/


/************************************************************************************************************************************************/
// Registros para la configuracion del pin mux, que permite salida para los pines
// Control Module register (Ver página 180)
#define   CONT_MOD_ADDR			0x44E10000					// Direccion base de los registros CONT_MOD
#define	  CONT_MOD_LEN			0x2000 						// Tamaño de la zona de registros CONT_MOD (128kb)

// Para ver el tema del modo de los pines, ver página 44 del manual del Sitara (235 hojas) 


// Offsets. Arranca en página 1455 (Sección 9.3.1 CONTROL_MODULE Registers)
// Leer pagina 1458 y 4884 para diagrama en bloques
#define 	conf_spi0_sclk		0x950
#define 	conf_spi0_d0 		0x954 						// Es mi MISO (Entrada porque soy maestro)
#define 	conf_spi0_d1 		0x958 						// Es mi MOSI (Salida porque soy maestro)
#define 	conf_spi0_cs0 		0x95C 						// Chip select. Salida porque soy master 
#define 	conf_spi0_cs1 		0x960						// Chip select. Salida porque soy master

// Viendo datasheet del procesador, necesito el Modo 0 para D0 y D1
// Pagina 1512
#define		PIN_CONFIG_MODE_0	0x0

/************************************************************************************************************************************************/


/************************************************************************************************************************************************/
// Prototipos de función
/************************************************************************************************************************************************/
static irq_handler_t SPI0_IRQHandler (unsigned int irq, void *dev_id, struct pt_regs *regs);
static int spi_open(struct inode *inode, struct file *filp);
static int spi_close(struct inode *inode, struct file *filp);
static ssize_t spi_read(struct file *file, char *buf, size_t count, loff_t *nose);
static ssize_t spi_write (struct file *file, const char __user *user_buffer,size_t length, loff_t *offset);
static int midriver_probe(struct platform_device *);
static int midriver_remove(struct platform_device *);
/************************************************************************************************************************************************/


// Inicialización del I2C. Pagina 4919 - Programming Aid

