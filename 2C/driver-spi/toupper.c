#include "spi_td3.h"


MODULE_LICENSE("Dual BSD/GPL");

/************************************************************************************************************************************************/
// File operations
/************************************************************************************************************************************************/
static struct file_operations spi_ops = {
	.owner = 	THIS_MODULE,
	.read = 	spi_read,
	.open = 	spi_open,
	.write = spi_write,
	.release = 	spi_close,
};
/************************************************************************************************************************************************/



/************************************************************************************************************************************************/
// Variables
/************************************************************************************************************************************************/
static dev_t mi_dispo;
static struct cdev * p_cdev;
static struct class * pclase;
static struct device * pdevice_sys;
// OJO, a los punteros ponerle volatile?
static int spi_irq=0;
static struct resource mem_res;
static DECLARE_WAIT_QUEUE_HEAD (spi_waitqueue);
static void *SPI0; 											// Poseera la direccion del SPI0 donde inician los registros del SPI0
static void *cmper;										    // Possera la direccion base de la zona de registros para el CMPER
static void *cont_mod;									    // Possera la direccion base de la zona de registros para el CONT_MOD

// Variables propias del driver
static char *drv_bufferRx;
static char *drv_bufferTx;
static int drv_buffRx_cnt;
static int drv_buffRx_length;

static DECLARE_WAIT_QUEUE_HEAD(wait_q_tx);
static DECLARE_WAIT_QUEUE_HEAD(wait_q_rx);
static int flag_wait_tx;
static int flag_wait_rx;



/************************************************************************************************************************************************/



/************************************************************************************************************************************************/
// Defino el platform driver
/************************************************************************************************************************************************/
static const struct of_device_id spi_td3_dt[] = {
	{ .compatible = "td3,omap4-mcspi" },
	{}
};

// OJO REVISAR EL .COMPATIBLE

MODULE_DEVICE_TABLE (of, spi_td3_dt);

static struct platform_driver midriver_spi = {
    .probe = midriver_probe,
    .remove = midriver_remove,
    .driver = {
            .name = "spi_td3",
            .of_match_table = of_match_ptr(spi_td3_dt),
    },
};
/************************************************************************************************************************************************/



/************************************************************************************************************************************************/
// PROBE
/************************************************************************************************************************************************/
static int midriver_probe(struct platform_device *pp) 
{
	int aux;
	int contador = 0;

	unsigned int aux_2 = 0;

	printk(KERN_ALERT "Entre al probe!\n");

	// Obtengo un virtual IRQ para la interrupción de mi devsitivo
	spi_irq = irq_of_parse_and_map ((pp->dev).of_node, 0);
	if(spi_irq==0)
	{
		platform_driver_unregister(&midriver_spi);
		device_destroy(pclase, mi_dispo);
		class_destroy(pclase);
		cdev_del (p_cdev);
		unregister_chrdev_region(mi_dispo, NBR_DEV);
		printk(KERN_ALERT "spi_td3: Error al intentar obtener el VIRQ\n");
		return EBUSY;
	}
 	printk(KERN_ALERT "spi_td3: VIRQ obtenido para spi_td3 = %d\n", spi_irq);

	// Implanto el handler de IRQ
  	aux = request_irq(spi_irq, (irq_handler_t) SPI0_IRQHandler,  IRQF_TRIGGER_RISING  , "spi_td3", NULL);
	if(aux!=0)
	{
		platform_driver_unregister(&midriver_spi);
		device_destroy(pclase, mi_dispo);
		class_destroy(pclase);
		cdev_del (p_cdev);
		unregister_chrdev_region(mi_dispo, NBR_DEV);
		printk(KERN_ALERT "spi_td3: Error al intentar implantar handler de IRQ\n");
		return EBUSY;
	}
 	printk(KERN_ALERT "spi_td3: Handler de IRQ implantado OK\n");

	// Leo la zona de memoria asociada al spi_td3 del device tree
	aux = of_address_to_resource((pp->dev).of_node, 0, &mem_res);
	if (aux)
	{
		free_irq(spi_irq, NULL);
		platform_driver_unregister(&midriver_spi);
		device_destroy(pclase, mi_dispo);
		class_destroy(pclase);
		cdev_del (p_cdev);
		unregister_chrdev_region(mi_dispo, NBR_DEV);
		printk(KERN_ALERT "spi_td3: Error al obtener zona de memoria del device tree\n");
		return EBUSY;
	}

	// Registro la zona de memoria como IO asociada al devsitivo
	// Chequear esto en la BEAGLE
	if  (!request_mem_region(mem_res.start, resource_size(&mem_res), "/ocp/spi_td3"))
	{
		free_irq(spi_irq, NULL);
		platform_driver_unregister(&midriver_spi);
		device_destroy(pclase, mi_dispo);
		class_destroy(pclase);
		cdev_del (p_cdev);
		unregister_chrdev_region(mi_dispo, NBR_DEV);
		printk(KERN_ALERT "spi_td3: Error al registrar zona de memoria\n");
		return EBUSY;
 	}

	// Pido memoria física en la zona del pad de pines
	cont_mod = ioremap (CONT_MOD_ADDR, CONT_MOD_LEN);
	if (cont_mod == NULL)
	{
		free_irq(spi_irq, NULL);
		platform_driver_unregister(&midriver_spi);
		device_destroy(pclase, mi_dispo);
		class_destroy(pclase);
		cdev_del (p_cdev);
		unregister_chrdev_region(mi_dispo, NBR_DEV);
		printk(KERN_ALERT "spi_td3: Error en ioremap de cont_mod\n");
		return -EBUSY;
	}

	// Pido memoria física en la zona de los registros del SPI0 
	SPI0 = of_iomap ((pp->dev).of_node, 0);
	if (SPI0 == NULL)
	{
		iounmap(cont_mod);
		free_irq(spi_irq, NULL);
		platform_driver_unregister(&midriver_spi);
		device_destroy(pclase, mi_dispo);
		class_destroy(pclase);
		cdev_del (p_cdev);
		unregister_chrdev_region(mi_dispo, NBR_DEV);
		printk(KERN_ALERT "spi_td3: Error en ioremap de la zona de registros del i2c2\n");
		return -EBUSY;
	}

	// Pido memoria física en la zona de los registros de la CMPER
	cmper = ioremap (CMPER_ADDR, CMPER_LEN);
	if (cmper == NULL)
	{
		iounmap(cont_mod);
		iounmap(SPI0);
		free_irq(spi_irq, NULL);
		platform_driver_unregister(&midriver_spi);
		device_destroy(pclase, mi_dispo);
		class_destroy(pclase);
		cdev_del (p_cdev);
		unregister_chrdev_region(mi_dispo, NBR_DEV);
		printk(KERN_ALERT "spi_td3: Error en ioremap de la zona de registros de la cmper\n");
		return -EBUSY;
	}



	printk(KERN_ALERT "OPEN\n");

	// Habilito clck
	aux = ioread32(cmper + CM_PER_L4LS);
	iowrite32(CM_PER_L4LS_ENABLED | aux, cmper + CM_PER_L4LS);
	iowrite32(CM_PER_SPI0_ENABLED ,cmper + CM_PER_SPI0_CLKCTRL);

	aux_2 = 0;
	while (aux_2!= CM_PER_SPI0_ENABLED)
	{
	msleep (1);
	aux_2 =  ioread32 (cmper + CM_PER_SPI0_CLKCTRL);
	if ( contador > 4 )
	{
		iounmap(cont_mod);
			iounmap(cmper);
			iounmap(SPI0);
		printk(KERN_ALERT "No se pudo configurar clock del SPI0\n");
		return -1;
	}
	contador++;
	}
	contador = 0;

	// Aca debo configurar MISO MOSI Y CLK
	// Me tengo que fijar si van o no los pull-ups. El modo de MISO y MOSI es el MODO 0 (CERO)
	// Bits 2-0 = 0x000 (MODO 0)
	// Bit 3 = 0 (pull up/down enabled)
	// Bit 4 = 1 (pull up selected)
	// Bit 5 = 0: receiver disabled
	//		   1: receiver enabled (Viene por defecto en 1)

	iowrite32(0x30 , cont_mod + conf_spi0_sclk);				// Modo 0. Pull up enabled and selected. Receiver enabled

	iowrite32(0x20, cont_mod + conf_spi0_d0);					// MISO. Modo 0.Pull up enabled pero no selected. Receiver enabled

	iowrite32(0x30, cont_mod + conf_spi0_d1);					// MOSI. Modo 0. Pull up enabled and selected. Receiver enabled

	// Configuro los CS. PIN 115
	gpio_request(GPIO_CS, "sysfs");
	gpio_direction_output(GPIO_CS, 1);
	gpio_set_value(GPIO_CS, 1);
	gpio_export(GPIO_CS, 0);


	// Reset del modulo. Es un soft reset, pongo en uno el bit 1 del registro
	aux_2 = ioread32(SPI0 + MCSPI_SYSCONFIG);
	iowrite32(2,SPI0 + MCSPI_SYSCONFIG);

	// Esperar a ver si reinicio ok. Leer el bit 0 del MCSPI_SYSSTATUS. Si es 1, reset ok
	aux_2 = 0;
	while(aux_2!=1)
	{
			msleep(1);
			aux_2 = ioread32(SPI0 + MCSPI_SYSSTATUS);
			if(contador>2)
			{
				iounmap(cont_mod);
				iounmap(cmper);
				iounmap(SPI0);
			printk(KERN_ALERT "No se hizo el RESET correctamente\n");
			return -1;
			}
			contador++;
	}
	contador=0;

	// Configuracion del MCSPI_MODULECTRL. Pag 4933
	// Bit 2 = 0: Master. Viene en 1 por defecto!
	// Bit 1 = 1: No CS. Viene en 0 por defecto
	// Bit 0 = 1: Single channel. Viene en 0 por defecto
	//aux = ioread32(SPI0 + MCSPI_MODULECTRL);
	//iowrite32(3 | (aux & ~(1<<2)),SPI0 + MCSPI_MODULECTRL); // Borro el bit 2 y hago una OR con 0x11 (3)


	//iowrite32 (0x308, SPI0 + MCSPI_SYSCONFIG);
	iowrite32(2, SPI0 + MCSPI_MODULECTRL); 					// LO PONGO MULTICHANNEL
	iowrite32 (0x308, SPI0 + MCSPI_SYSCONFIG);

	// Configuracion del MCSPI_SYSCONFIG. Pag 4923
	// Bit 9-8 = 3 (0x11): OCP and functional clocks are mantained. Vienen en 0x00 por defecto
	// Bit 4-3 = 1 (0x01): if an idle request is detected, the request is ignored and keeps on behaving normally. Vienen en 0x00 por defecto
	// 1100001000 es mi mascara = 0x308
	//aux = ioread32(SPI0 + MCSPI_SYSCONFIG);
	//iowrite32 (0x308, SPI0 + MCSPI_SYSCONFIG);

	// Configuracion del MSCPI_SYST. Pag 4930
	// Bit 9 SPIDATDIR1 = 0: Salida - es mi MOSI (viene en 0 por defecto)
	// Bit 8 SPIDATDIR0 = 1: Entrada - es mi MISO (viene en 0 por defecto)
	// 0100000000 es mi mascara
	//aux = ioread32(SPI0 + MCSPI_SYST);
	//iowrite32(aux | 0x100, SPI0 + MCSPI_SYST);


	// Configuracion  del MCSPI_CH0CONF
	// Bit 18 IS Input select = 0: Data line 0 selected for reception (viene en 1 por defecto)
	// Bit 17 DPE1 = 0: Data line 1 selected for transmission (Viene en 1 por defecto)
	// Bit 16 DPE0 = 1: No TX en DAT0
	// Bit 11-7 WL = 7h (0x111): Palabra de 8 bits, chequear con sensor
	// Bit 5-2 CLKD = 3h: Clock divider in 8 -> 48MHz / 8 = 6MHz de clock
	// Polaridad y fase????
	// 0001 0000 0011 1000 1100 = 0x38C
	// aux = ioread32(SPI0 + MCSPI_CH0CONF);
	// iowrite32(0x38C | (aux & ~(0x11<18)),SPI0 + MCSPI_CH0CONF); // Borro bits 18 y 17 y seteo los demas bits
	// iowrite32(0x1038C, SPI0 + MCSPI_CH0CONF);
	//iowrite32(0x1038F, SPI0 + MCSPI_CH0CONF);
	//iowrite32(0x1079F, SPI0 + MCSPI_CH0CONF); //ANDA

	//iowrite32(0x1038F, SPI0 + MCSPI_CH0CONF); // 6MHz
	iowrite32(0x81103D3, SPI0 + MCSPI_CH0CONF);  // 3MHz


	// Guido
	// 0001 0000 0111 1001 1111 = guido toca bit 6 y 1
	iowrite32(0, SPI0 + MCSPI_CH0CTRL);

	iowrite32(0x7FFF, SPI0 + MCSPI_IRQSTATUS);			// Limpio interrupciones
	iowrite32(5, SPI0 + MCSPI_IRQENABLE);  // RX0 FULL ENABLE y TX0 EMPTY ENABLE

	// Configuracion del MCSPI_CH0CTRL
	// Bit 0 EN = 1:  Channel 0 is active (viene en 0 por defecto)
	// aux = ioread32(SPI0 + MCSPI_CH0CTRL);
	iowrite32(0x01, SPI0 + MCSPI_CH0CTRL);
	printk(KERN_CRIT"spi_td3: Probe finalizado correctamente\n");
	return 0;
}

/************************************************************************************************************************************************/


/************************************************************************************************************************************************/
// REMOVE
/************************************************************************************************************************************************/
static int midriver_remove(struct platform_device *midriver_spi)
{

	printk(KERN_ALERT "Entre al remove!\n");
	
	free_irq(spi_irq, NULL);
	iounmap(cmper);
	iounmap(SPI0);
	iounmap(cont_mod);
	
	return 0;
}
/************************************************************************************************************************************************/



/************************************************************************************************************************************************/
// OPEN.
/************************************************************************************************************************************************/
static int spi_open(struct inode *inode, struct file *filp) {

  printk(KERN_ALERT "Open ejecutado correctamente\n");

  return 0;
}
/************************************************************************************************************************************************/




/************************************************************************************************************************************************/
// INSMODE // INIT
/************************************************************************************************************************************************/
static int hello_init(void) 
{
	int i;
	static struct platform_device * pdev;
	
	// Mensaje inicial
	printk(KERN_ALERT "Hello, world. Estoy en INIT!!!\n");

	//p_cdev = cdev_alloc();
	// Pido el numero mayor
	if((alloc_chrdev_region(&mi_dispo, FIRST_MINOR, NBR_DEV,"spi_td3"))<0)
	//if((alloc_chrdev_region(&mi_dispo, FIRST_MINOR, NBR_DEV,"toupper_td3"))<0) asi lo teniamos con guido
	{
		printk(KERN_ALERT "No se asigno el numero mayor\n");
		return -EBUSY;
	}

	printk(KERN_ALERT "Numero mayor asignado = %d\n", MAJOR(mi_dispo));
	// OJO ACA CON EL ORDEN

	p_cdev = cdev_alloc();
	//unregister_chrdev_region en caso de error

	// Registro el dispositivo
	p_cdev->ops = &spi_ops;
	p_cdev->owner = THIS_MODULE;
	p_cdev->dev = mi_dispo;

	//printk(KERN_ALERT "struct cdev allocada\n");

	if((cdev_add(p_cdev, mi_dispo, NBR_DEV))<0)
	{
		printk(KERN_ALERT "No es posible registrar\n");
		unregister_chrdev_region(mi_dispo, NBR_DEV);
		return -EBUSY;
	}

	// Creo la clase. Hacer error check
	pclase = class_create(THIS_MODULE, "td3");
	if (IS_ERR(p_cdev))
	{
		cdev_del (p_cdev);
		unregister_chrdev_region(mi_dispo, NBR_DEV);
		return PTR_ERR(pclase);
	}

	// Creo el dispositivo dentro de la clase. Hacer error check
	pdevice_sys = device_create(pclase, NULL, mi_dispo, NULL, "spi_td3"); /// ACA PUEDE QUE VALLA toupper
	if (IS_ERR(pdevice_sys))
	{
		class_destroy(pclase);
		cdev_del (p_cdev);
		unregister_chrdev_region(mi_dispo, NBR_DEV);
		return PTR_ERR(pdevice_sys);
	}

	// Creo el platform device
	pdev =	platform_device_alloc("spi_td3", -1);
	printk(KERN_ALERT "Devuelto platform_device_alloc = %x\n", (unsigned int) pdev);
	if (pdev == 0)
	{
		device_destroy(pclase, mi_dispo);
		class_destroy(pclase);
		cdev_del (p_cdev);
		unregister_chrdev_region(mi_dispo, NBR_DEV);
		printk(KERN_ALERT "No paso el platform_device_alloc\n");
		return -EBUSY;
	}

	// Obtengo la información del device tree, la información del devsitivo
	i = of_platform_populate(NULL, of_match_ptr(spi_td3_dt), NULL , NULL);
	if (i != 0)
	 {
		 device_destroy(pclase, mi_dispo);
		 class_destroy(pclase);
		 cdev_del (p_cdev);
		 unregister_chrdev_region(mi_dispo, NBR_DEV);
		 printk(KERN_ALERT "No paso el of_platform_populate\n");
		 return -EBUSY;
	 }
	
	// Registro el driver, una vez realizado esto ingreso a la función probe
	i =	platform_driver_register(&midriver_spi);
	if (i != 0)
	{
		device_destroy(pclase, mi_dispo);
		class_destroy(pclase);
		cdev_del (p_cdev);
		unregister_chrdev_region(mi_dispo, NBR_DEV);
		printk(KERN_ALERT "No se pudo completar la registracion\n");
		return -EBUSY;
	}

	// Inicializo el buffer interno del driver
	drv_bufferRx = (char *) kmalloc(Rx_MEMORY_SYZE, GFP_KERNEL);
	drv_bufferTx = (char *) kmalloc(Tx_MEMORY_SYZE, GFP_KERNEL);
	if(drv_bufferRx == NULL || drv_bufferTx == NULL)
	{
		printk(KERN_ALERT "spi message: Init: Failed to allocate memory for the internal buffer.\n");
		platform_driver_unregister(&midriver_spi);
		device_destroy(pclase, mi_dispo);
		class_destroy(pclase);
		cdev_del (p_cdev);
		unregister_chrdev_region(mi_dispo, NBR_DEV);
		printk(KERN_ALERT "No se pudo completar la registracion\n");
		return -ENOMEM;
	}

	if( memset(drv_bufferRx, 0, Rx_MEMORY_SYZE) == NULL ){
		printk(KERN_ALERT "spi message: Init: Failed initializing memory for the internal buffer.\n");
		printk(KERN_ALERT "spi message: Init: Failed to allocate memory for the internal buffer.\n");
		platform_driver_unregister(&midriver_spi);
		device_destroy(pclase, mi_dispo);
		class_destroy(pclase);
		cdev_del (p_cdev);
		unregister_chrdev_region(mi_dispo, NBR_DEV);
		return -ENOMEM;
	}
	if( memset(drv_bufferTx, 0, Tx_MEMORY_SYZE) == NULL ){
		printk(KERN_ALERT "spi message: Init: Failed initializing memory for the internal buffer.\n");
    	printk(KERN_ALERT "spi message: Init: Failed to allocate memory for the internal buffer.\n");
		platform_driver_unregister(&midriver_spi);
		device_destroy(pclase, mi_dispo);
		class_destroy(pclase);
		cdev_del (p_cdev);
		unregister_chrdev_region(mi_dispo, NBR_DEV);
		return -ENOMEM;
	}

	// Registracion
	//platform_driver_register(&midriver_i2c);
	printk(KERN_ALERT "Me registre\n");

	return 0;
}
/************************************************************************************************************************************************/


/************************************************************************************************************************************************/
// EXIT
/************************************************************************************************************************************************/
static void hello_exit(void) 
{

	iounmap(cont_mod);
	iounmap(SPI0);
	iounmap(cmper);
	release_mem_region(mem_res.start, resource_size(&mem_res));
	free_irq(spi_irq, NULL);

	// Me unregistro
	platform_driver_unregister(&midriver_spi);
	printk(KERN_ALERT "Estoy en EXIT!!!. Me des-registre\n");

	
	device_destroy(pclase, mi_dispo);
	
	class_destroy(pclase);
	
	cdev_del(p_cdev);
	
	unregister_chrdev_region(mi_dispo, NBR_DEV);
	
	// Mensaje final
	printk(KERN_ALERT "Goodbye, cruel world. He pasado por el exit\n");
}
/************************************************************************************************************************************************/



/************************************************************************************************************************************************/
// CLOSE. Falta implementar
/************************************************************************************************************************************************/
static int spi_close(struct inode *inode, struct file *filp){


  	printk(KERN_CRIT "CLOSE\n");

	return 0;
}
/************************************************************************************************************************************************/


/************************************************************************************************************************************************/
// READ. Falta implementar
/************************************************************************************************************************************************/
static ssize_t spi_read(struct file *file, char *buf, size_t length, loff_t *nose) {
    
// TODO: asignar distintos return segun cada error
	int copied;
	int cnt;
	unsigned int ret;
	char *read_buffer;

	#ifdef DEBUG
		printk(KERN_INFO "spi message: Read: Start of reading.\n");
	#endif

	if(length > MAX_BYTE_READ)
	{
		printk(KERN_ALERT "spi message: Read: Attempted to read more bytes than allowed.\n");
		return -1;
	}

	// Verifico si el puntero a espacio usuario es valido
	if(!(access_ok(VERIFY_WRITE, buf, length))){
		printk(KERN_ALERT "spi message: Read: Invalid user buffer passed as an argument.\n");
		return -1;
	}

	// Inicializo el buffer de transmision
	read_buffer = (char *) kmalloc(length, GFP_KERNEL);
	ret = copy_from_user(read_buffer, buf, length);
	if(ret < 0)
	{
		printk(KERN_ALERT "spi message: Write: Failed copying data from user buffer.\n");
		return -1;
	}

	// Reinicio el contador de datos recibidos a cero
	drv_buffRx_cnt = 0;
	drv_buffRx_length = length;
	flag_wait_rx = 0;

	gpio_set_value(GPIO_CS, 0);

	// 3) Copio los datos al buffer de Tx de McSPI
	for(cnt=0; cnt<length; cnt++)
	{
		iowrite32(read_buffer[cnt], SPI0 + MCSPI_TX0);
		do {
			ret = ioread32(SPI0 + MCSPI_IRQSTATUS);
		} while( !(ret & 1) );
	}
	gpio_set_value(GPIO_CS, 1);

	// Espero a que terminen de llegar los datos
	ret = wait_event_interruptible_timeout(wait_q_rx, flag_wait_rx > 0, (HZ));

	if(ret < 0)
	{
		// Error de interrupcion
		printk(KERN_ALERT "spi message: Read: Interrupt error.\n");
		return -1;
	}
	else
	{
		if(!ret)
		{
			// Timeout
			printk(KERN_ALERT "spi message: Read: Timeout error.\n");
			return -2;
		}
	}

	copied = copy_to_user(buf, drv_bufferRx, length);
	if(copied < 0)
	{
		printk(KERN_ALERT "spi message: Read: Failed copying data to user buffer.\n");
	}

	#ifdef DEBUG
		printk(KERN_INFO "spi message: Read: End of reading.\n");
	#endif

	return length;
}



static ssize_t spi_write (struct file *file, const char __user *buf,size_t length, loff_t *offset)
{
	int ret;
	int reg;
	int cnt;

	#ifdef DEBUG
		printk(KERN_INFO "spi message: Write: Start of transmision.\n");
	#endif

// TODO: retornar valores diferentes segun error
	if(length > MAX_BYTE_WRITE)
	{
		printk(KERN_ALERT "spi message: Write: Attempted to write more bytes than allowed.\n");
		return -1;
	}

	// Verifico si el buffer que me paso el usuario esta bien
	if(!(access_ok(VERIFY_WRITE, buf, length))){
		printk(KERN_ALERT "spi message: Write: Invalid user buffer passed as an argument.\n");
		return -1;
	}

	ret = copy_from_user(drv_bufferTx, buf, length);
	if(ret < 0)
	{
		printk(KERN_ALERT "spi message: Write: Failed copying data from user buffer.\n");
		return -1;
	}
	printk(KERN_INFO "spi message: Write: Received %s from user", drv_bufferTx);
	

	flag_wait_tx = 1;
	gpio_set_value(GPIO_CS, 0);

	// 3) Copio los datos al buffer de Tx de McSPI
	for(cnt=0; cnt<length; cnt++)
	{
		iowrite32(drv_bufferTx[cnt], SPI0 + MCSPI_TX0);
		do {
			reg = ioread32(SPI0 + MCSPI_IRQSTATUS);
		} while( !(reg & (1)) );
	}
	gpio_set_value(GPIO_CS, 1);
	flag_wait_tx = 0;

	#ifdef DEBUG
		printk(KERN_INFO "spi message: Write: End of transmission.\n");
	#endif
	
	return length;

}

/************************************************************************************************************************************************/
// HANDLER DE INTERRUPCION. Falta implementar
/************************************************************************************************************************************************/
static irq_handler_t SPI0_IRQHandler (unsigned int irq, void *dev_id, struct pt_regs *regs){ 


	int irq_handled = 0;
	int irq_status;
	char in;

  	printk(KERN_CRIT "Entre al handler de interrupcion\n");

	// Obtengo el estado de las interrupciones (pagina xxx)
	irq_status = ioread32(SPI0 + MCSPI_IRQSTATUS);

	// Interrupcion de Bus Free
	if(irq_status & (1))
	{
		#ifdef DEBUG_INTERRUPTS
			//printk(KERN_INFO "spi message: Interrupt: Bus Free.\n");
		#endif

		// Levanto el flag de fin de espera
		//flag_wait_tx = 1;

		// Limpio el flag de interrupcion, e indico que atendi la interrupcion
		iowrite32((1), SPI0 + MCSPI_IRQSTATUS);
		irq_handled = 1;

		wake_up_interruptible(&wait_q_tx);
	}


	// Interrupcion de Rx Full
	if(irq_status & (1 << 2))
	{
		in = (char) ioread32(SPI0 + MCSPI_RX0);

		if(!flag_wait_tx)
		{
			drv_bufferRx[drv_buffRx_cnt] = in;

			if(drv_buffRx_cnt < Rx_MEMORY_SYZE)
			{
				drv_buffRx_cnt++;
			}

			// Levanto el flag de fin de espera
			if(drv_buffRx_cnt >= drv_buffRx_length)
			{
				flag_wait_rx = 1;
				drv_buffRx_length = 0;
			}

			#ifdef DEBUG_INTERRUPTS
				printk(KERN_INFO "spi message: Interrupt: Reception Ready %d. Received: %d.\n", drv_buffRx_cnt, drv_bufferRx[drv_buffRx_cnt]);
			#endif
		} else {
			printk(KERN_INFO "spi message: Interrupt: Dummy write reception %d", in);
		}

		// Limpio el flag de interrupcion, e indico que atendi la interrupcion
		iowrite32((1 << 2), SPI0 + MCSPI_IRQSTATUS);
		irq_handled = 1;

		wake_up_interruptible(&wait_q_rx);
	}

	// Cualquier otra interrupcion
	if(!irq_handled)
	{
		// Limpio todos los flags de interrupcion
		iowrite32(0x7FFF, SPI0 + MCSPI_IRQSTATUS);
		printk(KERN_INFO "spi message: Interrupt: Other");
	}

	return (irq_handler_t) IRQ_HANDLED;

}
/************************************************************************************************************************************************/

// Callback de ejecucion con 'sudo insmod toupper.ko'
module_init(hello_init);

// Callback de ejecucion con 'sudo rmmod toupper.ko'
module_exit(hello_exit);
