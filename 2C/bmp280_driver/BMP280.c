/*
	Funciones creadas por mhafuzul islam para manejar los distintos pasos necesarios
	para obtener el valor de temperatura correcto segun los datos que nos devuelve el driver al hacerle un read
*/

#include "BMP280.h"
#include "math.h"

int file;

/*
*	Initialize library and coefficient for measurements
*/
char BMP280_begin(char *bus)
{
	//char *bus = "/dev/i2c-2";
	
	if((file = open(bus, O_RDWR)) < 0) 
	{
		perror("Failed to open the bus. \n");
		exit(1);
	}
	
	return (BMP280_readCalibration());
}

/*
*	Deinitialize library and coefficient for measurements
*/
char BMP280_end(void)
{
	if(file < 0) 
	{
		return 0;
	}
	else
	{
		close(file);
	}

	return 1;
}

// The BMP280 includes factory calibration data stored on the device.
// Each device has different numbers, these must be retrieved and
// used in the calculations when taking measurements.

// Retrieve calibration data from device:
char BMP280_readCalibration() 
{
	
	if (    
		BMP280_readUInt(0x88, &dig_T1) &&
		BMP280_readInt(0x8A, &dig_T2)  &&
		BMP280_readInt(0x8C, &dig_T3)  &&
		BMP280_readUInt(0x8E, &dig_P1) &&
		BMP280_readInt(0x90, &dig_P2)  &&
		BMP280_readInt(0x92, &dig_P3)  &&
		BMP280_readInt(0x94, &dig_P4)  &&
		BMP280_readInt(0x96, &dig_P5)  &&
		BMP280_readInt(0x98, &dig_P6)  &&
		BMP280_readInt(0x9A, &dig_P7)  &&
		BMP280_readInt(0x9C, &dig_P8)  &&
		BMP280_readInt(0x9E, &dig_P9)){
		return (1);
	}
	else 
		return (0);
}

/*
**	Read a signed integer (two bytes) from device
**	@param : address = register to start reading (plus subsequent register)
**	@param : value   = external variable to store data (function modifies value)
*/
char BMP280_readInt(char address, double *value)
{
	unsigned char data[2];	//char is 4bit,1byte

	data[0] = address;
	if (BMP280_readBytes(data,2))
	{
		*value = (double)(int16_t)(((unsigned int)data[1]<<8)|(unsigned int)data[0]); //
		return(1);
	}
	*value = 0;
	return(0);
}
/* 
**	Read an unsigned integer (two bytes) from device
**	@param : address = register to start reading (plus subsequent register)
**	@param : value 	 = external variable to store data (function modifies value)
*/

char BMP280_readUInt(char address, double *value)
{
	unsigned char data[2];	//4bit
	data[0] = address;
	if (BMP280_readBytes(data,2))
	{
		*value = (double)(unsigned int)(((unsigned int)data[1]<<8)|(unsigned int)data[0]);
		return(1);
	}
	*value = 0;
	return(0);
}
/*
** Read an array of bytes from device
** @param : value  = external array to hold data. Put starting register in values[0].
** @param : length = number of bytes to read
*/

char BMP280_readBytes(unsigned char *values, char length)
{
	char *buf = malloc(length+1);
	int i;

	buf[0] = values[0] | 0x80;
	for(i=1; i<length+1; i++)
	{
		buf[i] = buf[i-1]+1;
		buf[i] |= 0x80;
	}

	char bytes_read = read(file, buf, length+1);

	memcpy(values, buf+1, length);

	return(bytes_read);
}
/*
** Write an array of bytes to device
** @param : values = external array of data to write. Put starting register in values[0].
** @param : length = number of bytes to write
*/
char BMP280_writeBytes(unsigned char *values, char length)
{
	char *buf = malloc(length+1);
	int i;

	buf[0] = values[0] & 0x7F;
	for(i=1; i<length+1; i++)
	{
		buf[i] = buf[i-1]+1;
		buf[i] &= 0x7F;
	}
	
	char bytes_written = write(file, buf, length+1);

	return(bytes_written);
}

short BMP280_getOversampling(void)
{
	return oversampling;
}

char BMP280_setOversampling(short oss)
{
	oversampling = oss;
	return (1);
}
/*
**	Begin a measurement cycle.
** Oversampling: 0 to 4, higher numbers are slower, higher-res outputs.
** @returns : delay in ms to wait, or 0 if I2C error.
*/
char BMP280_startMeasurement(void)

{
	unsigned char data[2], result, delay;
	
	data[0] = BMP280_REG_CONTROL;

	switch (oversampling)
	{
		case 0:
			data[1] = BMP280_COMMAND_PRESSURE0;     
			oversampling_t = 1;
			delay = 8;			
		break;
		case 1:
			data[1] = BMP280_COMMAND_PRESSURE1;     
			oversampling_t = 1;
			delay = 10;			
		break;
		case 2:
			data[1] = BMP280_COMMAND_PRESSURE2;		
			oversampling_t = 1;
			delay = 15;
		break;
		case 3:
			data[1] = BMP280_COMMAND_PRESSURE3;
			oversampling_t = 1;
			delay = 24;
		break;
		case 4:
			data[1] = BMP280_COMMAND_PRESSURE4;
			oversampling_t = 1;
			delay = 45;
		break;
		case 16:
			data[1] = BMP280_COMMAND_OVERSAMPLING_MAX;
			oversampling_t = 1;
			delay = 80;
		break;
		default:
			data[1] = BMP280_COMMAND_PRESSURE0;
			delay = 9;
		break;
	}
	result = BMP280_writeBytes(data, 2);
	if (result)
		return(delay); // return the delay in ms (rounded up) to wait before retrieving data
	else
		return(0); // or return 0 if there was a problem communicating with the BMP
}

/*
**	Get the uncalibrated pressure and temperature value.
**  @param : uP = stores the uncalibrated pressure value.(20bit)
**  @param : uT = stores the uncalibrated temperature value.(20bit)
*/
char BMP280_getUnPT(double *uP, double *uT)
{
	unsigned char data[6];
	char result;
	
	data[0] = BMP280_REG_RESULT_PRESSURE; 	//0xF7 

	result = BMP280_readBytes(data, 6); 	// 0xF7; 0xF8, 0xF9, 0xFA, 0xFB, 0xFC
	if (result)
	{
		*uP = (((long)data[0] * 65536) + ((long)data[1] * 256) + (long)(data[2] & 0xF0)) / 16;
		*uT = (((long)data[3] * 65536) + ((long)data[4] * 256) + (long)(data[5] & 0xF0)) / 16;
	}
	return(result);
}
/*
** Retrieve temperature and pressure.
** @param : T = stores the temperature value in degC.
** @param : P = stores the pressure value in mBar.
*/
char BMP280_getTemperatureAndPressure(double *T,double *P)
{
	double uT;
	double uP;
	char result = BMP280_getUnPT(&uP,&uT);
	if(result!=0){
		// calculate the temperature
		result = BMP280_calcTemperature(T,uT);
		if(result){
			// calculate the pressure
			result = BMP280_calcPressure(P,uP);
			if(result)return (1);
			else error = 3 ;	// pressure error
			return (9);
		}else 
			error = 2;	// temperature error
	}
	else 
		error = 1;
	
	return (9);
}
/*
** temperature calculation
** @param : T  = stores the temperature value after calculation.
** @param : uT = the uncalibrated temperature value.
*/
char BMP280_calcTemperature(double *T, double adc_T)
{
	double var1 = (adc_T/16384.0 - dig_T1/1024.0)*dig_T2;
	double var2 = ((adc_T/131072.0 - dig_T1/8192.0)*(adc_T/131072.0 - dig_T1/8192.0))*dig_T3;
	t_fine = var1+var2;
	*T = (var1+var2)/5120.0;
	
	if(*T>100 || *T <-100)return 0;
	
	return (1);
}
/*
**	Pressure calculation from uncalibrated pressure value.
**  @param : P  = stores the pressure value.
**  @param : uP = uncalibrated pressure value. 
*/
char BMP280_calcPressure(double *P,double uP)
{
	//char result;
	double var1 , var2 ;
	double p = *P;
	
	var1 = (t_fine/2.0) - 64000.0;
	var2 = var1 * (var1 * (dig_P6/32768.0));
	var2 = var2 + (var1 * dig_P5 * 2.0);
	var2 = (var2/4.0)+((dig_P4)*65536.0);
	var1 = (dig_P3 * var1 * var1/524288.0 + dig_P2 * var1) / 524288.0;
	var1 = (1.0 + var1/32768.0) * dig_P1;
	p = 1048576.0- uP;
	p = (p-(var2/4096.0))*(6250.0/var1);
	var1 = dig_P9*(p)*(p)/2147483648.0;
	var2 = (p)*dig_P8/32768.0;
	p = p + (var1+var2+dig_P7)/16.0;
	p = (p)/100.0 ;
	*P = p;
	
	if(*P>1200.0 || *P < 800.0) {
		return (0);
	} else {
		return (1);
	}
}


double BMP280_getSealevel(double P, double A)
// Given a pressure P (mb) taken at a specific altitude (meters),
// return the equivalent pressure (mb) at sea level.
// This produces pressure readings that can be used for weather measurements.
{
	return(P/pow(1-(A/44330.0),5.255));
}


double BMP280_getAltitude(double P, double Po)
// Given a pressure measurement P (mb) and the pressure at a baseline P0 (mb),
// return altitude (meters) above baseline.
{
	return(44330.0*(1-pow(P/Po,1/5.255)));
}


char BMP280_getError(void)
	// If any library command fails, you can retrieve an extended
	// error code using this command. Errors are from the wire library: 
	// 0 = Success
	// 1 = Data too long to fit in transmit buffer
	// 2 = Received NACK on transmit of address
	// 3 = Received NACK on transmit of data
	// 4 = Other error
{
	return(error);
}

