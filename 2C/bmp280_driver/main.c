// Distributed with a free-will license.
// Use it any way you want, profit or free, provided it fits in the licenses of its associated works.
// BMP280
// This code is designed to work with the BMP280_I2CS I2C Mini Module available from ControlEverything.com.
// https://www.controleverything.com/content/Barometer?sku=BMP280_I2CSs#tabs-0-product_tabset-2

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <linux/i2c-dev.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <math.h>
#include <time.h>

#include "BMP280.h"

int main()
{
	printf("--------------- BMP280 - TD3 - Trotta Santiago ---------------\n\n");
	
	double pressure;
	double cTemp;
	double altitude;
	char result;
	int readings, i;
	unsigned char ver[]={0xD0};
	
	if( !BMP280_begin("/dev/spi_td3") )
	{
		printf("Error initializing sensor.\n");
		return -1;
	}
	BMP280_setOversampling(4);

	BMP280_readBytes(ver, 1);

	printf("BMP280 ID: %x\n", ver[0]);

	printf("Cuantas mediciones te gustaria tomar?\n>> ");
	scanf("%d", &readings);
	
	for(i=0; i<readings; i++)
	{
		result = BMP280_startMeasurement();

		if(result != 0){
			sleep(1);
			result = BMP280_getTemperatureAndPressure(&cTemp,&pressure);
			
			if(result != 0)
			{
				altitude = BMP280_getAltitude(pressure, P0);
				// Output data to screen
				printf("Temperature\t: %.2f C \n", cTemp);
				printf("Pressure\t: %.2f hPa \n", pressure);
				printf("Altitude \t: %.2f m \n", altitude);
				printf("\n");
			}
		}
		else {
			printf("Error leyendo del sensor.\n");
			return -1;
		}
	}
	
	BMP280_end();
	
	printf("\n----------------------------------------------------------\n");
	
	return 0;
}
