// #################################################################################################
// TP 2do Cuatrimestre - 2019 - Curso R5054 - Trotta Santiago
// #################################################################################################

// #################################################################################################
// Librerias necesarias
// #################################################################################################
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/socket.h>
#include <sys/shm.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>
#include <signal.h>
#include <math.h>
#include <time.h>

// #################################################################################################
// Defines
// #################################################################################################
#define 	PUERTO 		8000
#define 	BACKLOG  	5
#define 	BUFLEN		30000
#define  	SEM_VALUE   20

#define  	ERROR_400	1
#define  	ERROR_404	2
#define  	OK_200		3
#define  	CSS			4

#define		BACKLOG_INICIAL		2
#define		MAX_CONEX_INICIAL	1000
#define		FREQ_INICIAL		5
#define		MEDIA_INICIAL		1

// #define 	DEBUG 				1

// #################################################################################################*/
// Variables globales
// #################################################################################################*/
typedef struct memoria_compartida{
	float promedio;
} memsh;

union semun{
    int val;
    struct semid_ds *buf;
    unsigned short *array;
};

// #################################################################################################*/
// Prototipos de funciones
// #################################################################################################*/

void childWork(int sockClient, memsh *Temp, int semid);
int sendToClient(int sockClient, char *str, char *ipAddr, int Port);
void receiveFromClient(int sockClient, char *buf, char *ipAddr, int Port);
int  sendToClient (int sockClient, char *str, char *ipAddr, int Port);
void handler_sigchld (int sig);
void handler_sigusr1 (int sig);
int  htmlAnalysis (char *buf);
void HTMLResponse(int sockClient, char*ipAddr, int Port, int state_html_handle, memsh *Temp, int semid);
void config_file(void);



void Procesamiento_Temperatura(memsh *Temperatura, int semid);
void lock(int semid, struct sembuf sb);
void unlock(int semid, struct sembuf sb);
float Calculo_Media(int medicion);