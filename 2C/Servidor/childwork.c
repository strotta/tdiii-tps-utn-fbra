// #################################################################################################
// TP 2do Cuatrimestre - 2019 - Curso R5054 - Trotta Santiago
// #################################################################################################

#include "server.h"
#include "BMP280.h"

extern volatile int flag_usr1;

// #################################################################################################*/
// Tarea de cada proceso hijo
// #################################################################################################*/
void childWork(int sockClient, memsh *Temp, int semid)
{
	char buf[BUFLEN];
	char ipAddr[20];
	socklen_t length;
	int state_html_handle;
	struct sockaddr_in clientAddr;
	int Port;

  /* 8. Obtener direccion IP y puerto del cliente
   */

	length = sizeof(clientAddr);
	if (getpeername(sockClient, (struct sockaddr *)&clientAddr, &length))
	{
		perror("Error en función peername");
		exit(1);
	}

	strcpy(ipAddr, inet_ntoa(clientAddr.sin_addr));
	Port = ntohs(clientAddr.sin_port);

	receiveFromClient(sockClient, buf, ipAddr, Port);

	usleep(1000);

	// printf("Data from client (Request): %s", buf);
	// Debug trama HTML
	
	state_html_handle = htmlAnalysis(buf);

	HTMLResponse(sockClient, ipAddr, Port, state_html_handle, Temp, semid);	//Armamos respuesta

}


// #################################################################################################*/
// Funcion de envio hacia el cliente
// #################################################################################################*/
int sendToClient(int sockClient, char *str, char *ipAddr, int Port)
{
	// Para debug de trama HTML
	// printf("SERVIDOR: %s\r\n", str);
	if (send(sockClient, str, strlen(str), 0) < 0)
	{
		if (errno == EPIPE)
		{
			printf("Conexion cortada por el cliente %s:%d\n", ipAddr, Port);
		}
		else
		{
			perror("Fallo en la transmision de datos.");
		}
		exit(1);
	}
	return 0;
}

// #################################################################################################*/
// Funcion de recepcion  (desde cliente)
// #################################################################################################*/
void receiveFromClient(int sockClient, char *buf, char *ipAddr, int Port)
{
	int msgLength;

	memset(buf, 0, BUFLEN);
	if ((msgLength = recv(sockClient, buf, BUFLEN, 0)) < 0)
	{
		if (errno == EPIPE)
		{
			printf("Conexion cortada por el cliente %s:%d\n", ipAddr, Port);
			return;
		}
		perror("Fallo en la recepcion de datos.");
		exit(1);
	}
	// Para debug trama HTML
	// printf("CLIENTE %s:%d : %s\r\n",ipAddr, Port, buf);
}

/*#################################################################################################
Analizo el request del cliente

Para entender el formato: https://es.m.wikipedia.org/wiki/Protocolo_de_transferencia_de_hipertexto

Ejemplo de request del cliente:

GET /index.html HTTP/1.1
Host: www.example.com
Referer: www.google.com
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:45.0) Gecko/20100101 Firefox/45.0
Connection: keep-alive
[Línea en blanco]

Mi respuesta deberia ser:
HTTP/1.1 200 OK
Date: Fri, 31 Dec 2003 23:59:59 GMT
Content-Type: text/html
Content-Length: 1221

Codigo HTML
#################################################################################################*/
int htmlAnalysis (char *buf){
	
	char *line;
	char *aux;
	
	line = strtok(buf, "\n");			//Nos quedamos con la primer linea del request del cliente					
	
	aux = strtok(line, " ");			// Analizo esta primer linea

	if (strcmp(aux, "GET")){		// Si no  es GET => ERROR 400
		return ERROR_400;
	}
	
	aux = strtok(NULL, " ");			//Segundo componente de la primer linea
	
	//printf("Auxiliar: %s", Auxiliar);
	if(strcmp(aux, "/")==0){			//si es este caso, hay que enviar el codigo HTML
		return OK_200;
	}
	
	if(strcmp(aux, "/Servidor.css")==0){
		return CSS;
	}
	
	return ERROR_404;
}

// #################################################################################################*/
// Funcion que responde al request del cliente
// #################################################################################################*/
void HTMLResponse(int sockClient, char*ipAddr, int Port, int state_html_handle, memsh *Temp, int semid){
	 
	 int fd_html;
	 int  bytes_leidos;
	 char buffer[BUFLEN];
	 char Auxiliar[BUFLEN];
	 char respuesta[BUFLEN];
	 struct sembuf sb;
	 time_t curtime;			//Para enviar al cliente dia y horario
	 
	 time(&curtime);			//Obtiene dia y hora actual y lo devuelve en formato time_t
								//Luego ctime lo convierte a string
	 
	 switch(state_html_handle)
	 {
		 
		 case OK_200:

			fd_html = open("Servidor.html", O_RDONLY);
			if (fd_html < 0){
				perror("Error abriendo Servidor.html");
				exit(1);
			}
			
			memset(buffer, 0, BUFLEN);
			memset(Auxiliar, 0, BUFLEN);
			bytes_leidos = read(fd_html, buffer, sizeof(buffer)-1);
			
			// Para debug de trama HTML
			// printf("Bytes leidos: %d\n", bytes_leidos);
			if (bytes_leidos < 0){
				perror("Error leyendo Servidor.html");
				exit(1);
			}
			
			sendToClient(sockClient, "HTTP/1.1 200 OK\n", ipAddr, Port);
			sendToClient(sockClient, "Content-Type: text/html;charset=UTF-8\n", ipAddr, Port);
			sprintf(Auxiliar, "Content-Length: %d\n\n", bytes_leidos);
			sendToClient(sockClient, Auxiliar, ipAddr, Port);
			
			lock(semid, sb);  													//Como vamos a leer la memoria compartida, tomamos el semaforo
  
			sprintf(respuesta, buffer, Temp -> promedio, ctime(&curtime));		//Armamos respuesta (leemos memoria compartida)
  
			unlock(semid, sb);													//Una vez que leimos la memoria compartida, liberamos el semaforo
			
			sendToClient(sockClient, respuesta, ipAddr, Port);
			
			close(fd_html);
			
			break;
			
			
		case CSS:
			sendToClient(sockClient, "HTTP/1.1 200 OK\n", ipAddr, Port);
			
			fd_html = open("Servidor.css", O_RDONLY);
			if (fd_html < 0){
				perror("Error abriendo el CSS");
				exit(1);
			}
			
			memset(buffer, 0, BUFLEN);
			bytes_leidos = read(fd_html, buffer, sizeof(buffer)-1);
			
			// Para debug de trama HTML
			// printf("Bytes leidos: %d\n", bytes_leidos);
			if (bytes_leidos < 0){
				perror("Error leyendo utn.png");
				exit(1);
			}
			
			sendToClient(sockClient, "Content-Type: text/css;charset=UTF-8\n", ipAddr, Port);
			sprintf(respuesta, "Content-Length: %d\n\n", bytes_leidos);
			sendToClient(sockClient, respuesta, ipAddr, Port);
			
			if (send(sockClient, buffer, bytes_leidos, 0) > 0) {
				// Para debug de maquina de estados		
				// printf("SERVIDOR: Archivo CSS Enviada\n\n");				
			}
			
			close(fd_html);
			break;
			
		case ERROR_404:
			sendToClient(sockClient, "HTTP/1.1 404 Bad resource\n", ipAddr, Port);
			
			fd_html = open("Error_404.html", O_RDONLY);
			if (fd_html < 0){
				perror("Error abriendo Error_404.html");
				exit(1);
			}
			
			memset(buffer, 0, BUFLEN);
			memset(Auxiliar, 0, BUFLEN);
			bytes_leidos = read(fd_html, buffer, sizeof(buffer)-1);
			
			// Para debug de trama HTML
			// printf("Bytes leidos: %d\n", bytes_leidos);
			if (bytes_leidos < 0){
				perror("Error leyendo Error_404.html");
				exit(1);
			}
			
			sendToClient(sockClient, "Content-Type: text/html;charset=UTF-8\n", ipAddr, Port);
			sprintf(Auxiliar, "Content-Length: %d\n\n", bytes_leidos);
			sendToClient(sockClient, Auxiliar, ipAddr, Port);
			
			sendToClient(sockClient, buffer, ipAddr, Port);
			
			close(fd_html);
			break;
			
		case ERROR_400:
			sendToClient(sockClient, "HTTP/1.1 400 Bad method\n", ipAddr, Port);
			
			fd_html = open("Error_400.html", O_RDONLY);
			if (fd_html < 0){
				perror("Error abriendo Error_400.html");
				exit(1);
			}
			
			memset(buffer, 0, BUFLEN);
			memset(Auxiliar, 0, BUFLEN);
			bytes_leidos = read(fd_html, buffer, sizeof(buffer)-1);
			
			// Para debug de trama HTML
			// printf("Bytes leidos: %d\n", bytes_leidos);
			if (bytes_leidos < 0){
				perror("Error leyendo Error_400.html");
				exit(1);
			}
			
			sendToClient(sockClient, "Content-Type: text/html;charset=UTF-8\n", ipAddr, Port);
			sprintf(Auxiliar, "Content-Length: %d\n\n", bytes_leidos);
			sendToClient(sockClient, Auxiliar, ipAddr, Port);
			
			sendToClient(sockClient, buffer, ipAddr, Port);
			
			close(fd_html);
			break;
			
		default:
			printf("Error en Respuesta_HTML");
			break;
	 }
}


// #################################################################################################*/
// Uso del driver
// #################################################################################################*/
void Procesamiento_Temperatura(memsh *Temperatura, int semid){
    
   	struct sembuf sb;
	//int fd;
	//int bytesRead;
	//char bufferAux[3];
	float temp_media;

	double pressure;
	double cTemp;
	double altitude;
	char result;
	unsigned char ver[]={0xD0};


    lock(semid, sb);	//Tomamos el semaforo ya que vamos a escribir en la memoria compartida


	if( !BMP280_begin("/dev/spi_td3") )
	{
		printf("No funciono el open de /dev/spi_td3.\n");
		//return -1;
	}

	BMP280_setOversampling(4);

	BMP280_readBytes(ver, 1);

	// Ver de poner un sleep chico acá

	result = BMP280_startMeasurement();

	if(result != 0)
	{
		// Con esto mide
		sleep(1);
		result = BMP280_getTemperatureAndPressure(&cTemp,&pressure);
			
		if(result != 0)
		{
			altitude = BMP280_getAltitude(pressure, P0);
			// Output data to screen
			printf("Temperature\t: %.2f C \n", cTemp);
			printf("Pressure\t: %.2f hPa \n", pressure);
			printf("Altitude \t: %.2f m \n", altitude);
			printf("\n");
			
		}
		else 
		{
			printf("Error leyendo del sensor.\n");
			//return -1;
		}
	}

	// desComentar para usar calculo media
 	//temp_media = Calculo_Media((int) cTemp);
    
 	//Temperatura -> promedio = temp_media;						//Temperatura obtenida por el sensor
    
    // desComentar para usar el valor directo
   	Temperatura -> promedio = cTemp;

   	//sleep(1);
   	BMP280_end();
   	//sleep(1);
   	unlock(semid, sb);											//Liberamos el semaforo
}

// #################################################################################################*/
// Calculo de media (n mediciones)
// #################################################################################################*/
float Calculo_Media(int medicion){

	static int bufferMedia[5];
    static int n = 0;
    int suma = 0;
    int i = 0;
    float resultado;

    bufferMedia[n] = medicion;

    for(i=0; i<=n; i++) {
    	suma = suma + bufferMedia[i];
    }

    n++;
    resultado = (float) suma/n;

    if (n == 5) {
    	n = 0;
    }

    return resultado;
}

// #################################################################################################*/
// Funcion que toma el semaforo
// #################################################################################################*/
void lock(int semid, struct sembuf sb){
    
    sb.sem_num = 0;
    sb.sem_op = -1;
    sb.sem_flg = 0;
    
    if (semop(semid, &sb, 1) == -1) {
        perror("Error en funcion semop");
        exit(1);
    }
}

// #################################################################################################*/
// Para liberar el semaforo
// #################################################################################################*/
void unlock(int semid, struct sembuf sb){

    sb.sem_num = 0;
    sb.sem_op = 1;
    sb.sem_flg = 0;
    
    if (semop(semid, &sb, 1) == -1) {
        perror("Error en funcion semop");
        exit(1);
    }
}

// #################################################################################################*/
// Handler de señales capturadas
// #################################################################################################*/
void handler_sigchld (int sig)
{ 
	//Recordar guardar errno, ya que waitpid puede modificarlo en caso de error y pierdo el que tenia
	while (waitpid(-1, NULL, WNOHANG) > 0);  
}

// #################################################################################################*/
// Handler de SIGINT (CTRL + C)
// #################################################################################################*/
// void handler_sigint (int sig)
// { 
// 	//Recordar guardar errno, ya que waitpid puede modificarlo en caso de error y pierdo el que tenia
// 	while (waitpid(-1, NULL, WNOHANG) > 0);  
// }

// #################################################################################################*/
// Handler de SIGUSR1 (Para archivo de configuracion)
// #################################################################################################*/
void handler_sigusr1 (int sig)
{ 
	flag_usr1=1;
}


// #################################################################################################*/
// Funcion para leer archivo de configuracion y actualizar parametros del server
// #################################################################################################*/
void config_file (void)
{	
	char linea [200];
	char linea_aux [200];
	// int counter=0;
	char valor[5];
	char parametro[20];
	char letra = '=';
	int backlog = BACKLOG_INICIAL;
	int conexiones = MAX_CONEX_INICIAL;
	int frecuencia = FREQ_INICIAL;
	int media = MEDIA_INICIAL;
	FILE *file = NULL;

	file = fopen ("config.ini","r");

	if (file == NULL)
	{
		printf("\n\nError al abrir config file\n");
		printf("\nValores por defecto:");
		printf("\nBacklog: %d", backlog);
		printf("\nNum. Max. Conex.: %d", conexiones);
		printf("\nFrecuencia: %d", frecuencia);
		printf("\nMedia: %d", media);
	}
	else
	{
		// Leo linea por linea del archivo de configuracion
		while(fgets(linea, 200, file)!=NULL)
		{
			// Hago un backupo de la linea que levanto para usar strtok 
			// pues strtok es destructiva (modifica el valor del string que lee)
			strcpy(linea_aux,linea);

			// Filtro la linea hasta el =. Ej: Si la linea es .backlog=200 voy a obtener .backlog
			strcpy(parametro,strtok(linea_aux,"="));

			#ifdef DEBUG
				printf("\nParametro: %s",parametro);
			#endif

			strcpy(valor,strchr(linea, letra)+1);
			
			#ifdef DEBUG
			 	printf("\nValor: %s",valor);
			 #endif

			if (strcmp(parametro, ".backlog")==0)
			{
				backlog = atoi(valor);
				printf("\nValor BACKLOG actualizado: %d", backlog);
			}
			if (strcmp(parametro, ".conexiones")==0)
			{
				conexiones = atoi(valor);
				printf("\nValor CONEXIONES actualizado: %d", conexiones);
			}
			if (strcmp(parametro, ".frecuencia")==0)
			{
				frecuencia = atoi(valor);
				printf("\nValor FRECUENCIA actualizado: %d", frecuencia);
			}
			if (strcmp(parametro, ".media")==0)
			{
				media = atoi(valor);
				printf("\nValor MEDIA actualizado: %d\n\n", media);
			}
		}

		fclose(file);
	}

}