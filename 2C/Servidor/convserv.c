/*#################################################################################################*/
/* TP 2do Cuatrimestre - 2019 - Curso R5054 - Trotta Santiago */
/* Código basado en archivo convserv.c hecho por Dario Alpern */
/* Compilacion: make all*/
/* Copia a la beagle: make copy (ver directorio en makefile)*/
/* Para correr: ./convserv y en Firefox poner: 192.168.7.2:8000 */
/* Para lanzar una señal SIGUSR1: kill -10 PID*/
/* Mas informacion de kill: kill -l*/
/*#################################################################################################*/

#include "server.h"
volatile int flag_usr1 = 0;
/*#################################################################################################*/
/* Main */
/*#################################################################################################*/
int main(int argc, char *argv[])
{
	// Sockets section
	socklen_t length;
	int pid;
	struct sockaddr_in servAddr;
	int sockMain, sockClient;
	int yes = 1;

	// Shared memory section
	key_t key_shm;														//Key para memoria compartida
	int shmid;															//Identificador de memoria compartida
	memsh *Temperatura;			   										//Estructura que pondremos en memoria compartida, contendra la info que mande el LM75
	char *addr;
	int pid_sh;

	// Semaforo
	union semun argumento;
    key_t key_sem;
    int semid;

    //Variable para el proceso de temperatura
    int pid_padre;

    // Defino estructura para usar sigaction (reemplaza a signal para capturar sigusr1)
    struct sigaction sigusr1={0};
    sigusr1.sa_handler = handler_sigusr1;
    sigusr1.sa_flags = 0;

    signal(SIGCHLD, handler_sigchld);
	// signal(SIGUSR1, handler_sigusr1);	NO USAR SIGNAL! ver man signal
	sigaction(SIGUSR1, &sigusr1, NULL);

    // ftok usa el path y un ID para generar una key
    key_sem = ftok("Servidor", 'B');

    // Crea el segmento solamente si no existe = IPC_CREAT
    // El 1 es el MEM_SZ
    if ((semid = semget(key_sem, 1, IPC_CREAT | 0666)) == -1) {			//Obtenemos el identificador del semaforo
       perror("Error en funcion semget");
       exit(1);
    }

    argumento.val = SEM_VALUE;											//Seteo del semaforo
    if (semctl(semid, 0, SETVAL, argumento) == -1) {
        perror("Error en funcion semctl");
        exit(1);
    }
    
	key_shm = ftok("Servidor", 'A');

	shmid = shmget(key_shm, sizeof(memsh), IPC_CREAT|0660);				//Obtenemos identificador de la memoria compartida
	
	addr = shmat(shmid, NULL, 0);

	if (addr == (char *)(-1)){
		perror("Error en shmat");
		exit(1);
	}
	
	Temperatura = (memsh*) addr;										//Nuestra estructura apuntara a la memoria compartida
	
	Temperatura -> promedio = 0;

	if ((pid_sh = fork()) < 0) {										//Creamos un proceso hijo que leera lo que envie el BMP280 y lo escribira en la memoria compartida
		printf("Fallo en fork\n");										//Error en fork
		exit(1);
	}
	
	if (pid_sh == 0) {													//Proceso Hijo que se quedara procesando los datos del sensor
		
		pid_padre = getppid();											//Obtenemos el ppid, para en caso de que si muere el padre, no quede este proceso vivo consumiendo recursos
		
		while(1){
			if (pid_padre == getppid())
			{
				printf("Parent ID: %d\n",pid_padre);
				Procesamiento_Temperatura(Temperatura, semid);
			}
            else{
				printf("Murio el proceso padre\n");
				exit(1);
			}
		}
	}

	// signal(SIGCHLD, handler_sigchld);

	// // EN DEBUG
	// signal(SIGUSR1, handler_sigusr1);	// Capturo SIGUSR1 para archivo de configuracion
	// signal(SIGINT, handler_sigint)		// Capturo ctrl + C
	// //

  	/*Crea el bloque de control de transmision maestro*/

	if ((sockMain = socket(PF_INET, SOCK_STREAM, 0)) < 0)
	{
		perror("El servidor no puede abrir el socket.");
		exit(1);
	}
	/* Con esto evito"Address is already in use"*/
	if (setsockopt(sockMain, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1) {
		perror("Error en setsockopt");
		exit(1);
	}

  /* 2. Crea una estructura de datos para mantener la direccion
   *    y puerto local de IP a usar. Se podrian aceptar conexiones
   *    de clientes a cualquier direccion local de IP (INADDR_ANY).
   *    Como el servidor no usa ningun puerto publico, se fija en
   *    puerto = 0. La llamada a bind asignara un puerto al
   *    servidor y lo escribira en el TCB.
   */

	memset( (char *) &servAddr, 0, sizeof(servAddr));
	servAddr.sin_family = AF_INET;
	servAddr.sin_addr.s_addr = htonl(INADDR_ANY);
	//servAddr.sin_port = 0;
	servAddr.sin_port = htons(PUERTO);

  /* 3. Llama a bind. Bind recoge numero de puerto y lo
   *    escribe en el TCB.
   */

	if (bind(sockMain, (struct sockaddr *)&servAddr, sizeof(servAddr)))
	{
		perror("Fallo en funcion bind");
		exit(1);
	}

  /* Queremos ver el numero del puerto. Se usara la funcion
   * getsockname() para copiar el puerto en servAddr.
   */

	length = sizeof(servAddr);
	if (getsockname(sockMain, (struct sockaddr *)&servAddr, &length))
	{
		perror("Fallo en funcion getsockname()");
		exit(1);
	}
	printf("SERVIDOR: Escuchando por el puerto %d\n", ntohs(servAddr.sin_port));

  /* 4. Configura una cola que pueda dar cabida a cinco clientes.
   */
	// 5 es el BACKLOG
	listen(sockMain, 5);

  /* 5. Espera a que llegue un cliente. Accept devolvera
   *    un nuevo descriptor de conector que se usara con dicho cliente.
   */

	for (;;)
	{
		printf("Antes accept\n");
		sockClient = accept(sockMain, 0, 0);
		printf("Despues accept\n");
		if (sockClient < 0)
		{
			if(flag_usr1==1)
			{
				printf("SIGUSR1\n");
				config_file();
				flag_usr1=0;
				continue;
			}
			else
			{
				perror("Fallo en funcion accept");
				exit(1);
			}
		}

  /* 6. Crea un proceso hijo para manejar al cliente.
   */

		if ((pid = fork()) < 0)
		{
			printf("Fallo en fork");
			exit(1);
		}

		// Proceso hijo
		if (pid == 0)
		{    
			childWork(sockClient, Temperatura, semid);
			close(sockClient);
			shmdt(addr);
			exit(0);
		}

	close(sockClient);
	}
}


